<?php
/**
 * Created by PhpStorm.
 * User: zhan
 * Date: 2020/6/6  18:07
 */

// 获取当前域名
function getDomain(){
    return $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'];
}

//阿里OSS访问域名
function oss_domain(){
    return $_SERVER['REQUEST_SCHEME'] .'://'.env('ALIYUN_OSS_VISIT','').'/';
}

/**
 *获取唯一订单号
 * @return string
 */
function order_number(){
    return date('Ymd').substr(implode(NULL, array_map('ord', str_split(substr(uniqid(), 7, 13), 1))), 0, 8);
}

//公共方法
function policyMessageFormat($status, $message = '权限不足!',$code=405)
{

    if(!$status) {
        throw new \App\Exceptions\PolicyException('无权限',$code);
    }
    return true;
}

/**
 * 获取上传文件名
 */
function pathName(){
    // 保存路径
    $saveName = 'wxhead/' . date('Ymd') . '/';

    // 全路径
    $path = 'upload/' . $saveName;
    // 验证文件夹是否存在
    if(!is_dir($path)){
        // 不存在则创建
        mkdir($path,0777,true);
    }

    // 获取图片名称
    $imageName = md5(time()) . '.jpg';
    $pathName = 'upload/'.$saveName . $imageName;
    return $pathName;
}

/**
 * 下载微信头像
 */
function getWxHead($imageUrl){
    //dd($imageUrl);die;
    // 保存路径
    $saveName = 'wxhead/' . date('Ymd') . '/';

    // 全路径
    $path = 'upload/' . $saveName;
    // 验证文件夹是否存在
    if(!is_dir($path)){
        // 不存在则创建
        mkdir($path,0777,true);
    }

    // 获取图片名称
    $imageName = md5(time()) . '.jpg';

    downloadImages($imageUrl, $path . $imageName);
    return '/upload/'.$saveName . $imageName;

}

/**
 * 从网上下载图片保存到服务器
 * @param $url 图片网址
 * @param $path 保存到服务器的路径
 */
function downloadImages($url,$path) {
    $img = getcurl($url);
    //保存的文件名称用的是链接里面的名称
    $fp = fopen($path,'w');
    fwrite($fp, $img);
    fclose($fp);
}

/**
 * 获取curl 返回数据
 * @param $url
 * @return bool|string
 */
function getcurl($url){
    $ch = curl_init ($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
    curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
    curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,false); // 绕过ssl
    $curl = curl_exec($ch);
    curl_close($ch);

    return $curl;
}

function replaceHtml($data){
    return preg_replace("/(\s|\&nbsp\;|　|\xc2\xa0)/", " ", strip_tags($data));
}


/**
 * @desc: 根据开始时间和每周的周期计算出天数范围内的所有时间段
 * @param: $start  开始时间
 * @param: $weeks  每周的星期 如每周的周一,周二  对应值 [1,2]
 * @parma: $days   需要计算出多少天
 * @return: Array
 */
function getWeekDays($start,$weeks = [],$days = 1,$end_time='')
{
    if(in_array(7, $weeks)){
        $weeks = array_merge(array_diff($weeks, array(7)));
        array_push($weeks,0);
    }
    //dd($weeks);
    $datas = [];
    $date = $start;
    $counts = 0;
    $last_time = '';
    while ($counts < $days) {
        if(!empty($end_time)){
            if($date >= $end_time){
                break;
            }
        }
        $week = date('w', strtotime($date));
        if (in_array($week, $weeks)) {
            $counts++;
            //array_push($datas, $date);
            $datas[] = [
              'data'=>$date,
              'w'=>$week
            ];
        }
        $date = date('Y-m-d', strtotime('+1 day', strtotime($date)));
    }
    return $datas;
}

/**
 * 获取两个时间之间的日期
 * @param $startDate
 * @param $endDate
 * @return array
 */
function getDatesBetweenTwoDays($startDate, $endDate)
{
    $dates = [];
    if (strtotime($startDate) > strtotime($endDate)) {
        // 如果开始日期大于结束日期，直接return 防止下面的循环出现死循环
        return $dates;
    } elseif ($startDate == $endDate) {
        // 开始日期与结束日期是同一天时
        array_push($dates, $startDate);
        return $dates;
    } else {
        array_push($dates, $startDate);
        $currentDate = $startDate;
        do {
            $nextDate = date('Y-m-d', strtotime($currentDate . ' +1 days'));
            array_push($dates, $nextDate);
            $currentDate = $nextDate;
        } while ($endDate != $currentDate);

        return $dates;
    }
}

/**
 * 二维数组根据某个字段排序
 * @param array $array 要排序的数组
 * @param string $keys   要排序的键字段
 * @param string $sort  排序类型  SORT_ASC     SORT_DESC
 * @return array 排序后的数组
 */
function arraySort($array, $keys, $sort = SORT_DESC) {
    $keysValue = [];
    foreach ($array as $k => $v) {
        $keysValue[$k] = $v[$keys];
    }
    array_multisort($keysValue, $sort, $array);
    return $array;
}

/**
 * 多维数组转化为一维数组
 * @param array $array 多维数组
 * @return array $result_array 一维数组
 */
function array_multi2single($array)
{
    //首先定义一个静态数组常量用来保存结果
    static $result_array = array();
    //对多维数组进行循环
    foreach ($array as $value) {
        //判断是否是数组，如果是递归调用方法
        if (is_array($value)) {
            array_multi2single($value);
        } else  //如果不是，将结果放入静态数组常量
            $result_array [] = $value;
    }
    //返回结果（静态数组常量）
    return $result_array;
}

function flareout_array($array) {

    $return = [];

    array_walk_recursive($array,function ($x) use (&$return) {

        $return[] = $x;
    });
    return $return;
}