<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserCheckMiddleware extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      // dd(Auth::guard('api')->user()->status);die;
        try{
            if (!$this -> auth -> parser() -> setRequest($request) -> hasToken()) {
                throw new \Exception('token不能为空');
            }

            if (!Auth::guard('api') -> check()) {
                throw new \Exception('token无效或已过期');
            }

            if (Auth::guard('api')->user()->status == 2) {
                // Authentication passed...
                throw new \Exception('禁止操作');
            }

            return $next($request);

        }catch (\Exception $e){
            return response() -> json([
                'status'=>false,
                'code' => 400,
                'msg' => $e -> getMessage()
            ]);
        }
    }
}
