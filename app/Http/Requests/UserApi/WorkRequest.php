<?php

namespace App\Http\Requests\UserApi;

//use Illuminate\Foundation\Http\FormRequest;

use App\Http\Requests\BaseRequest;

class WorkRequest extends BaseRequest
{
    protected $rules = [

    ];

    //这里我只写了部分字段，可以定义全部字段
    protected $strings_key = [
        'team_id' => '小组ID',
        'team_name' => '小组名称',
        'article_id' => '文章ID',
        'study_id' => '我的作业ID',
        'work_id' => '作业ID',
        'class_id' => '活动ID',
        'class_work_id' => '课堂任务表ID',
        'is_opend' => '是否公开',
        'key_id' => '外键ID',
        'type' => '小组类型',
        'start_time' => '开始时间',
        'end_time' => '结束时间',
        'book_id' => '书本Id',
    ];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $function = request()->route()->getAction();
        $rules = $this->rules;
        // 根据不同的情况, 添加不同的验证规则

        if ($function['controller'] == 'App\Http\Controllers\UserApi\WorkController@article_calss_work')
        {
            $rules=[
                'book_id'=>'required|exists:book,id',
                'key_id'=>'required',
            ];
        }

        if ($function['controller'] == 'App\Http\Controllers\UserApi\WorkController@company_read_work')
        {
            $rules=[
                //'article_id'=>'required|exists:article,id',
                'book_id'=>'required|exists:book,id',
                'key_id'=>'required',
            ];
        }

        if ($function['controller'] == 'App\Http\Controllers\UserApi\WorkController@personal_read_work')
        {
            $rules=[
               // 'article_id'=>'required|exists:article,id',
                'book_id'=>'required|exists:book,id',
                'key_id'=>'required',
            ];
        }

        if ($function['controller'] == 'App\Http\Controllers\UserApi\WorkController@add_work')
        {
            $rules=[
                'is_opend'=>'required',
                'class_work_id'=>'required|exists:class_work,id',
                'article_id'=>'required|exists:article,id',
               // 'team_id'=>'required|exists:activity_team,id',
            ];
        }

        if ($function['controller'] == 'App\Http\Controllers\UserApi\WorkController@edit_work')
        {
            $rules=[
                'study_id'=>'required|exists:study,id',
            ];
        }

        if ($function['controller'] == 'App\Http\Controllers\UserApi\WorkController@my_study_info')
        {
            $rules=[
                'study_id'=>'required|exists:study,id',
            ];
        }

        if ($function['controller'] == 'App\Http\Controllers\UserApi\WorkController@team_user_work')
        {
            $rules=[
                'team_id'=>'required|exists:activity_team,id',
            ];
        }

        if ($function['controller'] == 'App\Http\Controllers\UserApi\WorkController@study_del')
        {
            $rules=[
                'study_id'=>'required|exists:study,id',
            ];
        }

        if ($function['controller'] == 'App\Http\Controllers\UserApi\WorkController@study_sort')
        {
            $rules=[
                'type'=>'required',
            ];
        }

        if ($function['controller'] == 'App\Http\Controllers\UserApi\WorkController@company_work_record')
        {
            $rules=[
                'start_time'=>'required',
                'end_time'=>'required',
            ];
        }

        if ($function['controller'] == 'App\Http\Controllers\UserApi\WorkController@team_study_sort')
        {
            $rules=[
                'team_id'=>'required|exists:activity_team,id',
            ];
        }

        return $rules;

    }

}
