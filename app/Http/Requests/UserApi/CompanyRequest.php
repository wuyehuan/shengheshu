<?php

namespace App\Http\Requests\UserApi;

use App\Http\Requests\BaseRequest;
use Illuminate\Foundation\Http\FormRequest;

class CompanyRequest extends BaseRequest
{
    protected $rules = [

    ];

    //这里我只写了部分字段，可以定义全部字段
    protected $strings_key = [
        'company_name' => '公司名称',
        'employees_number' => '小组人数',
        'province' => '省',
        'city' => '市',
        'district' => '区/街道',
        'company_address' => '详细地址',
        'company_id' => '公司ID',
        'Invitation_code' => '企业邀请码',
    ];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $function = request()->route()->getAction();
        $rules = $this->rules;
        // 根据不同的情况, 添加不同的验证规则
        if ($function['controller'] == 'App\Http\Controllers\UserApi\CompanyController@create_company')
        {
            $rules=[
                'company_name'=>'required|unique:company,company_name',
                'employees_number'=>'integer',
            ];

        }
        if ($function['controller'] == 'App\Http\Controllers\UserApi\CompanyController@company_info')
        {
            $rules=[
                'company_id'=>'required|exists:company,id',
            ];

        }
        if ($function['controller'] == 'App\Http\Controllers\UserApi\CompanyController@bind_company')
        {
            $rules=[
                'Invitation_code'=>'required|exists:company,Invitation_code',
            ];

        }
        return $rules;
    }

}
