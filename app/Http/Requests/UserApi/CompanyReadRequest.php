<?php

namespace App\Http\Requests\UserApi;

use App\Http\Requests\BaseRequest;
//use Illuminate\Foundation\Http\FormRequest;

class CompanyReadRequest extends BaseRequest
{
    protected $rules = [

    ];

    //这里我只写了部分字段，可以定义全部字段
    protected $strings_key = [
        'company_id' => '公司ID',
        'company_read_id' => '企业活动ID',
    ];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $function = request()->route()->getAction();
        $rules = $this->rules;
        // 根据不同的情况, 添加不同的验证规则
        if ($function['controller'] == 'App\Http\Controllers\UserApi\CompanyReadController@company_read_list')
        {
            $rules=[
              //  'company_id'=>'required|exists:company,id',
            ];

        }
        if ($function['controller'] == 'App\Http\Controllers\UserApi\CompanyReadController@company_read_info')
        {
            $rules=[
                'company_read_id'=>'required|exists:company_read,id',
            ];

        }
        return $rules;
    }
}
