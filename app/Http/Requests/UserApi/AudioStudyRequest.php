<?php

namespace App\Http\Requests\UserApi;

//use Illuminate\Foundation\Http\FormRequest;

use App\Http\Requests\BaseRequest;

class AudioStudyRequest extends BaseRequest
{
    protected $rules = [

    ];

    //这里我只写了部分字段，可以定义全部字段
    protected $strings_key = [
        'audio_id' => '音频学习ID',
    ];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $function = request()->route()->getAction();
        $rules = $this->rules;
        // 根据不同的情况, 添加不同的验证规则
        if ($function['controller'] == 'App\Http\Controllers\UserApi\AudioStudyController@audio_info')
        {
            $rules=[
                'audio_id'=>'required|exists:audio_study,id',
            ];

        }
        return $rules;
    }
}
