<?php

namespace App\Http\Requests\UserApi;

//use Illuminate\Foundation\Http\FormRequest;

use App\Http\Requests\BaseRequest;

class CollectionRequest extends BaseRequest
{
    protected $rules = [

    ];

    //这里我只写了部分字段，可以定义全部字段
    protected $strings_key = [
        'article_id' => '文章ID',
        'collection_id' => '收藏ID',
    ];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $function = request()->route()->getAction();
        $rules = $this->rules;
        // 根据不同的情况, 添加不同的验证规则
        if ($function['controller'] == 'App\Http\Controllers\UserApi\CollectionController@create_collection')
        {
            $rules=[
                'article_id'=>'required|exists:article,id',
            ];

        }

        if ($function['controller'] == 'App\Http\Controllers\UserApi\CollectionController@del_collection')
        {
            $rules=[
                'collection_id'=>'required|exists:collection_article,id',
            ];

        }

        return $rules;
    }
}
