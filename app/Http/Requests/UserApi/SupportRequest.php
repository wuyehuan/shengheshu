<?php

namespace App\Http\Requests\UserApi;

use App\Http\Requests\BaseRequest;

class SupportRequest extends BaseRequest
{
    protected $rules = [

    ];
    //这里我只写了部分字段，可以定义全部字段
    protected $strings_key = [
        //'team_id' => '队伍id',
        'article_id' => '文章ID',
        'study_id' => '学习ID',
    ];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $function = request()->route()->getAction();
        $rules = $this->rules;
        // 根据不同的情况, 添加不同的验证规则
        if ($function['controller'] == 'App\Http\Controllers\UserApi\SupportController@create_support')
        {
            $rules=[
                'study_id'=>'required|exists:study,id',
            ];

        }

        return $rules;

    }
}
