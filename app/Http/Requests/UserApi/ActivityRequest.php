<?php

namespace App\Http\Requests\UserApi;

//use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\BaseRequest;

class ActivityRequest extends BaseRequest
{
    protected $rules = [

    ];

    //这里我只写了部分字段，可以定义全部字段
    protected $strings_key = [
        'activity_id' => '活动ID',
    ];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $function = request()->route()->getAction();
        $rules = $this->rules;
        // 根据不同的情况, 添加不同的验证规则
        if ($function['controller'] == 'App\Http\Controllers\UserApi\ActivityController@activity_info')
        {
            $rules=[
                'activity_id'=>'required|exists:activity,id',
            ];

        }
        return $rules;
    }
}
