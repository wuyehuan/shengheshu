<?php

namespace App\Http\Requests\UserApi;

use App\Http\Requests\BaseRequest;

class LoginRequest extends BaseRequest
{
    protected $rules = [

    ];
    //这里我只写了部分字段，可以定义全部字段
    protected $strings_key = [
        'account'=>'账号',
        'password'=>'密码',
        'wx_code'=>'微信code',
    ];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $function = request()->route()->getAction();
        $rules = $this->rules;
        // 根据不同的情况, 添加不同的验证规则
        if ($function['controller'] == 'App\Http\Controllers\UserApi\LoginController@login')
        {
            $rules=[
                'account' => 'required|exists:user,account',
                'password' => 'required|between:2,15',
            ];

        }
        if ($function['controller'] == 'App\Http\Controllers\UserApi\LoginController@wx_login')
        {
            $rules=[
                'wx_code'=>'required',
            ];
        }

        return $rules;

    }

}
