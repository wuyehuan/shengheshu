<?php

namespace App\Http\Requests\UserApi;


use App\Http\Requests\BaseRequest;

class PersonalReadRequest extends BaseRequest
{
    protected $rules = [

    ];

    //这里我只写了部分字段，可以定义全部字段
    protected $strings_key = [
        'personal_read_id' => '塾生学习ID',
        'company_name' => '公司名称',
        'phone' => '手机号',
    ];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $function = request()->route()->getAction();
        $rules = $this->rules;
        // 根据不同的情况, 添加不同的验证规则
        if ($function['controller'] == 'App\Http\Controllers\UserApi\PersonalReadController@personal_read_list')
        {
            $rules=[
                //'book_id'=>'required|exists:book,id',
            ];

        }
        if ($function['controller'] == 'App\Http\Controllers\UserApi\PersonalReadController@personal_read_info')
        {
            $rules=[
                'personal_read_id'=>'required|exists:personal_read,id',
            ];

        }
        if ($function['controller'] == 'App\Http\Controllers\UserApi\PersonalReadController@personal_read_register')
        {
            $rules=[
                'personal_read_id'=>'required|exists:personal_read,id',
                'company_name'=>'required',
                'phone'=>'required',
            ];

        }
        return $rules;
    }
}
