<?php

namespace App\Http\Requests\UserApi;

use App\Http\Requests\BaseRequest;

class SignRequest extends BaseRequest
{
    protected $rules = [

    ];
    //这里我只写了部分字段，可以定义全部字段
    protected $strings_key = [
        'team_id' => '队伍id',
        'activity_id' => '活动ID',
        'company_id' => '公司ID',
    ];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $function = request()->route()->getAction();
        $rules = $this->rules;
        // 根据不同的情况, 添加不同的验证规则
        if ($function['controller'] == 'App\Http\Controllers\UserApi\SignController@set_sign')
        {
            $rules=[
                'company_id'=>'required|exists:company,id',
            ];

        }
        if ($function['controller'] == 'App\Http\Controllers\UserApi\SignController@personal_sign')
        {
            $rules=[

            ];

        }
        if ($function['controller'] == 'App\Http\Controllers\UserApi\SignController@team_personal_sign')
        {
            $rules=[
                'team_id'=>'required|exists:team,id',
                //'activity_id'=>'required|exists:activity,id',
            ];

        }
        return $rules;

    }

}
