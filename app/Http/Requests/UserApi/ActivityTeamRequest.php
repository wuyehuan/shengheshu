<?php

namespace App\Http\Requests\UserApi;

use App\Http\Requests\BaseRequest;
use Illuminate\Foundation\Http\FormRequest;

class ActivityTeamRequest extends BaseRequest
{
    protected $rules = [

    ];

    //这里我只写了部分字段，可以定义全部字段
    protected $strings_key = [
        'team_id' => '小组ID',
        'team_name' => '小组名称',
        'max_number' => '允许进组最大人数',
        'team_number' => '当前小组人数',
        'company_id' => '公司ID',
        'class_id' => '期数ID',
        'team_type' => '小组类型',
        'team_logo' => '小组LOGO',
        'team_synopsis' => '小组简介',
        'team_phone' => '小组手机号',
        'team_contacts' => '联系人',
        'key_id' => '外键ID',
        'user_id' => '用户ID',
        'team_user_stash' => '审核状态',
        'is_Invisible' => '是否打卡小组',
        'Invitation_code' => '验证码',
    ];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $function = request()->route()->getAction();
        $rules = $this->rules;
        // 根据不同的情况, 添加不同的验证规则
        if ($function['controller'] == 'App\Http\Controllers\UserApi\ActivityTeamController@create_activity_team')
        {
            $rules=[
                'team_name'=>'required|unique:activity_team,team_name',
                'team_type'=>'required',
                'team_phone'=>'required',
                'team_contacts'=>'required',
                'key_id'=>'required',
            ];

        }

        if ($function['controller'] == 'App\Http\Controllers\UserApi\ActivityTeamController@add_activity_team')
        {
            $rules=[
                'team_id'=>'required|exists:activity_team,id',
                'is_Invisible'=>'required',
               // 'Invitation_code'=>'required',
            ];

        }

        if ($function['controller'] == 'App\Http\Controllers\UserApi\ActivityTeamController@team_list')
        {
            $rules=[
               // 'class_id'=>'required|exists:activity_class,id',
            ];

        }

        if ($function['controller'] == 'App\Http\Controllers\UserApi\ActivityTeamController@examine_team')
        {
            $rules=[
                'user_id'=>'required|exists:activity_team_user,user_id',
                'team_id'=>'required|exists:activity_team_user,team_id',
                'team_user_stash'=>'required',
            ];

        }

        if ($function['controller'] == 'App\Http\Controllers\UserApi\ActivityTeamController@team_list')
        {
            $rules=[
                //'team_type'=>'required|exists:activity_class,id',
            ];

        }

        if ($function['controller'] == 'App\Http\Controllers\UserApi\ActivityTeamController@out_team_user')
        {
            $rules=[
                'team_id'=>'required|exists:activity_team_user,team_id',
            ];

        }

        if ($function['controller'] == 'App\Http\Controllers\UserApi\ActivityTeamController@team_info')
        {
            $rules=[
                'team_id'=>'required|exists:activity_team,id',
            ];

        }

        if ($function['controller'] == 'App\Http\Controllers\UserApi\ActivityTeamController@edit_my_team_is_invisible')
        {
            $rules=[
                'team_id'=>'required|exists:activity_team_user,team_id',
                'key_id'=>'required|exists:activity_class,id',
            ];

        }

        return $rules;
    }

}
