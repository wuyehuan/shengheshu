<?php

namespace App\Http\Requests\UserApi;

use App\Http\Requests\BaseRequest;


class PersonalRequest extends BaseRequest
{
    protected $rules = [

    ];

    //这里我只写了部分字段，可以定义全部字段
    protected $strings_key = [
        'user_id'=>'用户ID',
        'nickname'=>'昵称',
        'avatar'=>'头像',
        'phone'=>'手机号',
    ];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $function = request()->route()->getAction();
        $rules = $this->rules;
        //others_index
        if ($function['controller'] == 'App\Http\Controllers\UserApi\PersonalController@others_index')
        {
            $rules=[
                'user_id'=>'required|exists:user,id',
            ];

        }
        if ($function['controller'] == 'App\Http\Controllers\UserApi\PersonalController@edit_user_info')
        {
            $rules=[
               // 'nickname'=>'required',
               // 'avatar'=>'required',
               // 'phone'=>'required',
            ];

        }

        return $rules;
    }
}
