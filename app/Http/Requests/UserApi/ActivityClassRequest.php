<?php

namespace App\Http\Requests\UserApi;

//use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\BaseRequest;

class ActivityClassRequest extends BaseRequest
{
    protected $rules = [

    ];

    protected $strings_key = [
        'class_id' => '六項精進ID',
        'Invitation_code' => '邀請碼',
       // 'class_id' => '六項精進ID不為空',
    ];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $function = request()->route()->getAction();
        $rules = $this->rules;
        // 根据不同的情况, 添加不同的验证规则
        if ($function['controller'] == 'App\Http\Controllers\UserApi\ActivityClassController@class_all_info')
        {
            $rules=[
                'class_id'=>'required|exists:activity_class,id',
            ];

        }
        if ($function['controller'] == 'App\Http\Controllers\UserApi\ActivityClassController@add_activity_register')
        {
            $rules=[
                'Invitation_code'=>'required|exists:activity_class,Invitation_code',
                'class_id'=>'required|exists:activity_class,id',
            ];
        }
        //add_activity_register
        return $rules;
    }
}
