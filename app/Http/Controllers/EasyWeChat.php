<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use EasyWeChat\Factory;

class EasyWeChat
{
    static function app(){
        $config = [
            'app_id' => config('wechat.wx_appid'),
            'secret' => config('wechat.wx_appsecret'),
            // 下面为可选项
            // 指定 API 调用返回结果的类型：array(default)/collection/object/raw/自定义类名
            'response_type' => 'array',

            'log' => [
                'level' => 'debug',
                'file' => __DIR__.'/wechat.log',
            ],
        ];

       return Factory::miniProgram($config);
    }

    public function Template($openid){
        $App = $this->app();
        $data = [
            'template_id' => 'hquj3fHjiKfl83HWOvC-sa0IBhTlmnCksu9GYsgvFYA', // 所需下发的订阅模板id
            'touser' =>$openid,     // 接收者（用户）的 openid
            'page' => '',       // 点击模板卡片后的跳转页面，仅限本小程序内的页面。支持带参数,（示例index?foo=bar）。该字段不填则模板无跳转。
            'data' => [         // 模板内容，格式形如 { "key1": { "value": any }, "key2": { "value": any } }
                'date01' => [
                    'value' => '2019-12-01',
                ],
                'number01' => [
                    'value' => 10,
                ],
            ],
        ];

       $res =  $App->subscribe_message->send($data);
       dd($res);
    }


}
