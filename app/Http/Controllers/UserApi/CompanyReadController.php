<?php

namespace App\Http\Controllers\UserApi;

use App\Http\Controllers\Controller;
use App\Models\CompanyReadModel;
//use Illuminate\Http\Request;
use App\Services\Api\CompanyReadService;
use App\Http\Requests\UserApi\CompanyReadRequest;

class CompanyReadController extends Controller
{
    public function __construct(CompanyReadService $companyReadService)
    {
        $this->middleware('user_check');
        $this->CompanyReadService = $companyReadService;
    }

    public function company_read_list(CompanyReadModel $companyReadModel,CompanyReadRequest $companyReadRequest){
        return $this->CompanyReadService->company_read_list($companyReadRequest,$companyReadModel);
    }

    public function company_read_info(CompanyReadModel $companyReadModel,CompanyReadRequest $companyReadRequest){
        return $this->CompanyReadService->company_read_info($companyReadRequest,$companyReadModel);
    }
}
