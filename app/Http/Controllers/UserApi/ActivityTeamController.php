<?php

namespace App\Http\Controllers\UserApi;

use App\Http\Controllers\Controller;
use App\Services\Api\ActivityTeamService;
use App\Http\Requests\UserApi\ActivityTeamRequest;
use App\Models\UserModel;
use App\Models\ActivityTeamModel;
use App\Models\ActivityTeamUser;
use App\Models\IntegralChange;

class ActivityTeamController extends Controller
{
    //中间件
    public function __construct(ActivityTeamService $teamService)
    {
        $this->middleware('user_check');
        $this->teamService = $teamService;
    }

    /**
     * @param ActivityTeamRequest $teamRequest
     * @param ActivityTeamModel $teamModel
     * @param UserModel $userModel
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create_activity_team(ActivityTeamRequest $teamRequest, ActivityTeamModel $teamModel, UserModel $userModel){
        $this->authorize('check_company',$userModel);
        return $this->teamService->create_activity_team($teamRequest,$teamModel,$userModel);
    }

    public function add_activity_team(ActivityTeamRequest $teamRequest, ActivityTeamUser $teamUser,UserModel $userModel){

        return $this->teamService->add_activity_team($teamRequest,$teamUser);
    }

    public function my_activity_team(ActivityTeamRequest $teamRequest, ActivityTeamUser $teamUser){
        return $this->teamService->my_activity_team($teamRequest->user()->id,$teamRequest->team_type,$teamRequest->team_user_stash,$teamUser);
    }

    public function team_list(ActivityTeamRequest $teamRequest,ActivityTeamModel $teamModel){
        return $this->teamService->team_list($teamRequest,$teamModel);
    }

    public function other_team_list(ActivityTeamRequest $teamRequest,ActivityTeamModel $teamModel){
        return $this->teamService->other_team_list($teamRequest,$teamModel);
    }

    public function team_info(ActivityTeamRequest $teamRequest,ActivityTeamModel $teamModel){
        return $this->teamService->team_info($teamRequest,$teamModel);
    }

    public function my_create_team(ActivityTeamRequest $teamRequest,ActivityTeamModel $teamModel){
        return $this->teamService->my_create_team($teamRequest,$teamModel);
    }

    public function examine_team(ActivityTeamRequest $teamRequest,ActivityTeamUser $teamUser, UserModel $userModel){
        $this->authorize('check_company',$userModel);
        return $this->teamService->examine_team($teamRequest,$teamUser);
    }

    public function out_team_user(ActivityTeamRequest $teamRequest,ActivityTeamUser $teamUser, UserModel $userModel){
        return $this->teamService->out_team_user($teamRequest,$teamUser);
    }

    public function integral_rank(ActivityTeamRequest $teamRequest,ActivityTeamModel $teamModel){
        return $this->teamService->integral_rank($teamRequest,$teamModel);
    }

    public function team_integral_rank(ActivityTeamRequest $teamRequest,ActivityTeamModel $teamModel){
        return $this->teamService->team_integral_rank($teamRequest,$teamModel);
    }

    public function edit_my_team_is_Invisible(ActivityTeamRequest $teamRequest,ActivityTeamUser $teamUser){
        return  $this->teamService->edit_my_team_is_Invisible($teamRequest,$teamUser);
    }

    public function all_team_list(ActivityTeamRequest $teamRequest,ActivityTeamModel $teamModel){
        return $this->teamService->all_team_list($teamRequest,$teamModel);
    }

}
