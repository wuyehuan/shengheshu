<?php

namespace App\Http\Controllers\UserApi;

use App\Http\Controllers\Controller;
use App\Services\Api\BookRecordService;
use App\Models\BookRecord;
use App\Http\Requests\UserApi\BookRecordRequest;

class BookRecordController extends Controller
{
    public function __construct(BookRecordService $bookRecordService)
    {
        $this->middleware('user_check');
        $this->bookService = $bookRecordService;
    }

    public function my_book_record(BookRecordRequest $bookRequest,BookRecord $bookRecord){
        return $this->bookService->my_book_record($bookRequest,$bookRecord);
    }
}
