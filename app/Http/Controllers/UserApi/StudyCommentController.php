<?php

namespace App\Http\Controllers\UserApi;

use App\Http\Controllers\Controller;
use App\Services\Api\StudyCommentService;
use App\Models\StudyComment;
use App\Http\Requests\UserApi\StudyCommentRequest;

class  StudyCommentController extends Controller
{
    public function __construct(StudyCommentService $studyCommentService)
    {
        $this->middleware('user_check');
        $this->studyCommentService = $studyCommentService;
    }

    public function create_study_comment(StudyCommentRequest $studyCommentRequest,StudyComment $studyComment){
        return $this->studyCommentService->create_study_comment($studyCommentRequest,$studyComment);
    }

    public function del_study_comment(StudyCommentRequest $studyCommentRequest,StudyComment $studyComment){
        return $this->studyCommentService->del_study_commnet($studyCommentRequest,$studyComment);
    }
}
