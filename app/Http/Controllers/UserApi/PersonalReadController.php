<?php

namespace App\Http\Controllers\UserApi;

use App\Http\Controllers\Controller;
use App\Models\PersonalRead;
use App\Models\UserModel;
use App\Services\Api\PersonalReadService;
use App\Http\Requests\UserApi\PersonalReadRequest;
use App\Models\PersonalReadRegister;
use App\Models\Phone;

class PersonalReadController extends Controller
{
    public function __construct(PersonalReadService $personalReadService)
    {
        $this->middleware('user_check');
        $this->personalReadService = $personalReadService;
    }

    public function personal_read_list(PersonalRead $personalReadModel,PersonalReadRequest $personalReadRequest){
       // $this->authorize('personal_read_register_policy',$personalReadModel);
        return $this->personalReadService->personal_read_list($personalReadModel,$personalReadRequest);
    }

    public function personal_read_info(
        PersonalRead $personalReadModel,
        PersonalReadRequest $personalReadRequest,
        PersonalReadRegister $personalReadRegister,
        UserModel $userModel
    )
    {
        //$this->authorize('personal_read_register_policy',$userModel,$personalReadRegister);
        return $this->personalReadService->personal_read_info($personalReadRequest,$personalReadModel,$personalReadRegister);
    }

    public function personal_read_register(PersonalReadRequest $personalReadRequest,PersonalReadRegister $personalReadRegister){
        return $this->personalReadService->personal_read_register($personalReadRequest,$personalReadRegister);
    }

    public function phone(Phone $phone){
        //return $phone->value('phone');
        return $this->personalReadService->phone($phone);
    }
}
