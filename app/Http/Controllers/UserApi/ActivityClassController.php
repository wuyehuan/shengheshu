<?php

namespace App\Http\Controllers\UserApi;

use App\Http\Controllers\Controller;
//use Illuminate\Http\Request;
use App\Models\ActivityClassModel;
use App\Models\CompanyReadModel;
use App\Models\PersonalRead;
use App\Services\Api\ActivityClassService;
use App\Http\Requests\UserApi\ActivityClassRequest;
use App\Models\AudioStudy;
use App\Models\ActivityRegister;
use App\Models\ActivityTeamModel;

class ActivityClassController extends Controller
{
    public function __construct(ActivityClassService $activityClassService)
    {
        $this->middleware('user_check');
        $this->activityClassService = $activityClassService;
    }

    /**
     * @param ActivityClassRequest $classRequest
     * @param ActivityClassModel $activityClassModel
     * @return \Illuminate\Http\JsonResponse
     */
    public function class_all_info(ActivityClassRequest $classRequest,ActivityClassModel $activityClassModel){
        return $this->activityClassService->class_all_info($classRequest->class_id,$activityClassModel,$classRequest->user()->id);
    }

    /**
     * @param ActivityClassModel $activityClassModel
     * @return mixed
     */
    public function class_list(ActivityClassRequest $classRequest,ActivityClassModel $activityClassModel){
        return $this->activityClassService->calss_list($classRequest,$activityClassModel);
    }

    public function activity_list(ActivityClassRequest $classRequest,
                                  ActivityClassModel $activityClassModel,
                                  CompanyReadModel $companyReadModel,
                                  PersonalRead $personalRead,AudioStudy $audioStudy){
        return $this->activityClassService->activity_list($classRequest,$activityClassModel,$companyReadModel,$personalRead,$audioStudy);
    }

    public function add_activity_register(ActivityClassRequest $classRequest,ActivityRegister $activityRegister,ActivityClassModel $activityClassModel,ActivityTeamModel $activityTeamModel){
        return $this->activityClassService->add_activity_register($classRequest,$activityRegister,$activityClassModel,$activityTeamModel);
    }

    public function my_activity_list(ActivityClassRequest $classRequest,
                                ActivityClassModel $activityClassModel,
                                AudioStudy $audioStudy,
                                PersonalRead $personalRead,
                                CompanyReadModel $companyReadModel)
    {
        return $this->activityClassService->my_activity_list($classRequest,$activityClassModel,$audioStudy,$personalRead,$companyReadModel);
    }
}
