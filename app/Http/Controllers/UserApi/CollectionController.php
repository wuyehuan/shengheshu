<?php

namespace App\Http\Controllers\UserApi;

use App\Http\Controllers\Controller;
//use Illuminate\Http\Request;
use App\Http\Requests\UserApi\CollectionRequest;
use App\Models\CollectionArticle;
use App\Services\Api\CollectionService;

class CollectionController extends Controller
{
    public function __construct(CollectionService $collectionService)
    {
        $this->middleware('user_check');
        $this->collectionService = $collectionService;
    }

    public function create_collection(CollectionRequest $request,CollectionArticle $collectionArticle){
        return $this->collectionService->create_collection($request,$collectionArticle);
    }

    public function my_collection(CollectionRequest $request,CollectionArticle $collectionArticle){
        return $this->collectionService->my_collection($request,$collectionArticle);
    }

    public function del_collection(CollectionRequest $request,CollectionArticle $collectionArticle){
        return $this->collectionService->del_collection($request,$collectionArticle);
    }

}
