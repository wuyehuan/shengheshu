<?php

namespace App\Http\Controllers\UserApi;

use App\Http\Controllers\Controller;

class AliYunOsssController extends Controller
{
    public function __construct()
    {
        $this->middleware('user_check');
    }

    public function aliyun_oss_config(){
        $upload = 'upload/'.date('Y-m-d').'/';
        return response()->json([
            'status'  => true,
            'code'    => 200,
            'message' => config('errorcode.code')[200],
            'data'    => [
                'access_id'=>config('alioss.aliyun_access_id'),
                'access_key'=>config('alioss.aliyun_access_key'),
                'oss_endpoint'=>config('alioss.aliyun_oss_endpoint'),
                'oss_bucket'=>config('alioss.aliyun_oss_bucket'),
                'upload_url'=>'https://'.config('alioss.aliyun_oss_bucket').'.'.config('alioss.aliyun_oss_endpoint'),
                'url'=>$upload,
            ],
        ]);
    }
}
