<?php

namespace App\Http\Controllers\UserApi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CompanyModel;
use App\Models\UserModel;
use App\Services\Api\CompanyService;
use App\Http\Requests\UserApi\CompanyRequest;

class CompanyController extends Controller
{
    public function __construct(CompanyService $companyService)
    {
        $this->middleware('user_check');
        $this->companyService = $companyService;
    }

    //公司认证
    public function create_company(CompanyRequest $request,CompanyModel $companyModel,UserModel $userModel){
        //$this->authorize('check_company',$userModel);
        return $this->companyService->create_company($request,$companyModel,$userModel);
    }

    public function bind_company(CompanyRequest $request,UserModel $userModel,CompanyModel $companyModel){
        return $this->companyService->bind_company($request,$companyModel,$userModel);
    }

    //公司详情
    public function company_info(CompanyRequest $request,CompanyModel $companyModel){
        return $this->companyService->company_info($request->company_id,$companyModel);
    }

}
