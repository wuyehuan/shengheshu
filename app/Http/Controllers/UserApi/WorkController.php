<?php

namespace App\Http\Controllers\UserApi;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserApi\WorkRequest;
use App\Services\Api\WorkService;
use App\Models\WorkModel;
use App\Models\StudyModel;
use App\Models\ClassWorkModel;

class WorkController extends Controller
{
    public function __construct(WorkService $workService)
    {
        $this->middleware('user_check');
        $this->workservice = $workService;
    }


    public function article_calss_work(WorkRequest $request,ClassWorkModel $classWorkModel){
        $class_type = 2;
        return $this->workservice->article_calss_work($request,$classWorkModel,$class_type);
    }

    public function company_read_work(WorkRequest $request,ClassWorkModel $classWorkModel){
        $class_type = 1;
        return $this->workservice->article_calss_work($request,$classWorkModel,$class_type);
    }

    public function personal_read_work(WorkRequest $request,ClassWorkModel $classWorkModel){
        $class_type = 3;
        return $this->workservice->article_calss_work($request,$classWorkModel,$class_type);
    }

    public function audio_study_work(WorkRequest $request,ClassWorkModel $classWorkModel){
        $class_type = 4;
        return $this->workservice->article_calss_work($request,$classWorkModel,$class_type);
    }

    /**
     * @param WorkRequest $request
     * @param StudyModel $studyModel
     * @return mixed
     */
    public function add_work(WorkRequest $request,StudyModel $studyModel){
        return $this->workservice->add_work($request,$studyModel);
    }

    public function edit_work(WorkRequest $request,StudyModel $studyModel){
        return $this->workservice->edit_work($request,$studyModel);
    }

    public function my_work(WorkRequest $request,StudyModel $studyModel){
        return $this->workservice->my_work($request,$studyModel);
    }

    public function team_user_work(WorkRequest $request,StudyModel $studyModel){
        return $this->workservice->team_user_work($request,$studyModel);
    }

    public function my_finish_work(WorkRequest $request,StudyModel $studyModel){
        return $this->workservice->my_finish_work($request,$studyModel);
    }

    public function my_study_info(WorkRequest $request,StudyModel $studyModel){
        return $this->workservice->my_study_info($request,$studyModel);
    }

    public function study_list(WorkRequest $request,StudyModel $studyModel){
        return $this->workservice->study_list($request,$studyModel);
    }

    public function study_del(WorkRequest $request,StudyModel $studyModel){
        return $this->workservice->study_del($request,$studyModel);
    }

    public function study_sort(WorkRequest $request,StudyModel $studyModel){
        return $this->workservice->study_sort($request,$studyModel);
    }

    public function team_study_sort(WorkRequest $request,StudyModel $studyModel){
        return $this->workservice->team_study_sort($request,$studyModel);
    }

    public function company_work_record(WorkRequest $request,StudyModel $studyModel){
        return $this->workservice->company_work_record($request,$studyModel);
    }

    public function company_read_work_record(WorkRequest $request,StudyModel $studyModel){
        return $this->workservice->company_read_work_record($request,$studyModel);
    }

    public function personal_read_work_record(WorkRequest $request,StudyModel $studyModel){
        return $this->workservice->personal_read_work_record($request,$studyModel);
    }

    public function my_personal_work(WorkRequest $request,StudyModel $studyModel){
        return $this->workservice->my_personal_work($request,$studyModel);
    }
}
