<?php

namespace App\Http\Controllers\UserApi;

use App\Http\Controllers\Controller;
use App\Services\Api\SignService;
use App\Models\SetSignModel;
use App\Models\SignModel;
use App\Models\SignTeamModel;
use App\Http\Requests\UserApi\SignRequest;
use App\Models\IntegralChange;

class SignController extends Controller
{
    public function __construct(SignService $signService)
    {
        $this->middleware('user_check');
        $this->signService = $signService;
    }

    public function set_sign(SignRequest $signRequest,SetSignModel $setSignModel){
        return $this->signService->set_sign($signRequest,$setSignModel);
    }

    public function personal_sign(SignRequest $signRequest,SignModel $signModel){
        //dd($signRequest->user());die;
        return $this->signService->personal_sign($signRequest,$signModel);
    }

    public function team_personal_sign(SignRequest $signRequest,SignTeamModel $signTeamModel){
        return $this->signService->team_personal_sign($signRequest,$signTeamModel);
    }

    public function sign_team_order(SignRequest $signRequest,SignTeamModel $signTeamModel){
        return $this->signService->sign_team_order($signRequest,$signTeamModel);
    }

    public function sign_user_order(SignRequest $signRequest,SignModel $signModel){
        return $this->signService->sign_user_order($signRequest,$signModel);
    }

    public function integral_record(SignRequest $signRequest,IntegralChange $integralChange){
        return $this->signService->integral_record($signRequest,$integralChange);
    }
}
