<?php

namespace App\Http\Controllers\UserApi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Api\LoginService;
use App\Http\Requests\UserApi\LoginRequest;
use App\Models\UserModel;

class  LoginController extends Controller
{
    //账号登录接口
    public function login(LoginRequest $request,LoginService $loginService){
        //$this->authorize('check_stash',$userModel);
        return $loginService->account_login($request->post());
    }

    //微信登录接口
    public function wx_login(LoginRequest $request,LoginService $loginService,UserModel $userModel){
        return $loginService->wx_login($request->post(),$userModel);
    }

}
