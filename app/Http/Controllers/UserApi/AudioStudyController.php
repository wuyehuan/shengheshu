<?php

namespace App\Http\Controllers\UserApi;

use App\Http\Controllers\Controller;
//use Illuminate\Http\Request;
use App\Services\AudioStudyService;
use App\Models\AudioStudy;
use App\Http\Requests\UserApi\AudioStudyRequest;

class AudioStudyController extends Controller
{
    public function __construct(AudioStudyService $audioStudyService)
    {
        $this->middleware('user_check');
        $this->audioStudyService = $audioStudyService;
    }

    public function audio_study_list(AudioStudyRequest $request,AudioStudy $audioStudy){
        return $this->audioStudyService->audio_study_list($request,$audioStudy);
    }

    public function audio_info(AudioStudyRequest $request,AudioStudy $audioStudy){
        return $this->audioStudyService->audio_info($request,$audioStudy);
    }
}
