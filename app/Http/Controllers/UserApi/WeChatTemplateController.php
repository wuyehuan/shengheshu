<?php

namespace App\Http\Controllers\UserApi;

use App\Http\Controllers\Controller;
//use Illuminate\Http\Request;
use App\Services\Api\WeChatTemplateService;
use App\Http\Requests\UserApi\WeChatTemplateRequest;

class WeChatTemplateController extends Controller
{
    public function __construct(WeChatTemplateService $chatTemplateService)
    {
        $this->middleware('user_check');
        $this->chatTemplateService = $chatTemplateService;
    }

    public function wx_remind(WeChatTemplateRequest $request){

    }
}
