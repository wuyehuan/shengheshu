<?php

namespace App\Http\Controllers\UserApi;

use App\Http\Controllers\Controller;
use App\Http\Controllers\EasyWeChat;
use Illuminate\Http\Request;
use App\Services\Api\IndexService;
use App\Models\BannerModel;
use App\Models\Poster;
use App\Models\ActivityCove;
use App\Models\BookshelfGuide;
use App\Models\SystemImages;

class  IndexController extends Controller
{
    public function __construct(IndexService $indexService)
    {
        $this->indexService = $indexService;
    }

    //轮播图
    public function banner(BannerModel $bannerModel){
        return $this->indexService->banner($bannerModel);
    }

    //海报
    public function poster(Poster $poster){
        return $this->indexService->poster($poster);
    }

    public function activity_coves(Request $request,ActivityCove $activityCove){
        //dd($request->type);
        return  $this->indexService->activity_coves($request->type,$activityCove);
    }

    public function bookshelf_guide(BookshelfGuide $bookshelfGuide){
        return  $this->indexService->bookshelf_guide($bookshelfGuide);
    }

    public function test(){
        $app = new EasyWeChat();
        $app->Template('o3cmg4muK638zF3Z-kwp3096WAdE');
    }

    public function system_images(Request $request,SystemImages $images){
        return $this->indexService->system_images($images,$request->input('type'));
    }
}
