<?php

namespace App\Http\Controllers\UserApi;

use App\Http\Controllers\Controller;
//use Illuminate\Http\Request;
use App\Http\Requests\UserApi\SupportRequest;
use App\Models\Support;

class SupportController extends Controller
{
    public function __construct()
    {
        $this->middleware('user_check');
    }

    public function create_support(SupportRequest $request,Support $support){
        $check = $support->check_support($request->study_id,$request->user()->id);
        if(!empty($check)){
            return response()->json([
                'status'  => false,
                'code'    => 405,
                'message' => '请勿重复点赞',
                'data'    => '',
            ]);
        }
        $data = $request->post();
        //dd($data);die;
        $data['user_id'] = $request->user()->id;
        $support->create_support($data);
        return response()->json([
            'status'  => true,
            'code'    => 200,
            'message' => config('errorcode.code')[200],
            'data'    => '',
        ]);
    }

   public function del_support(SupportRequest $request,Support $support){
       $check = $support->check_support($request->study_id,$request->user()->id);
       if(empty($check)){
           return response()->json([
               'status'  => false,
               'code'    => 405,
               'message' => '没有找到记录',
               'data'    => '',
           ]);
       }
       $res = $support
           ->where('study_id',$request->study_id)
           ->where('user_id',$request->user()->id)
           ->delete();
       if($res){
           return response()->json([
               'status'  => true,
               'code'    => 200,
               'message' => '删除成功',
               'data'    => '',
           ]);
       }
       return response()->json([
           'status'  => false,
           'code'    => 405,
           'message' => '删除失败',
           'data'    => '',
       ]);
   }

}
