<?php

namespace App\Http\Controllers\UserApi;

use App\Http\Controllers\Controller;

use App\Http\Requests\UserApi\ArticleRequest;
use App\Services\Api\ArticleService;
use App\Models\ArticleModel;

class ArticleController extends Controller
{
    public function __construct(ArticleService $articleService)
    {
        $this->middleware('user_check',['except' => 'activity_list']);
        $this->articleService = $articleService;
    }

    //文章详情
    public function article_info(ArticleRequest $request,ArticleModel $articleModel){
       return $this->articleService->article_info($request,$articleModel);
    }

}
