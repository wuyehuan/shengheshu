<?php

namespace App\Http\Controllers\UserApi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\BookModel;
use App\Services\Api\BookService;
use App\Http\Requests\UserApi\BookRequest;
use App\Models\BookShelf;
use App\Models\BookShelfRegister;
use App\Models\BookShelfCode;

class BookController extends Controller
{
    public function __construct(BookService $bookService)
    {
        $this->middleware('user_check');
        $this->bookService = $bookService;
    }

    //获取书本详情
    public function book_info(BookRequest $request,BookModel $bookModel){
        //dd($request);
        return $this->bookService->book_info($request,$bookModel);
    }

    //获取书本下面的文章
    public function book_base_info(BookRequest $request,BookModel $bookModel){
        return $this->bookService->book_base_info($request->book_id,$bookModel);
    }

    public function my_bookshelf(BookRequest $request,BookShelf $bookShelfModel){
        return $this->bookService->my_bookshelf($request,$bookShelfModel);
    }

    public function bookshelf_register(BookShelfRegister $bookShelfRegister,BookRequest $request,BookShelfCode $bookShelfCode){
        $code = $bookShelfCode->value('code');

        if($request->code == $code){
            $my_register = $bookShelfRegister->where('user_id',$request->user()->id)->first();
            if(!empty($my_register)){
                return response()->json([
                    'status'  => false,
                    'code'    => 500,
                    'message' => '请勿重复验证',
                    'data'    => '',
                ]);
            }
            $bookShelfRegister->insert([
                'user_id'=>$request->user()->id,
                    ]
            );
            return response()->json([
                'status'  => false,
                'code'    => 200,
                'message' => '验证成功',
                'data'    => '',
            ]);
        }
        return response()->json([
            'status'  => false,
            'code'    => 500,
            'message' => '验证错误',
            'data'    => '',
        ]);
    }
}
