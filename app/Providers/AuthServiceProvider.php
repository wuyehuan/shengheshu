<?php

namespace App\Providers;

use App\Models\PersonalReadRegister;
use App\Models\TeamModel;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use App\Models\UserModel;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        //Post::class => PostPolicy::class,
        UserModel::class => 'App\Policies\UserModelPolicy',
        TeamModel::class => 'App\Policies\TeamModelPolicy',
        PersonalReadRegister::class => 'App\Policies\PersonalReadRegisterPolicy'
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
