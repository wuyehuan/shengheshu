<?php

namespace App\Policies;

//use App\User;
use App\Models\UserModel;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserModelPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    //验证管理员状态
    public function check_company(UserModel $userModel){
        //dd('1111');die;
        return policyMessageFormat($userModel->identity == 2 && $userModel->identity_status == 3,'无权操作，你不是企业管理员',100002);
    }

    //验证普通会员验证状态
    public function check_user_company(UserModel $userModel){
        return policyMessageFormat(!empty($userModel->company_id) && $userModel->identity == 3 && $userModel->identity_status == 3,'无权操作,请绑定企业',100004);
    }
}
