<?php

namespace App\Policies;

use App\Models\TeamModel;
use App\Models\UserModel;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class TeamModelPolicy
{
    use HandlesAuthorization;

    public function create_team_user(TeamModel $teamModel,UserModel $userModel){
        return policyMessageFormat($userModel->identity == 2 && identity_status == 3,'无权操作',100002);
    }

    public function add_team_user(TeamModel $teamModel,UserModel $userModel){
        return policyMessageFormat(!empty($userModel->company_id) ,'无权操作,请加入企业',100002);
    }
}
