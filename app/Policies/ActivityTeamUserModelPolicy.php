<?php

namespace App\Policies;

use App\Models\UserModel;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class ActivityTeamUserModelPolicy
{
    use HandlesAuthorization;

    public function create_team_user(TeamModel $teamModel,UserModel $userModel){
        return policyMessageFormat($userModel->identity == 2 && identity_status == 3,'无权操作',100002);
    }

}
