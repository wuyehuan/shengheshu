<?php

Route::get('/', 'HomeController@index');

//企业读书活动
Route::resource('company-read-models', CompanyReadController::class);
//企业读书作业
Route::resource('company-read-class-work-models', CompanyReadWorkController::class);

//书本模块
Route::resource('book-models', BookController::class);

//文章模块
Route::resource('article-models', ArticleController::class);
//作业模块
Route::resource('work-models', WorkController::class);
Route::resource('banner-models', BannerController::class);

//六项精进
Route::resource('activity-class-models', ActivityClassController::class);

//绑定作业
Route::resource('class-work-models', ClassWorkController::class);

Route::resource('company-models', CompanyController::class);
Route::resource('user-models', CompanyUserController::class);

//小组成员管理
Route::resource('activity-team-users', TeamUserController::class);
//小组
Route::resource('activity-team-models', TeamController::class);
//音频学习
Route::resource('audio-studies', AudioStudyController::class);
//音频学习任务
Route::resource('audio-work-models', AudioStudyWorkController::class);
//用户管理
Route::resource('user-models', UserController::class);
//心得列表
Route::resource('study-models', StudyController::class);

