<?php

namespace App\Admin\Controllers;

use App\Models\ActivityTeamModel;
use App\Models\CompanyModel;
use App\Models\UserModel;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Tests\Models\User;

class TeamController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '小组管理';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ActivityTeamModel());

        $grid->disableCreateButton();
        $grid->model()->where('team_type','=','2');

        $grid->column('id', __('Id'));

        $grid->column('team_logo', '小组头像')->image('',200,200);

        $grid->column('team_name', '小组名称');
        $grid->column('max_number','允许最大人数 ');
        $grid->column('team_number','当前人数 ');
        $grid->column('company_id', '公司')->display(function($company_id) {
            return CompanyModel::find($company_id)->company_name;
        });

        //$grid->column('key_id','对应活动');
        $grid->column('team_stash', '状态')->using([
            1=>'正常',
            2=>'停用',
        ])->label([
            1 => 'success',
            2 => 'warning',
        ]);

       //$grid->column('team_type', __('Team type'));
        $grid->column('team_integral','小组总分');

        $grid->column('user_id', '用户')->display(function($user_id) {
            return UserModel::find($user_id)->nickname;
        });


       // $grid->column('team_synopsis', '小组简介');
        $grid->column('team_phone', '联系人手机');
        $grid->column('team_contacts', '联系人');

        $grid->column('created_at','创建时间');
        //$grid->column('updated_at','更新时间');
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(ActivityTeamModel::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('team_name', '小组名称');
        $show->field('max_number','允许进组最大人数 ');
        $show->field('team_number','当前小组人数 ');
        $show->field('company_id', '公司')->as(function ($company_id) {

            return CompanyModel::find($company_id)->company_name;

        });
        $show->field('user_id', '用户')->as(function ($user_id) {

            return UserModel::find($user_id)->nickname;

        });
        $show->field('key_id','对应活动');
        $show->field('team_stash', '队伍状态')->using([
            1=>'正常',
            2=>'停用',
        ]);
        //$show->field('team_type', __('Team type'));
        $show->field('team_integral','小组总分');

        $show->field('team_logo', '小组头像')->image('',500,500);
        $show->field('team_synopsis', '小组简介');
        $show->field('team_phone', '小组联系人手机');
        $show->field('team_contacts', '小组联系人');
        //team_user
        $show->team_user('小组成员', function ($comments) {

            $comments->resource('/admin/activity-team-users');
            $comments->disableCreateButton();
            //$comments->id();
            $comments->is_admin('是否是管理员')->using([
                1=>'是',
                2=>'否',
            ]);
            $comments->name('姓名');
            $comments->phone('手机号码');
            $comments->company_name('公司名称');
            $comments->team_user_stash('审核状态')->using([
                0=>'未审核',
                1=>'通过',
                2=>'不通过',
            ])->label([
                0=>'default',
                1 => 'success',
                2 => 'warning',
            ]);

        });
        $show->field('created_at','创建时间');
        $show->field('updated_at','更新时间');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new ActivityTeamModel());

        $form->text('team_name', '小组名称');
        $form->number('max_number','允许进组最大人数 ')->default(1);
        $form->number('team_number','当前小组人数 ')->default(1);
/*        $form->select('company_id', '公司')->options(
            CompanyModel::pluck('company_name','id')
        );*/
 /*       $form->number('user_id', '用户');
        $form->number('key_id','对应活动')*/;
        $form->select('team_stash', '队伍状态')
            ->options([
                1=>'正常',
                2=>'停用'
            ])
            ->default(1);

        $form->hasMany('team_user', '小组成员', function (Form\NestedForm $form) {
            $form->disableCreateButton();
            $form->text('name','姓名');
            $form->text('phone','手机号');
            //$form->text('key_id');
            //$form->text('team_id');
            $form->text('user_id','用户ID');
            //$form->text('team_type','');
            $form->select('team_user_stash','审核状态')->options([
                0=>'未审核',
                1=>'通过',
                2=>'不通过',
            ]);
            $form->text('company_name','公司名称');
            //$form->text('is_admin');

        });

        //$form->decimal('team_type', __('Team type'))->default(2);
/*        $form->number('team_integral','小组总分');
        $form->text('team_logo', '小组头像');
        $form->textarea('team_synopsis', '小组简介');
        $form->text('team_phone', '小组联系人手机');
        $form->text('team_contacts', '小组联系人');*/

        // 可以使用第二个参数设置label
/*        $form->hasMany('team_user', '小组成员', function (Form\NestedForm $form) {
           $form->text('name');
           $form->text('phone');

        });*/
        return $form;
    }
}
