<?php

namespace App\Admin\Controllers;

use App\Models\ActivityClassModel;
use App\Models\ArticleModel;
use App\Models\ClassWorkModel;
use App\Models\WorkModel;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class ClassWorkController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '布置任务(六项精进)';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ClassWorkModel());

        $grid->model()->where('class_type', '=', 2);

        $grid->column('id', __('Id'));
        $grid->column('article_id', '文章')->display(function ($article_id) {
           // return $this->first_name.' '.$this->last_name;
            return ArticleModel::where('id',$article_id)->value('article_title');
        });
        //$grid->column('key_id', __('Key id'));
        $grid->column('work_id', '作业')->display(function ($work_id) {
            return WorkModel::where('id',$work_id)->value('title');
        });

        $grid->column('activity_class_id', '活动')->display(function ($activity_class_id) {
            return ActivityClassModel::where('id',$activity_class_id)->value('class_name');
        });

        $grid->column('class_type','参加类型')->using([2=>'六项精进',1=>'企业读书',3=>'塾生学习']);
        $grid->column('type', '作业类型')->using([1=>'心得(模板格式,多个标题)',2=>'读原文\录音',3=>'心得(单个标题)']);

        $grid->column('created_at','创建时间');
        $grid->column('updated_at','更新时间');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(ClassWorkModel::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('article_id', '文章')->as(function ($article_id) {
            return ArticleModel::where('id',$article_id)->value('article_title');
        });
       // $show->field('key_id', __('Key id'));
        $show->field('work_id', '作业')->as(function ($work_id) {
            return WorkModel::where('id',$work_id)->value('title');
        });

        $show->field('activity_class_id', '活动')->as(function ($activity_class_id) {
            return ActivityClassModel::where('id',$activity_class_id)->value('class_name');
        });

        $show->field('class_type','参加类型')->using([2=>'六项精进',1=>'企业读书',3=>'塾生学习']);
        $show->field('type', '作业类型')->using([1=>'心得(模板格式,多个标题)',2=>'读原文\录音',3=>'心得(单个标题)']);
        $show->field('created_at','创建时间');
        $show->field('updated_at','更新时间');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new ClassWorkModel());

        $form->select('activity_class_id', '活动')->options(
            ActivityClassModel::orderBy('created_at','desc')->pluck('class_name as text','id')
        )->load('article_id', '/admin/class_article');

        $form->select('article_id', '文章')
            ->options(
                ArticleModel::whereIn('type',[1,2])->pluck('article_title as text','id')
            );

        $form->select('type', '作业类型')->options([
            1=>'心得(模板格式)',
            2=>'读原文\录音',
            3=>'心得（富文本)',
        ])->load('work_id', '/admin/get_work');

        $form->select('work_id', '作业')->options(
            WorkModel::pluck('title as text','id')
        );

        $form->hidden('key_id');
        $form->hidden('class_type');

        //保存前回调
        $form->saving(function (Form $form) {
            //dd($form->article_id,$form->activity_class_id);
            $form->key_id = $form->activity_class_id;
            $form->class_type = 2;

            //检查重复
   /*         $check_un = \DB::table('class_work')
                ->where('article_id',$form->article_id)*/
        });

        return $form;
    }
}
