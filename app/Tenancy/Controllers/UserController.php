<?php

namespace App\Admin\Controllers;

use App\Models\CompanyModel;
use App\Models\UserModel;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class UserController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '用户管理';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new UserModel());

        $grid->model()->where('id', '>', 100);

        $grid->column('id', __('Id'));
       // $grid->column('account', __('Account'));
        //$grid->column('password', __('Password'));
        $grid->column('nickname', '昵称');
        $grid->column('avatar','头像')->image('',200,200);
        $grid->column('name','姓名');
        //$grid->column('wx_openid', __('Wx openid'));
        $grid->column('phone','手机号');
        //$grid->column('mail', '邮箱');
        $grid->column('status', '账号状态')->using([
            1=>'正常',
            2=>'封停',
        ]);
        $grid->column('identity', '身份')->using([
            1=>'游客',
            2=>'企业验证负责人',
            3=>'企业员工',
        ]);
        $grid->column('identity_status', '身份审核')->using([
            1=>'未审核',
            2=>'审核中',
            3=>'审核通过',
            4=>'审核不通过',
        ]);
        $grid->column('last_used_at', '最后登录时间');
        $grid->column('company_id', '绑定的企业ID')->display(function ($company_id) {
            return CompanyModel::where('id',$company_id)->value('company_name');
        });

        //$grid->column('Invitation_code', '企业邀请码');
        $grid->column('total_integral', '总积分');
        $grid->column('created_at', '创建时间');
        $grid->column('updated_at','更新时间');

        //$grid->column('user_integral','用户个人积分');
        //$grid->column('activity_team_integral', '用户团队积分(六项精进活动)');
        //$grid->column('company_read_integral', '用户团队积分(企业读书活动)');
        //$grid->column('personal_read_integral','用户团队积分（塾生学习)');
        //$grid->column('company_account', '后台账号');
        //$grid->column('company_password', '后台密码');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(UserModel::findOrFail($id));

        $show->field('id', __('Id'));
        //$show->field('account', __('Account'));
        //$show->field('password', __('Password'));
        $show->field('nickname', '昵称');
        $show->field('avatar','头像')->image('',200,200);
        $show->field('name','姓名');
        //$show->field('wx_openid', __('Wx openid'));
        $show->field('phone','手机号');
        $show->field('mail', '邮箱');
        $show->field('status', '账号状态')->using([
            1=>'正常',
            2=>'封停',
        ]);
        $show->field('identity', '身份')->using([
            1=>'游客',
            2=>'企业验证负责人',
            3=>'企业员工',
        ]);

        $show->field('identity_status', '身份审核')->using([
            1=>'未审核',
            2=>'审核中',
            3=>'审核通过',
            4=>'审核不通过',
        ]);

        $show->field('last_used_at', '最后登录时间');
        $show->field('company_id', '绑定的企业ID');
        $show->field('Invitation_code', '企业邀请码');
        $show->field('created_at', '创建时间');
        $show->field('updated_at','更新时间');
        $show->field('total_integral', '总积分');
        $show->field('user_integral','用户个人积分');
        $show->field('activity_team_integral', '用户团队积分(六项精进活动)');
        $show->field('company_read_integral', '用户团队积分(企业读书活动)');
       // $show->field('personal_read_integral','用户团队积分（塾生学习)');
        $show->field('company_account', '后台账号');
        $show->field('company_password', '后台密码');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new UserModel());

        //$form->text('account', __('Account'));
        //$form->password('password', __('Password'));
        $form->text('nickname', '昵称');
        $form->image('avatar','头像')
            ->move('/upload/images/'.date('Ymd'))
            ->uniqueName();
        $form->text('name','姓名');
        //$form->text('wx_openid', __('Wx openid'));
        $form->mobile('phone','手机号');
        $form->email('mail', '邮箱');
        $form->select('status', '账号状态')
            ->options([
                1=>'正常',
                2=>'封停',
            ])
            ->default(1);
        $form->select('identity', '身份')
            ->options([
                1=>'游客',
                2=>'企业验证负责人',
                3=>'企业员工',
            ])
            ->default(1);
        $form->select('identity_status', '身份审核')
            ->options([
                1=>'未审核',
                2=>'审核中',
                3=>'审核通过',
                4=>'审核不通过',
            ])
            ->default(1);
        $form->datetime('last_used_at', '最后登录时间')->default(date('Y-m-d H:i:s'));
        $form->number('company_id', '绑定的企业ID');
        $form->text('Invitation_code', '企业邀请码');
        $form->number('total_integral', '总积分');
        $form->number('user_integral','用户个人积分');
        $form->number('activity_team_integral', '用户团队积分(六项精进活动)');
        $form->number('company_read_integral', '用户团队积分(企业读书活动)');
       // $form->number('personal_read_integral','用户团队积分（塾生学习)');
        $form->text('company_account', '后台账号');
        $form->text('company_password', '后台密码');

        return $form;
    }
}
