<?php

namespace App\Tenancy\Controllers;

use App\Models\AudioStudy;
use App\Models\ClassWorkModel;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Models\ArticleModel;
use App\Models\WorkModel;
use Illuminate\Support\Facades\Auth;

class AudioStudyWorkController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '布置任务(音频学习)';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ClassWorkModel());

        $admin = Auth::guard('admin')->user()->toArray();
        //查询出ids
        $audio_study_ids = AudioStudy::where('company_id',$admin['company_id'])->pluck('id')->toArray();

        $grid->model()
            ->whereIn('audio_study_id',$audio_study_ids)
            ->where('class_type', '=', 4);

        $grid->column('id', __('Id'));
        $grid->column('article_id', '文章')->display(function ($article_id) {
            // return $this->first_name.' '.$this->last_name;
            return ArticleModel::where('id',$article_id)->value('article_title');
        });
        //$grid->column('key_id', __('Key id'));
        $grid->column('work_id', '作业')->display(function ($work_id) {
            return WorkModel::where('id',$work_id)->value('title');
        });

        $grid->column('audio_study_id', '活动')->display(function ($audio_study_id) {
            return AudioStudy::where('id',$audio_study_id)->value('title');
        });

        $grid->column('class_type','参加类型')->using([2=>'六项精进',1=>'企业读书',3=>'塾生学习',4=>'音频学习']);
        $grid->column('type', '作业类型')->using([1=>'心得(模板格式,多个标题)',2=>'读原文\录音',3=>'心得(单个标题)']);

        $grid->column('created_at','创建时间');
        $grid->column('updated_at','更新时间');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(ClassWorkModel::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('article_id', '文章')->as(function ($article_id) {
            return ArticleModel::where('id',$article_id)->value('article_title');
        });
        // $show->field('key_id', __('Key id'));
        $show->field('work_id', '作业')->as(function ($work_id) {
            return WorkModel::where('id',$work_id)->value('title');
        });

        $show->field('audio_study_id', '活动')->as(function ($audio_study_id) {
            return AudioStudy::where('id',$audio_study_id)->value('title');
        });

        $show->field('class_type','参加类型')->using([2=>'六项精进',1=>'企业读书',3=>'塾生学习',4=>'音频学习']);
        $show->field('type', '作业类型')->using([1=>'心得(模板格式,多个标题)',2=>'读原文\录音',3=>'心得(单个标题)']);
        $show->field('created_at','创建时间');
        $show->field('updated_at','更新时间');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new ClassWorkModel());

        $form->select('audio_study_id', '活动')->options(
            AudioStudy::orderBy('created_at','desc')->pluck('title as text','id')
        )->load('article_id', '/admin/audio_article');

        $form->select('article_id', '文章')
            ->options(
                ArticleModel::/*whereIn('type',[1,2])->*/pluck('article_title as text','id')
            );

        $form->select('type', '作业类型')->options([
            1=>'心得(模板格式,多个标题)',
            2=>'读原文\录音',
            3=>'心得（富文本,单个标题)',
        ])->load('work_id', '/admin/get_work');

        $form->select('work_id', '作业')->options(
            WorkModel::pluck('title as text','id')
        );

        $form->hidden('key_id');
        $form->hidden('class_type');

        //保存前回调
        $form->saving(function (Form $form) {
            //dd($form->article_id,$form->activity_class_id);
            $form->key_id = $form->audio_study_id;
            $form->class_type = 4;
        });

        return $form;
    }
}
