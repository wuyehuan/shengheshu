<?php

namespace App\Admin\Controllers;

use App\Models\ActivityClassModel;
use App\Models\ArticleModel;
use App\Models\CompanyReadModel;
use App\Models\PersonalRead;
use App\Models\StudyModel;
use App\Models\UserModel;
use App\Models\WorkModel;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class StudyController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '心得列表';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new StudyModel());
        $grid->disableCreateButton();
        $grid->column('id', __('Id'));
        $grid->column('user_id', '用户')->display(function ($user_id){
            return UserModel::where('id',$user_id)->value('nickname');
        });
        $grid->column('key_id', '对应活动')->display(function ($key_id){
            if($this->type == 1){
                return CompanyReadModel::where('id',$key_id)->value('title');
            }elseif($this->class_type == 2){
                return ActivityClassModel::where('id',$key_id)->value('class_name');
            }elseif ($this->class_type == 3){
                return PersonalRead::where('id',$key_id)->value('personal_title');
            }elseif ($this->class_type == 4){
                return PersonalRead::where('id',$key_id)->value('title');
            }
        });
        $grid->column('article_id', '对应文章')->display(function ($article_id){
            return ArticleModel::where('id',$article_id)->value('article_title');
        });

        //dd($this->study_json_content);
        //$grid->column('class_work_id', '任务ID');
        $grid->column('work_id', '对应作业')->display(function ($work_id){
            //dd($this->study_json_content);
            return WorkModel::where('id',$work_id)->value('title');
        });

     /*   $grid->column('column_not_in_table','提交作业')->display(function () {

            if($this->type == 1){
                $study_json_contents = $this->study_json_content;
                $html = '';
                foreach ($study_json_contents as $k=>$v){
                   //dd($k);die;
                    $html .= "<div class='panel panel-default' style=\"width: 50%;\">
                  <div class='panel-heading'>
                    <h3 class='panel-title''>".''."</h3>
                  </div>
                  <div class='panel-body'>"
                    .''.
                    "</div>
                </div>";
                }
                return $html;
            }elseif($this->type == 2){
                return $this->study_url;
            }elseif ($this->type == 3){
                return $this->study_content;
            }
        });*/

        $grid->column('type', '任务类型')->using([
            1=>'心得（多标题类型）',
            2=>'音频',
            3=>'心得(单标题类型)',
        ]);
        $grid->column('class_type', '类型')->using([
            1=>'企业读书',
            2=>'六项精进',
            3=>'塾生学习',
            4=>'音频学习',
        ]);

        $grid->column('is_opend','是否公开')->using([
            1=>'对小组公开',
            2=>'全部人公开',
            3=>'私有',
        ]);
        $grid->column('is_draft','草稿箱')->using([
            1=>'是',
            2=>'否',
        ]);
        $grid->column('created_at', '提交时间');

        $grid->paginate(8);
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(StudyModel::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('user_id', '用户');
        $show->field('key_id', '对应活动');
        $show->field('article_id', '对应文章');
        $show->field('class_work_id', '任务ID');
        $show->field('work_id', '对应作业');
        $show->field('type', '任务类型')->using([
            1=>'心得（多标题类型）',
            2=>'音频',
            3=>'心得(单标题类型)',
        ]);
        $show->field('class_type', '参加类型')->using([
            1=>'企业读书',
            2=>'六项精进',
            3=>'塾生学习',
            4=>'音频学习',
        ]);

/*        $show->field('study_json_content', '提交 文字/json 内容');
        $show->field('study_url','提交的录音路径');
        $show->field('study_content', '提交的富文本内容');*/
        $show->field('is_opend','提是否公开');
        $show->field('is_draft','是否是草稿箱');
        $show->field('created_at', '创建时间');
        $show->field('updated_at', '更新时间');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new StudyModel());

/*        $form->number('user_id', '用户');
        $form->number('key_id', '对应活动');
        $form->number('article_id', '对应文章');
        $form->number('class_work_id', '任务ID');
        $form->number('work_id', '对应作业');
        $form->decimal('type', '任务类型');
        $form->decimal('class_type', '参加类型');
        $form->textarea('study_json_content', '提交 文字/json 内容');
        $form->text('study_url','提交的录音路径');
        $form->textarea('study_content', '提交的富文本内容');*/
        $form->decimal('is_opend','提是否公开')->default(1);
        $form->decimal('is_draft','是否是草稿箱')->default(2);

        return $form;
    }
}
