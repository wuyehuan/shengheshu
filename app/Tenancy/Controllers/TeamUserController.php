<?php

namespace App\Admin\Controllers;

use App\Models\ActivityTeamModel;
use App\Models\ActivityTeamUser;
use App\Models\UserModel;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class TeamUserController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '小组成员\报名管理';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ActivityTeamUser());

        $grid->column('id', __('Id'));
        $grid->column('team_id','小组')->display(function($team_id) {
            return ActivityTeamModel::find($team_id)->team_name;
        });
        //$grid->column('key_id', __('Key id'));
        $grid->column('user_id','用户')->display(function($user_id) {
            return UserModel::find($user_id)->nickname;
        });
        $grid->column('team_user_stash', '审核状态')->using([
            0=>'未审核',
            1=>'通过',
            2=>'不通过',
        ]);
        $grid->column('team_type', '小组类型');
        $grid->column('name','姓名');
        $grid->column('phone', '手机号');
        $grid->column('company_name','公司名称');
        $grid->column('created_at', '创建时间');
        $grid->column('updated_at', '更新时间');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(ActivityTeamUser::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('team_id','小组');
       // $show->field('key_id', __('Key id'));
        $show->field('user_id','用户');
        $show->field('team_user_stash', '审核状态')->using([
            0=>'未审核',
            1=>'通过',
            2=>'不通过',
        ]);
        $show->field('team_type', '小组类型');
        $show->field('name','姓名');
        $show->field('phone', '手机号');
        $show->field('company_name','公司名称');
        $show->field('created_at', '创建时间');
        $show->field('updated_at', '更新时间');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new ActivityTeamUser());

        $form->number('team_id','小组');
        //$form->number('key_id', __('Key id'));
        $form->number('user_id','用户');
        $form->select('team_user_stash', '审核状态')->options([
            0=>'未审核',
            1=>'通过',
            2=>'不通过',
        ]);
        $form->select('team_type', '小组类型')
            ->options([
                1=>'企业读书',
                2=>'六项精进',
                3=>'塾生学习',
                4=>'音频学习',
            ])
            ->default(2);
        $form->text('name','姓名');
        $form->mobile('phone', '手机号');
        $form->text('company_name','公司名称');

        return $form;
    }
}
