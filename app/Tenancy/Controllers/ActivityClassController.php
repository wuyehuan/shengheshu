<?php

namespace App\Tenancy\Controllers;

use App\Models\ActivityClassModel;
use App\Models\BookModel;
use App\Models\BookPermission;
use App\Models\CompanyModel;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Models\ArticleModel;
use Illuminate\Support\Facades\Auth;

class ActivityClassController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '六项精进';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ActivityClassModel());

       // $admin = Auth::guard('admin')->user()->toArray();
        $company_id = Auth::guard('admin')->user()->company_id;
        if(!empty($company_id)){
            //是否有上传权限
            $upload_permission = CompanyModel::where('id',$company_id)->value('upload_permission');
            if($upload_permission == 2){
                $grid->disableCreateButton();
                $grid->disableActions();
            }
            //查出专属书本
            $book_ids = BookPermission::where('company_id',$company_id)->pluck('book_id')->toArray();
        }
        $grid->model()->where('company_id', '=',$company_id);

        $grid->column('id', __('Id'));
        //$grid->column('sort_id', __('Sort id'));
        $grid->column('class_name', '活动名称');
        $grid->column('author', '负责人');
        $grid->column('class_cove', '封面图')->image('',200,200);
        $grid->column('start_time', '开始时间');
        $grid->column('class_number', '参与人数');
        $grid->column('order', '排序');
        $grid->column('created_at', '创建时间');
        $grid->column('updated_at','更新时间');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(ActivityClassModel::findOrFail($id));

        $show->field('id', __('Id'));
        //$show->field('sort_id', __('Sort id'));
        $show->field('class_name', '活动名称');
        $show->field('author', '负责人');
        $show->class_cove('封面图')->image();
        $show->field('start_time', '开始时间');
        $show->field('class_number', '参与人数');
        $show->field('order', '排序');
        $show->field('created_at', '创建时间');
        $show->field('updated_at','更新时间');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new ActivityClassModel());

        $company_id = Auth::guard('admin')->user()->company_id;
        if(!empty($company_id)){
            //公开
            $book_ids_1 = BookModel::where('opend_type',1)->pluck('id')->toArray();
            //查出专属书本
            $book_ids_2 = BookPermission::where('company_id',$company_id)->pluck('book_id')->toArray();
            $book_ids = array_keys(array_flip($book_ids_1)+array_flip($book_ids_2));
        }

        $form->text('class_name', '活动名称');
        $form->text('author', '负责人');
    /*    $form->select('company_id', '公司')->options(
            CompanyModel::where('status',1)->value('company_name')
        );*/
        $form->image('class_cove', '封面图')
            ->move('/upload/images/'.date('Ymd'));
        $form->datetime('start_time', '开始时间')->default(date('Y-m-d H:i:s'));
        $form->number('class_number', '参与人数');
        $form->number('order', '排序');

        $form->hasMany('admin_class_article','对应文章(可读章节安排)',function (Form\NestedForm $form)use($book_ids) {

            $form->select('article_id','文章')->options(
                ArticleModel::whereIn('book_id',$book_ids)->pluck('article_title','id')
            );

            $form->datetime('finish_time','完成时间');

        });

        $form->hidden('company_id','公司');
        //保存前回调
        $form->saving(function (Form $form) {
            $admin = Auth::guard('admin')->user()->toArray();
            $form->company_id = $admin['company_id'];
            //dd($form->company_id);
        });

        $form->saved(function (Form $form) {

            $class_id = $form->model()->id;
            $admin_class_article = $form->admin_class_article;
            //dd($admin_class_article);die;
            if(!empty($admin_class_article)){
                $data=[];
                foreach ($admin_class_article as $k=>$v){
                    $data[$k] = $v;
                    $data[$k]['class_id'] = $class_id;
                }
                $form->admin_class_article = $data;
            }

        });

        return $form;
    }
}
