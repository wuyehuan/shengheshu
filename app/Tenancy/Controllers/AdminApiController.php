<?php

namespace App\Admin\Controllers;

use App\Models\ActivityClassArticle;
use App\Models\ArticleModel;
use App\Models\AudioStudyArticle;
use App\Models\CompanyReadModel;
use App\Models\WorkModel;
use Encore\Admin\Controllers\AdminController;
use Illuminate\Http\Request;
use App\Models\BookModel;
use App\Models\PersonalRead;

class AdminApiController extends AdminController
{
    public function getBook(Request $requests,BookModel $bookModel){
        $q = $requests->get('q');
        return $bookModel->where('book_title','like', "%$q%")->paginate(10, ['id', 'book_title as text']);
    }

    public function class_article(Request $requests,ArticleModel $articleModel,ActivityClassArticle $activityClassArticle){
        $q = $requests->get('q');
        $article_ids = $activityClassArticle->where('class_id',$q)->pluck('article_id');
        //->get(['id', DB::raw('name as text')])
        return $articleModel->whereIn('id',$article_ids)->get(['id','article_title as text']);
    }

    public function audio_article(Request $requests,ArticleModel $articleModel,AudioStudyArticle $studyArticle){
        $q = $requests->get('q');
        $article_ids = $studyArticle->where('audio_study_id',$q)->pluck('article_id');
        //->get(['id', DB::raw('name as text')])
        return $articleModel->whereIn('id',$article_ids)->get(['id','title as text']);
    }

    public function personal_article(Request $requests,ArticleModel $articleModel,PersonalRead $personalRead){
        $q = $requests->get('q');
        $article_ids = $personalRead->where('personal_read_id',$q)->pluck('article_id');
        //->get(['id', DB::raw('name as text')])
        return $articleModel->whereIn('id',$article_ids)->get(['id','title as text']);
    }

    public function book_article(Request $requests,ArticleModel $articleModel,ActivityClassArticle $activityClassArticle){
        $q = $requests->get('q');
        //$article_ids = $activityClassArticle->where('class_id',$q)->pluck('article_id');
        //->get(['id', DB::raw('name as text')])
        return $articleModel->where('book_id',$q)/*->whereIn('type',[1,3])*/->get(['id','article_title as text']);
    }

    public function company_article(Request $requests,ArticleModel $articleModel,ActivityClassArticle $activityClassArticle){
        $q = $requests->get('q');
        $article_ids = $activityClassArticle->where('class_id',$q)->pluck('article_id');
        //->get(['id', DB::raw('name as text')])
        return $articleModel->whereIn('id',$article_ids)->get(['id','article_title as text']);
    }

    //shs_personal_read
/*    public function personal_article(Request $requests,ArticleModel $articleModel,ActivityClassArticle $activityClassArticle){
        $q = $requests->get('q');
        $article_ids = $activityClassArticle->where('class_id',$q)->pluck('article_id');
        //->get(['id', DB::raw('name as text')])
        return $articleModel->whereIn('id',$article_ids)->get(['id','article_title as text']);
    }*/

    public function get_work(Request $request,WorkModel $workModel){
        $q = $request->input('q');
        return $workModel->where('type',$q)->get(['id','title as text']);
    }

    public function company_read(Request $request,CompanyReadModel $companyReadModel){

    }
}
