<?php

namespace App\Tenancy\Controllers;

use App\Models\ActivityClassModel;
use App\Models\ArticleModel;
use App\Models\ClassWorkModel;
use App\Models\CompanyModel;
use App\Models\WorkModel;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Models\CompanyReadModel;
use App\Models\AudioStudy;
use Illuminate\Support\Facades\Auth;

class CompanyReadWorkController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '布置任务(企业读书)';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ClassWorkModel());

        //$admin = Auth::guard('admin')->user()->toArray();
        //查询出ids
        // $company_read_ids = CompanyReadModel::where('company_id',$admin['company_id'])->pluck('id')->toArray();
        $admin = Auth::guard('admin')->user()->toArray();
        $grid->model()
            ->where('company_id', '=', $admin['company_id'])
            ->where('class_type', '=', 1);

        $admin = Auth::guard('admin')->user()->toArray();
        $grid->model()
            ->where('company_id',$admin['company_id'])
            ->where('class_type', '=', 1);

        $grid->column('id', __('Id'));
        $grid->column('article_id', '文章')->display(function ($article_id) {

            return ArticleModel::where('id',$article_id)->value('article_title');
        });

/*        $grid->column('company_id', '公司')->display(function ($company_id) {

            return CompanyModel::where('id',$company_id)->value('company_name');
        });*/

        $grid->column('work_id', '作业')->display(function ($work_id) {
            return WorkModel::where('id',$work_id)->value('title');
        });

        $grid->column('company_read_id', '活动')->display(function ($company_read_id) {
            return CompanyReadModel::where('id',$company_read_id)->value('title');
        });

        $grid->column('class_type','参加类型')->using([1=>'六项精进',2=>'企业读书',3=>'塾生学习']);
        $grid->column('type', '作业类型')->using([1=>'心得(模板格式,多个标题)',2=>'读原文\录音',3=>'心得(单个标题)']);

        $grid->column('created_at','创建时间');
        $grid->column('updated_at','更新时间');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(ClassWorkModel::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('article_id', '文章')->as(function ($article_id) {
            return ArticleModel::where('id',$article_id)->value('article_title');
        });
        // $show->field('key_id', __('Key id'));
        $show->field('work_id', '作业')->as(function ($work_id) {
            return WorkModel::where('id',$work_id)->value('title');
        });

        $show->field('company_read_id', '活动')->as(function ($company_read_id) {
            return CompanyReadModel::where('id',$company_read_id)->value('title');
        });

        $show->field('class_type','参加类型')->using([1=>'六项精进',2=>'企业读书',3=>'塾生学习']);
        $show->field('type', '作业类型')->using([1=>'心得(模板格式,多个标题)',2=>'读原文\录音',3=>'心得(单个标题)']);
        $show->field('created_at','创建时间');
        $show->field('updated_at','更新时间');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new ClassWorkModel());

/*        $form->select('company_id', '公司')->options(
            CompanyModel::where('status',3)->pluck('company_name','id')
        )->load('company_read_id','/admin/company_read_list');*/
        $admin = Auth::guard('admin')->user()->toArray();

        $form->hidden('company_id','公司')->value($admin['company_id']);

        $form->select('company_read_id', '活动')->options(
            CompanyReadModel::where('company_id',$admin['company_id'])->orderBy('created_at','desc')->pluck('title as text','id')
        )->load('article_id', '/admin/company_read_book_article');

        $form->select('article_id', '文章')
            ->options(
            // ArticleModel::/*whereIn('type',[1,3])->*/pluck('article_title as text','id')
            );

        $form->select('type', '作业类型')->options([
            1=>'心得(模板格式)',
            2=>'读原文\录音',
            3=>'心得（富文本)',
        ])->load('work_id', '/admin/get_work');

        $form->select('work_id', '作业')->options(
            WorkModel::pluck('title as text','id')
        );

        /*        $form->select('work_id', '作业')->options(
                    WorkModel::pluck('title as text','id')
                );*/

        $form->hidden('key_id');
        $form->hidden('class_type');

        //保存前回调
        $form->saving(function (Form $form) {
            //dd($form->article_id,$form->activity_class_id);
            $form->key_id = $form->company_read_id;
            $form->class_type = 1;
            if(!empty($form->class_type)){

            }
        });

        return $form;
    }
}
