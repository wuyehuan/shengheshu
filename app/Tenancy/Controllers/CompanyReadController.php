<?php

namespace App\Tenancy\Controllers;

use App\Models\BookModel;
use App\Models\BookPermission;
use App\Models\CompanyReadModel;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Models\CompanyModel;
use Illuminate\Support\Facades\Auth;
class CompanyReadController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '企业读书管理';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new CompanyReadModel());

        $admin = Auth::guard('admin')->user()->toArray();
        $grid->column('id', __('Id'));
        $grid->model()
            ->where('company_id', '=', $admin['company_id']);
        //$grid->column('personal_title', '企业读书活动标题');
/*        $grid->column('company_id', '公司')->display(function ($company_id) {

            return CompanyModel::where('id',$company_id)->value('company_name');
        });*/
        //$grid->column('company_id', '创建企业名称');
        $grid->column('book_id','书本名称')->display(function ($book_id){
            return BookModel::where('id',$book_id)->value('book_title');
        });
        $grid->column('created_at','创建时间');
        $grid->column('updated_at', '更新时间');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(CompanyReadModel::findOrFail($id));

        $show->field('id', __('Id'));
        //$show->field('personal_title', '企业读书活动标题');
        $show->field('company_id', '创建企业名称');
        $show->field('book_id','书本名称');
        $show->field('created_at','创建时间');
        $show->field('updated_at', '更新时间');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new CompanyReadModel());

        $admin = Auth::guard('admin')->user()->toArray();

        $form->hidden('company_id','公司')->value($admin['company_id']);

        $form->text('title', '企业读书名称');
        /*        $form->select('company_id', '创建企业名称')->options(function ($company_id){
                    return CompanyModel::pluck('company_name','id');
                });*/

        $form->select('book_id','书本')->options(function ($id){
            return BookModel::/*where('id',$id)->*/pluck('book_title','id');
        });

        //保存前回调
        /*        $form->saving(function (Form $form) {
                    $admin = Auth::guard('admin')->user()->toArray();
                    $form->company_id = $admin['company_id'];
                    //dd($form->company_id);
                });*/
        return $form;
    }
}
