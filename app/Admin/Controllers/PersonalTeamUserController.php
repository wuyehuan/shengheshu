<?php
namespace App\Admin\Controllers;

use App\Models\ActivityTeamModel;
use App\Models\ActivityTeamUser;
use App\Models\UserModel;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Admin\Extensions\Tools\ReleasePost;
use App\Admin\Extensions\Tools\UserGender;
use Illuminate\Http\Request;

class PersonalTeamUserController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '小组成员\报名管理(塾生学习)';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ActivityTeamUser());

        $grid->model()->where('team_type', '=', 3);

        $grid->disableCreateButton();

 /*       $grid->tools(function ($tools) {
            $tools->append(new UserGender());
        });*/

        $grid->filter(function ($filter){
            // 去掉默认的id过滤器
            $filter->disableIdFilter();
            // 关联查询
            $filter->where(function ($query){
                $query -> whereHas('user',function ($query){
                    $query -> where('nickname','like',"%".$this -> input."%");
                });
            },'用户名');
            $filter->where(function ($query){
                $query -> whereHas('team',function ($query){
                    $query -> where('team_name','like',"%".$this -> input."%");
                });
            },'小组名');

            $filter->equal('team_user_stash','身份审核')->select([
                0=>'未审核',
                1=>'通过',
                2=>'未通过',
            ]);

        });

        $grid->tools(function ($tools) {
            $tools->batch(function ($batch) {
                $batch->add('一鍵審核', new ReleasePost(1));
            });
        });

        $grid->column('id', __('Id'))->style('display:table-cell;vertical-align:middle;text-align:center;');
        $grid->column('team_id','小组')->display(function($team_id) {
            return ActivityTeamModel::where('id',$team_id)->value('team_name');
        })->style('display:table-cell;vertical-align:middle;text-align:center;');
        //$grid->column('key_id', __('Key id'));
        $grid->column('user_id','用户')->display(function($user_id) {
            //dd($user_id);
           return UserModel::where('id',$user_id)->value('nickname');
        })->style('display:table-cell;vertical-align:middle;text-align:center;');
        $grid->column('team_user_stash', '审核状态')->using([
            0=>'未审核',
            1=>'通过',
            2=>'不通过',
        ])->style('display:table-cell;vertical-align:middle;text-align:center;');
        //is_Invisible
        $grid->column('is_Invisible', '是否是打卡小组')->using([
            1=>'是',
            2=>'否',
        ])->style('display:table-cell;vertical-align:middle;text-align:center;');
        $grid->column('team_type', '小组类型')->style('display:table-cell;vertical-align:middle;text-align:center;');
        $grid->column('name','姓名')->style('display:table-cell;vertical-align:middle;text-align:center;');
       // $grid->column('phone', '手机号')->style('display:table-cell;vertical-align:middle;text-align:center;');
        $grid->column('company_name','公司名称')->style('display:table-cell;vertical-align:middle;text-align:center;');
        $grid->column('created_at', '创建时间')->style('display:table-cell;vertical-align:middle;text-align:center;');
        $grid->column('updated_at', '更新时间')->style('display:table-cell;vertical-align:middle;text-align:center;');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(ActivityTeamUser::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('team_id','小组');
       // $show->field('key_id', __('Key id'));
        $show->field('user_id','用户');
        $show->field('team_user_stash', '审核状态')->using([
            0=>'未审核',
            1=>'通过',
            2=>'不通过',
        ]);
        $show->field('is_admin', '是否是管理员')->using([
            1=>'是',
            2=>'否',
        ]);
        $show->field('team_type', '小组类型');
        $show->field('name','姓名');
        $show->field('phone', '手机号');
        $show->field('company_name','公司名称');
        $show->field('created_at', '创建时间');
        $show->field('updated_at', '更新时间');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new ActivityTeamUser());

       // $form->number('team_id','小组');
        //$form->number('key_id', __('Key id'));
        //$form->number('user_id','用户');
        $form->select('team_user_stash', '审核状态')->options([
            0=>'未审核',
            1=>'通过',
            2=>'不通过',
        ]);

        $form->select('is_Invisible','是否是打卡小组')->options([
            1=>'是',
            2=>'否',
        ]);
/*        $form->select('team_type', '小组类型')
            ->options([
                1=>'企业读书',
                2=>'六项精进',
                3=>'塾生学习',
                4=>'音频学习',
            ])
            ->default(2);*/
        $form->select('is_supervisor','是否是监管者')->options([
            1=>'是',
            2=>'否',
        ]);
        $form->text('name','姓名');
        //$form->mobile('phone', '手机号');
        $form->text('company_name','公司名称');



        //保存后回调
        $form->saved(function (Form $form) {
            $team_user_stash = $form->model()->team_user_stash;
            if($team_user_stash == 1){

            }
        });

        return $form;
    }

}
