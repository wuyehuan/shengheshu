<?php

namespace App\Admin\Controllers;

use App\Models\Holidays;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class HolidaysController extends AdminController
{
    //废弃
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '休息日';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Holidays());

        $grid->column('id', __('Id'));
        $grid->column('type','类型')->using([
            1=>'企业读书',
            2=>'六项精进',
            3=>'塾生学习',
        ]);
        $grid->column('wee_holiday','每周休息日')->display(function ($wee_holiday){
           return $this->getOriginal('wee_holiday'); // 原始值
        });
        $grid->column('day_holiday_start','休息日開始')->date('Y-m-d H:i:s');
        $grid->column('day_holiday_end','休息日結束')->date('Y-m-d H:i:s');
        $grid->column('created_at','创建时间');
        $grid->column('updated_at','更新时间');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Holidays::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('type','类型')->using([
            1=>'企业读书',
            2=>'六项精进',
            3=>'塾生学习',
        ]);
        $show->field('wee_holiday','每周休息日');
        $show->field('day_holiday','设置休息日');
        $show->field('created_at','创建时间');
        $show->field('updated_at','更新时间');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Holidays());

        $form->select('type','类型')->options([
            1=>'企业读书',
            2=>'六项精进',
            3=>'塾生学习',
        ]);
        $form->checkbox('wee_holiday','每周休息日')->options([
            1=>'周一',
            2=>'周二',
            3=>'周三',
            4=>'周四',
            5=>'周五',
            6=>'周六',
            7=>'周日',
        ]);
      //  $form->text('day_holiday','设置休息日');
        $form->datetimeRange('day_holiday_start', 'day_holiday_end', '具体休息时间');

        $form->saving(function (Form $form) {

          // dd($form);
        });
        return $form;
    }
}
