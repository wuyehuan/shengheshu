<?php

namespace App\Admin\Controllers;

use App\Models\SystemImages;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class SystemImagesController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '上传小程序码';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new SystemImages());
        $grid->model()->where('type',1);
        //$grid->column('id', __('Id'));
        $grid->column('img_url', '路徑')->image();
        //$grid->column('type', '类型');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(SystemImages::findOrFail($id));

       /* $show->field('id', __('Id'));
        $show->field('img_url', '路徑');
        $show->field('type', '类型');*/

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new SystemImages());

        $form->image('img_url', '路徑')
            ->move('/upload/images/'.date('Ymd'))
            ->uniqueName();

        $form->hidden('type',1)->value(1);
        //$form->number('type', '类型');

        return $form;
    }
}
