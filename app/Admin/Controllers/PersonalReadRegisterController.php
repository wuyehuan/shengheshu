<?php

namespace App\Admin\Controllers;

use App\Models\PersonalReadRegister;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Models\PersonalRead;
use App\Models\UserModel;

class PersonalReadRegisterController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '塾生学习审核';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new PersonalReadRegister());

        $grid->column('id', __('Id'));
        $grid->column('personal_read_id', '塾生学习')->display(function ($personal_read_id){
            $per = new PersonalRead();
            return $per->where('id',$personal_read_id)->value('personal_title');
        });
        $grid->column('user_id', '用户')->display(function ($user_id){
            $user = new UserModel();
            return $user->where('id',$user_id)->value('nickname');
        });
        $grid->column('company_name', '填写的公司信息');
        $grid->column('phone', '填写的手机号');
        $grid->column('stash','审核状态')->using([
            1=>'未审核',
            2=>'通过',
            3=>'拒绝',
        ]);
        $grid->column('created_at','创建时间');
        $grid->column('updated_at','更新时间');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(PersonalReadRegister::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('personal_read_id', '塾生学习');
        $show->field('user_id', '用户');
        $show->field('company_name', '填写的公司信息');
        $show->field('stash','审核状态')->using([
            1=>'未审核',
            2=>'通过',
            3=>'拒绝',
        ]);
        $show->field('phone', '填写的手机号');
        $show->field('created_at','创建时间');
        $show->field('updated_at','更新时间');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new PersonalReadRegister());

        $form->number('personal_read_id', '塾生学习');
        $form->number('user_id', '用户');
        $form->text('company_name', '填写的公司信息');
        $form->mobile('phone', '填写的手机号');
        $form->select('stash','审核状态')->options([
            1=>'未审核',
            2=>'通过',
            3=>'拒绝',
        ]);

        return $form;
    }
}
