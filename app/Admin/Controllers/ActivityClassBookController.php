<?php

namespace App\Admin\Controllers;

use App\Models\ActivityClassBook;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class ActivityClassBookController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '六項精進綁定小組';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ActivityClassBook());

        $grid->column('id', __('Id'));
        $grid->column('book_id', __('Book id'));
        $grid->column('activity_class_id', __('Activity class id'));
        $grid->column('start_time', __('Start time'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(ActivityClassBook::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('book_id', __('Book id'));
        $show->field('activity_class_id', __('Activity class id'));
        $show->field('start_time', __('Start time'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new ActivityClassBook());

        $form->number('book_id', __('Book id'));
        $form->number('activity_class_id', __('Activity class id'));
        $form->datetime('start_time', __('Start time'))->default(date('Y-m-d H:i:s'));

        return $form;
    }
}
