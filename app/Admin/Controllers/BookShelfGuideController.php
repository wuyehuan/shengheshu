<?php

namespace App\Admin\Controllers;

use App\Models\BookshelfGuide;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class BookShelfGuideController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '书架指南';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new BookshelfGuide());
        $grid->disableCreateButton();
        $grid->disableFilter();

        $grid->disableExport();

        $grid->disableRowSelector();

        $grid->column('id', __('Id'));
        $grid->column('title', '标题');
        $grid->column('content', '内容')->limit(30);
        $grid->column('created_at', '创建');
        $grid->column('updated_at', '更新时间');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(BookshelfGuide::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('title', '标题');
        $show->field('content', '内容');
        $show->field('created_at', '创建');
        $show->field('updated_at', '更新时间');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new BookshelfGuide());

        $form->text('title', '标题');
        $form->textarea('content', '内容');

        return $form;
    }
}
