<?php

namespace App\Admin\Controllers;

use App\Models\ActivityCove;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Grid\Displayers\Actions;
use Encore\Admin\Show;

class ActivityCoveController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '活动封面';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ActivityCove());

        // 最原始的`按钮图标`形式
        $grid->setActionClass(Actions::class);

        $grid->column('id', __('Id'));
        $grid->column('cove_img','封面图')->image('',80,80);
        $grid->column('class_type', '封面图类型')->using(
            [
                1=>'企业读书',
                2=>'六项精进',
                3=>'塾生学习',
                4=>'音频学习',
            ]
        );
        $grid->column('created_at','创建时间');
        $grid->column('updated_at','更新时间');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(ActivityCove::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('cove_img','封面图')->image('',200,200);
        $show->field('class_type', '封面图类型');
        $show->field('created_at','创建时间');
        $show->field('updated_at','更新时间');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new ActivityCove());

        $form->image('cove_img','封面图')
            ->move('/upload/images/'.date('Ymd'))
            ->uniqueName();

        $form->select('class_type', '封面图类型')->options([
            1=>'企业读书',
            2=>'六项精进',
            3=>'塾生学习',
            4=>'音频学习',
        ]);

        return $form;
    }
}
