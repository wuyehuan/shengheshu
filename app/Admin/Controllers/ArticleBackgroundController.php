<?php

namespace App\Admin\Controllers;

use App\Models\ArticleBackground;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class ArticleBackgroundController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '文章背景圖';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ArticleBackground());

        $grid->actions(function (Grid\Displayers\Actions $actions) {
           // if ($actions->getKey() == 1) {
                $actions->disableDelete();
            //}
        });

        $grid->disableCreateButton();
        $grid->disablePagination();
        $grid->disableFilter();
        $grid->disableExport();
        $grid->disableRowSelector();
        $grid->disableColumnSelector();

        //$grid->column('id', __('Id'));
        $grid->column('url','图片')->image('',100,100)->style('display:table-cell;vertical-align:middle;text-align:center;');
        $grid->column('created_at','创建时间')->style('display:table-cell;vertical-align:middle;text-align:center;');
        $grid->column('updated_at','更新时间')->style('display:table-cell;vertical-align:middle;text-align:center;');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(ArticleBackground::findOrFail($id));

       // $show->field('id', __('Id'));
        $show->field('url','图片')->image('',120,120);
        $show->field('created_at','创建时间');
        $show->field('updated_at','更新时间');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new ArticleBackground());

        $form->image('url','图片')
            ->move('/upload/articlecove/'.date('Y-m-d'))
            ->uniqueName();

        return $form;
    }
}
