<?php

namespace App\Admin\Controllers;

use App\Models\ActivityClassModel;
use App\Models\ArticleModel;
use App\Models\AudioStudy;
use App\Models\BookModel;
use App\Models\CompanyModel;
use App\Models\CompanyReadModel;
use App\Models\PersonalRead;
use App\Models\WorkModel;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Grid\Displayers\Actions;
use Encore\Admin\Show;


class AudioStudyController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '音频学习';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new AudioStudy());

        // 最原始的`按钮图标`形式
        $grid->setActionClass(Actions::class);


        $grid->column('id', __('Id'));
        $grid->column('title', '标题');
        $grid->column('synopsis', '簡介');
/*        $grid->column('book_id', '书本')->display(function($book_id) {
            return BookModel::where('id',$book_id)->value('book_title');
        });*/
  /*      $grid->column('company_id', '公司')->display(function($company_id) {
            return CompanyModel::where('id',$company_id)->value('company_name');
        });*/
        $grid->column('cove','封面')->image('',150,150);
        $grid->column('created_at', '创建时间');
        $grid->column('updated_at','更新时间');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(AudioStudy::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('title', '标题');
        $show->field('book_id', '书本')->as(function ($book_id) {

            return BookModel::where('id',$book_id)->value('book_title');

        });
        $show->field('created_at', '创建时间');
        $show->field('updated_at','更新时间');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new AudioStudy());

        $form->hidden('title', '标题');

        $form->textarea('synopsis', '簡介');
      /*  $form->select('company_id','公司')
            ->options(
                '/admin/company_list'
            );*/

/*        $form->select('book_id', '书本')->options(
            BookModel::pluck('book_title','id')
        )->load('article_id','/admin/book_article');*/

        $form->image('cove', '封面')
            ->move('/upload/images/'.date('Y-m-d'))
            ->uniqueName();

        $form->select('book_id','书本')->options(function ($id){
            return BookModel::/*where('id',$id)->*/pluck('book_title','id');
        })->default(0);

        $form->saving(function (Form $form) {

        //    dump($form->username);
            $book_id = $form->book_id;
            $book_title = BookModel::where('id',$book_id)->value('book_title');
            $form->title = $book_title;

        });

/*        $form->select('class_type', '活动类型')
            ->options([
                1 => '企业读书',
                2 => '六项精进',
                3 => '塾生学习',
            ])->when(1, function (Form $form) {
                $form->select('key_id','企业活动')->options(
                    CompanyReadModel::pluck('title as text','id')
                );
            })->when(2, function (Form $form) {
                $form->select('key_id','六项精进活动')->options(
                    //CompanyReadModel::pluck('id','title')
                    ActivityClassModel::pluck('class_name as text','id')
                );
            })->when(3, function (Form $form) {
                $form->select('key_id','六项精进活动')->options(
                //CompanyReadModel::pluck('id','title')
                    PersonalRead::pluck('personal_title as text','id')
                );
            });*/

/*        $form->hasMany('many_audio_article','選擇录音',function (Form\NestedForm $form) {

            $form->select('article_id','录音')->options(
                ArticleModel::pluck('article_title','id')
            );

        });*/
     /*   $form->saved(function (Form $form) {

            $id = $form->model()->id;
            if(!empty($form->model()->key_id)){
                $article = [];
                \DB::table('audio_study_article')->where('audio_study_id',$id)->delete();
                if($form->model()->class_type ==1){
                    $ids = ArticleModel::where('book_id',$form->model()->book_id)->pluck('id')->toArray();
                    foreach ($ids as $k=>$v){
                        $article[$k]['article_id'] = $v;
                        $article[$k]['audio_study_id'] = $id;
                        // $article[$k]['book_id'] = $form->model()->book_id;
                    }

                }elseif($form->model()->class_type ==2){
                    $book_id = ActivityClassModel::where('id',$form->model()->key_id)->value('book_id');
                    $ids = ArticleModel::where('book_id',$book_id)->pluck('id')->toArray();
                    foreach ($ids as $k=>$v){
                        $article[$k]['article_id'] = $v;
                        $article[$k]['audio_study_id'] = $id;
                        //$article[$k]['book_id'] = $form->model()->book_id;
                    }

                }elseif($form->model()->class_type ==3){

                    $ids = ArticleModel::where('book_id',$form->model()->book_id)->pluck('id')->toArray();
                    foreach ($ids as $k=>$v){
                        $article[$k]['article_id'] = $v;
                        $article[$k]['audio_study_id'] = $id;
                       // $article[$k]['book_id'] = $form->model()->book_id;
                    }

                }

                //dd($article);
                \DB::table('audio_study_article')->insert($article);
            }
        });*/

        return $form;
    }
}
