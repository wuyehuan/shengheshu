<?php

namespace App\Admin\Controllers;

use App\Models\CompanyModel;
use App\Models\SetSignModel;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class SetSignController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '设置学分';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new SetSignModel());

        $grid->column('id', __('Id'));
        $grid->column('company_id','企业')->display(function ($company_id){
            $company_name = CompanyModel::where('id',$company_id)->value('company_name');
            $company = empty($company_id)?'总平台':$company_name;
            return $company;
        });
        $grid->column('integral_day','塾生学习获得学分');
        $grid->column('team_integral_day', '六项精进小组获得学分');
        $grid->column('company_integral_day', '企业读书小组获得学分');
        $grid->column('audio_integral_day', '音频学习小组获得学分');
        $grid->column('created_at', '创建时间');
        $grid->column('updated_at','更新时间');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(SetSignModel::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('company_id','企业ID');
        $show->field('integral_day','塾生学习获得学分');
        $show->field('team_integral_day', '六项精进小组获得学分');
        $show->field('company_integral_day', '企业读书小组获得学分');
        $show->field('audio_integral_day', '音频学习小组获得学分');
        $show->field('created_at', '创建时间');
        $show->field('updated_at','更新时间');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new SetSignModel());

        $form->select('company_id','企业')->options(
            CompanyModel::where('status',3)->pluck('company_name','id')
        );
        $form->number('integral_day','塾生学习获得学分')->default(1);
        $form->number('team_integral_day', '六项精进小组获得学分')->default(1);
        $form->number('company_integral_day', '企业读书小组获得学分');
        $form->number('audio_integral_day', '音频学习小组获得学分');

        return $form;
    }
}
