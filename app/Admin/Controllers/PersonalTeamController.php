<?php

namespace App\Admin\Controllers;

use App\Admin\Extensions\Tools\ReleasePost;
use App\Models\ActivityClassModel;
use App\Models\ActivityTeamModel;
use App\Models\BookModel;
use App\Models\CompanyReadModel;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Models\PersonalRead;
use Illuminate\Support\MessageBag;

class PersonalTeamController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '小组管理(塾生学习)';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ActivityTeamModel());

        $grid->model()->where('team_type', '=', 3);

        // 默认为每页20条
        $grid->paginate(8);

        $grid->tools(function ($tools) {
            $tools->batch(function ($batch) {
                $batch->add('一鍵審核', new ReleasePost(2));
            });
        });

        $grid->filter(function ($filter){
            // 去掉默认的id过滤器
            $filter->disableIdFilter();
            // 关联查询
            $filter->where(function ($query){
                $query -> whereHas('user',function ($query){
                    $query -> where('nickname','like',"%".$this -> input."%");
                });
            },'创建用户');

            $filter->contains('team_name','小组名称');

        });

        $grid->column('id', __('Id'));

        //$grid->team_logo( '小组头像')->lightbox(['width' => 80, 'height' => 80])->style('display:table-cell;vertical-align:middle;text-align:center;');

        $grid->column('team_name', '小组名称')->style('display:table-cell;vertical-align:middle;text-align:center;');

        $grid->column('key_id','对应活动')->display(function ($key_id){
            return PersonalRead::where('id',$key_id)->value('personal_title');
        })
            ->style('display:table-cell;vertical-align:middle;text-align:center;');

        $grid->column('team_stash', '状态')->using([
            1=>'正常',
            2=>'停用',
        ])->label([
            1 => 'success',
            2 => 'warning',
        ])->style('display:table-cell;vertical-align:middle;text-align:center;');

        $grid->column('wee_holiday','每周休息日')->display(function ($wee_holiday){
            return $this->getOriginal('wee_holiday'); // 原始值
        })->style('display:table-cell;vertical-align:middle;text-align:center;');
        $grid->column('day_holiday_start','休息日開始')->date('Y-m-d H:i:s')->style('display:table-cell;vertical-align:middle;text-align:center;');
        $grid->column('day_holiday_end','休息日結束')->date('Y-m-d H:i:s')->style('display:table-cell;vertical-align:middle;text-align:center;');

        $grid->column('created_at','创建时间')->style('display:table-cell;vertical-align:middle;text-align:center;');
        //$grid->column('updated_at','更新时间');
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(ActivityTeamModel::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('team_name', '小组名称');

        $show->field('key_id','对应活动')->as(function ($key_id) {
            return PersonalRead::where('id',$key_id)->value('personal_title');
        });

        $show->field('team_stash', '队伍状态')->using([
            1=>'正常',
            2=>'停用',
        ]);


       // $show->field('team_logo', '小组头像')->image('',500,500);

        $show->team_user('小组成员', function ($comments) {

            $comments->resource('/admin/activity-team-users');
            $comments->disableCreateButton();

            $comments->tools(function ($tools) {
                $tools->batch(function ($batch) {
                    $batch->add('一鍵審核', new ReleasePost(1));
                });
            });

            $comments->name('姓名');

            $comments->company_name('公司名称');

            $comments->is_Invisible('是否是打卡小组')->using([
                1=>'是',
                2=>'否',
            ]);

            $comments->team_user_stash('审核状态')->using([
                0=>'未审核',
                1=>'通过',
                2=>'不通过',
            ])->label([
                0=>'default',
                1 => 'success',
                2 => 'warning',
            ]);

        });
        $show->field('created_at','创建时间');
        $show->field('updated_at','更新时间');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new ActivityTeamModel());

        $form->hidden('id','id');
        $form->text('team_name', '小组名称');
        //$form->number('max_number','允许进组最大人数 ')->default(1);
        $form->hidden('team_type',3)->value(3);

/*        $form->image('team_logo','头像')
            ->move('/upload/images/'.date('Y-m-d'))
            ->uniqueName();*/

        $form->select('key_id','对应活动')->options(
            PersonalRead::pluck('personal_title as text','id')
        )->load('book_id','/admin/personal_book')->required();

        $form->select('team_stash', '队伍状态')
            ->options([
                1=>'正常',
                2=>'停用'
            ])
            ->default(1);

        $form->checkbox('wee_holiday','每周休息日')->options([
            1=>'周一',
            2=>'周二',
            3=>'周三',
            4=>'周四',
            5=>'周五',
            6=>'周六',
            7=>'周日',
        ]);
        //  $form->text('day_holiday','设置休息日');
        $form->text('Invitation_code','验证码')->required();
        $form->datetimeRange('day_holiday_start', 'day_holiday_end', '具体休息时间');

        $form->datetime('start_time', '开始时间')->default(date('Y-m-d H:i:s'))->required();

        //保存后回调
        $form->saved(function (Form $form){
            $api = new AdminApiController();
            //dd($form->id);die;
            $res = $api->redis_team_book_article($form->model()->key_id,3,$form->model()->id);
            //dd($data);
            if($res){
                $title = '成功';
                $message = '成功';
                $success = new MessageBag([
                    'title'   => $title,
                    'message' => $message,
                ]);
                return back()->with(compact('success'));
            }else{
                $title = '失敗';
                $message = '失敗';
                $error = new MessageBag([
                    'title'   => $title,
                    'message' => $message,
                ]);
                return back()->with(compact('error'));
            }
        });

        return $form;
    }
}
