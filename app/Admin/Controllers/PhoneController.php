<?php

namespace App\Admin\Controllers;

use App\Models\Phone;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class PhoneController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '绑定手机号码';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Phone());

        $grid->column('id', __('Id'));
        $grid->column('phone','手机号码');
        $grid->column('created_at', '创建时间');
        $grid->column('updated_at','更新时间');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Phone::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('phone','手机号码');
        $show->field('created_at', '创建时间');
        $show->field('updated_at','更新时间');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Phone());

        $form->mobile('phone','手机号码');

        return $form;
    }
}
