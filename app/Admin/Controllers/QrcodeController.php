<?php

namespace App\Admin\Controllers;

use App\Models\Qrcode;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class  QrcodeController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '打卡海報二维码';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Qrcode());

        $grid->actions(function (Grid\Displayers\Actions $actions) {
            if ($actions->getKey() == 1) {
                $actions->disableDelete();
            }
        });
        $grid->disableExport();
        $grid->disableRowSelector();
        //$grid->column('id', __('Id'));
        $grid->column('url','图片')->image('',80,80);

        $grid->column('created_at','创建时间');
        $grid->column('updated_at','更新时间');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Qrcode::findOrFail($id));

        //$show->field('id', __('Id'));
        $show->field('url','图片')->image('',80,80);

        $show->field('created_at','创建时间');
        $show->field('updated_at','更新时间');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Qrcode());

        $form->image('url','图片')
            ->move('/upload/qrcode/'.date('Y-m-d'))
            ->uniqueName();

        return $form;
    }
}
