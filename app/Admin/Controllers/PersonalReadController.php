<?php

namespace App\Admin\Controllers;

use App\Models\BookModel;
use App\Models\PersonalRead;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Models\PersonalReadBook;

class PersonalReadController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '活动列表';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new PersonalRead());

        $grid->column('id', __('Id'))->style('display:table-cell;vertical-align:middle;text-align:center;');
        $grid->column('personal_title','活动名称')->style('display:table-cell;vertical-align:middle;text-align:center;');
        $grid->column('cove_url','封面')->image('',120,120)->style('display:table-cell;vertical-align:middle;text-align:center;');

        $grid->column('order', '排序')->style('display:table-cell;vertical-align:middle;text-align:center;');
       // $grid->column('start_time', '开始时间')->style('display:table-cell;vertical-align:middle;text-align:center;');
        $grid->column('created_at','创建时间')->style('display:table-cell;vertical-align:middle;text-align:center;');
        $grid->column('updated_at', '更新时间')->style('display:table-cell;vertical-align:middle;text-align:center;');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(PersonalRead::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('personal_title','活动名称');

        $show->field('order', '排序');
        $show->field('created_at','创建时间');
        $show->field('updated_at', '更新时间');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new PersonalRead());

        $form->text('personal_title','活动名称');

        $form->image('cove_url', '封面图')
            ->move('/upload/images/'.date('Ymd'));

        $form->number('order', '排序');


       // $form->datetime('start_time', '开始时间')->default(date('Y-m-d H:i:s'))->required();

        $form->listboxsortable('book','綁定书本')->options(BookModel::pluck('book_title', 'id'));

        //保存后回调
        $form->saved(function (Form $form) {
            $book = $form->book;

            foreach ($book as $k=>$v){
                PersonalReadBook::where('personal_read_id',$form->model()->id)->where('book_id',$v)->update([
                    'sort'=>$k+1
                ]);
            }

        });
        return $form;
    }
}
