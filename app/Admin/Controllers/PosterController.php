<?php

namespace App\Admin\Controllers;

use App\Models\Poster;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class PosterController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '海报';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Poster());

        $grid->column('id', __('Id'));
        $grid->column('cove_url','海报图')->image('',80,80);
        $grid->column('created_at','创建时间');
        $grid->column('updated_at', '更新时间');
        $grid->disableExport();
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Poster::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('cove_url','海报图')->image('',200,200);
        $show->field('start_time','開始時間');
        $show->field('end_time','結束時間');
        $show->field('created_at','创建时间');
        $show->field('updated_at', '更新时间');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Poster());

        $form->image('cove_url','海报图')
            ->move('/upload/images/'.date('Ymd'))
            ->uniqueName();
        $form->datetime('start_time','开始时间')->format('YYYY-MM-DD HH:mm:ss');
        $form->datetime('end_time','结束时间')->format('YYYY-MM-DD HH:mm:ss');

        return $form;
    }
}
