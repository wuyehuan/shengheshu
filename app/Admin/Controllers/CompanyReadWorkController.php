<?php

namespace App\Admin\Controllers;

use App\Models\BookModel;
use App\Models\CompanyReadBook;
use App\Models\CompanyReadModel;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Models\CompanyModel;

class CompanyReadWorkController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '作业安排';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new CompanyReadModel());

        $grid->column('id', __('Id'));
        $grid->column('title', '标题');
        $grid->column('company_id','公司')->display(function ($company_id){
            return CompanyModel::where('id',$company_id)->value('company_name');
        });
        //$grid->column('book_id', __('Book id'));
        //$grid->column('synopsis', __('Synopsis'));
        $grid->column('cove_url', '封面')->image('',120,120);
        $grid->column('created_at', '创建时间');
        //$grid->column('updated_at', __('Updated at'));
        //$grid->column('start_time', __('Start time'));
        $grid->disableCreateButton();

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(CompanyReadModel::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('title', '标题');
        $show->field('company_id', __('Company id'));
        //$show->field('book_id', __('Book id'));
        //$show->field('synopsis', __('Synopsis'));
        $show->field('cove_url', '封面');
        $show->field('created_at', '创建时间');
        //$show->field('updated_at', __('Updated at'));
        //$show->field('start_time', __('Start time'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new CompanyReadModel());

        if ($form->isEditing()){
            $id=request()->route()->parameters()['company_read_work'];
        }

        $form->hasMany('company_read_work', '安排作业', function (Form\NestedForm $form)use($id) {
            $book_ids = CompanyReadBook::where('comapny_read_id',$id)->pluck('book_id');
            $form->select('book_id','书本')->options(
                BookModel::whereIn('id',$book_ids)->pluck('book_title', 'id')
            );

            $form->select('type','作业类型')->options(
            //1心得(模板格式) 2读原文\\录音 3心得（富文本）
                [
                    1=>'多标题作业',
                    2=>'录音',
                    3=>'单节课标题',                ]
            )->load('work_id','/admin/get_work');

            $form->select('work_id','作业')->options(
            );

            $states = [
                'on'  => ['value' => 1, 'text' => '是', 'color' => 'success'],
                'off' => ['value' => 2, 'text' => '否', 'color' => 'danger'],
            ];

            $form->switch('is_points','是否计算积分')->states($states);
            $form->text('points_value','积分值');

            $form->hidden('key_id','key_id')->value($id);
            $form->hidden('class_type','class_type')->value(1);
            $form->hidden('company_read_id','company_read_id')->value($id);

        });

        return $form;
    }
}
