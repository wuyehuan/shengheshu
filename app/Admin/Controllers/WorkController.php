<?php

namespace App\Admin\Controllers;

use App\Models\WorkModel;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class WorkController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '作业管理';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new WorkModel());

        $grid->column('id', __('Id'));
        $grid->column('title', '标题');
        $grid->column('describe', '任务描述');
        //$grid->column('content_json','创建模板');
        $grid->column('type', '作业类型')->using([1=>'六项精进模板(多标题多输入框)',2=>'读原文\录音',3=>'心得（单输入框)']);
        $grid->column('created_at', '创建时间');
        $grid->column('updated_at','更新时间');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(WorkModel::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('title', '标题');
        $show->field('describe', '任务描述');
        //$show->field('content_json','创建模板');
        $show->field('type', '作业类型')->using([1=>'六项精进模板(多标题多输入框)',2=>'读原文\录音',3=>'心得（单输入框)']);
        $show->field('created_at', '创建时间');
        $show->field('updated_at','更新时间');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new WorkModel());

        $form->text('title', '标题');
        $form->textarea('describe', '任务描述');

        $form->select('type', '作业类型')
            ->options([1=>'六项精进模板(多标题,多输入框)',2=>'读原文\录音',3=>'心得（单输入框)'])
            ->when(1, function (Form $form) {
                $form->list('content_json','创建(标题)');
            });

        return $form;
    }
}
