<?php

namespace App\Admin\Controllers;

use App\Models\PersonalRead;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Models\PersonalReadBook;
use App\Models\BookModel;

class PersonalReadWorkController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '作业安排';

    /**
     * Make a grid builder.     *     * @return Grid     */
    protected function grid()
    {
        $grid = new Grid(new PersonalRead());

        $grid->column('id', __('Id'));
        $grid->column('personal_title', '活动名称');
        $grid->column('cove_url', '封面')->image('',120,120);
        //$grid->column('order', __('Order'));
        $grid->column('created_at', '创建时间');
        $grid->disableCreateButton();

        //$grid->column('updated_at', __('Updated at'));
        //$grid->column('start_time', __('Start time'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(PersonalRead::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('personal_title', '活动名称');
        //$show->field('book_id', '书本');
        //$show->field('order', __('Order'));
        $show->field('created_at', '创建时间');
        //$show->field('updated_at', __('Updated at'));
        //$show->field('start_time', __('Start time'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new PersonalRead());
        if ($form->isEditing()){
            //dd(request()->route()->parameters());
            $id=request()->route()->parameters()['personal_reads_work'];
        }

        $form->hasMany('personal_class_work','安排作业', function (Form\NestedForm $form)use($id) {
            $book_ids = PersonalReadBook::where('personal_read_id',$id)->pluck('book_id');
            $form->select('book_id','书本')->options(
                BookModel::whereIn('id',$book_ids)->pluck('book_title', 'id')
            );

            $form->select('type','作业类型')->options(
            //1心得(模板格式) 2读原文\\录音 3心得（富文本）
                [
                    1=>'多活动名称作业',
                    2=>'录音',
                    3=>'单节课活动名称',                ]
            )->load('work_id','/admin/get_work');

            $form->select('work_id','作业')->options(
            );

            $states = [
                'on'  => ['value' => 1, 'text' => '是', 'color' => 'success'],
                'off' => ['value' => 2, 'text' => '否', 'color' => 'danger'],
            ];

            $form->switch('is_points','是否计算积分')->states($states);
            $form->text('points_value','积分值');

            $form->hidden('key_id','key_id')->value($id);
            $form->hidden('class_type','class_type')->value(3);
            $form->hidden('personal_read_id','personal_read_id')->value($id);

        });

        return $form;
    }
}
