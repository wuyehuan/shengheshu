<?php

namespace App\Admin\Controllers;

use App\Models\ActivityTeamModel;
use App\Models\ActivityTeamUser;
use App\Models\Statistics;
use App\Models\StudyModel;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
//use Encore\Admin\Grid;
use Lauwen\Grid\Grid;
use Encore\Admin\Show;
use Illuminate\Support\Facades\Request;

class PersonalReadStatisticsController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '塾生读书打卡统计';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        //dd(Request::get());

        $adminController = new AdminApiController();
        //var_dump($_REQUEST);
        $grid = new Grid(new Statistics());

        $grid->filter(function($filter){

            // 去掉默认的id过滤器
            $filter->disableIdFilter();

            $filter->equal('team_id','小组')->select(
                ActivityTeamModel::where('team_type',3)->pluck('team_name','id')
            );
            $filter->between('start_time','时间')->date();
        });

        //$grid = new \Lauwen\Grid\Grid(new Statistics());
        $number = 0;
        if(!empty($_REQUEST['team_id']) && !empty($_REQUEST['start_time'])){
            $team_id = $_REQUEST['team_id'];
            $start_time = $_REQUEST['start_time'];
            $number = $adminController->get_team_statistics($team_id,$start_time['start'],$start_time['end']);

        }
        $grid->header(function ($query)use($number) {
            $team_number = 0;
            $study_number = 0;
            $wdk_number = 0;
            $total_number = 0;
            //$number = 0;
            if(!empty($_REQUEST['team_id']) && !empty($_REQUEST['start_time'])){
                //dd($query->start_time);
                $team_id = $_REQUEST['team_id'];
                $start_time = $_REQUEST['start_time'];
               /* $number = $adminController->get_team_statistics($team_id,$start_time['start'],$start_time['end']);*/
                //var_dump($number);
                $team_number = ActivityTeamUser::where('team_id',$team_id)
                    //->where('start_time','>=',$start_time['start'])
                    //->where('start_time','<=',$start_time['end'])
                    ->count();

                $study_number = StudyModel::where('team_id',$team_id)
                    ->where('created_at','>=',$start_time['start'])
                    ->where('created_at','<=',$start_time['end'])
                    ->groupBy('user_id')
                    ->get()
                    ->count();
                //var_dump($study_number);die;
                $wdk_number = $team_number - $study_number;
                $total_number = StudyModel::where('team_id',$team_id)
                    ->count();
            }
            return "<table class=\"table table-bordered\">
                <tr>
                    <th>应打卡人数</th>
                    <th>实际打卡人数</th>
                    <th>未打卡人数</th>
                    <th>总打卡次数</th>
                </tr>
                <tr>
                    <td>$team_number</td>
                    <td>$study_number</td>
                    <td>$wdk_number</td>
                    <td>$total_number</td>
                </tr>
            </table>";
        });



        $grid->disableCreateButton();
        $grid->disableRowSelector();
        $grid->disableActions();
        $grid->disableColumnSelector();

        $grid->model()->where('team_type',3);
        $grid->column('team.team_name','小組名称');
        $grid->column('activity_class.personal_title','活動名称');
        $grid->column('user.nickname','用户名称名称');
        //$grid->column('team_user_stash', __('Team user stash'));
        //$grid->column('team_type', __('Team type'));
        //$grid->column('name', '填写名称');
        //$grid->column('company_name','填写公司');
        //$grid->column('start_time','开始时间');

        $grid->column('total','应打卡数')->display(function()use($number){

            return $number;

        });

        $grid->column('user_id','实际打卡数')->display(function($user_id)use($number){
            $my_study_number = 0;
            if(!empty($_REQUEST['team_id']) && !empty($_REQUEST['start_time'])){
                $my_study_number = StudyModel::where('team_id',$_REQUEST['team_id'])
                    ->where('created_at','>=',$_REQUEST['start_time']['start'])
                    ->where('created_at','<=',$_REQUEST['start_time']['end'])
                    ->where('user_id','=',$user_id)
                    //->groupBy('user_id')
                    ->get()
                    ->count();

            }
            return $my_study_number;

        });

        return $grid;
    }

}
