<?php

namespace App\Admin\Controllers;

use App\Models\ActivityClassBook;
use App\Models\ActivityClassModel;
use App\Models\BookModel;
use App\Models\CompanyModel;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Grid\Displayers\Actions;
use Encore\Admin\Show;
use App\Models\ArticleModel;
use Illuminate\Support\Facades\DB;

class ActivityClassController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '活动列表';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ActivityClassModel());

        // 最原始的`按钮图标`形式
        $grid->setActionClass(Actions::class);

        $grid->column('id', __('Id'))->style('display:table-cell;vertical-align:middle;text-align:center;');
        //$grid->column('sort_id', __('Sort id'));
        $grid->column('class_name', '大組名称')->style('display:table-cell;vertical-align:middle;text-align:center;');
        //$grid->column('author', '负责人');
        //$grid->column('class_cove', '封面图')->image('',200,200)->style('display:table-cell;vertical-align:middle;text-align:center;');
        //$grid->column('start_time', '开始时间')->style('display:table-cell;vertical-align:middle;text-align:center;');
        $grid->column('class_number', '参与人数')->style('display:table-cell;vertical-align:middle;text-align:center;');
        $grid->column('order', '大組排序')->style('display:table-cell;vertical-align:middle;text-align:center;');
        $grid->column('created_at', '创建时间')->style('display:table-cell;vertical-align:middle;text-align:center;');
        $grid->column('updated_at','更新时间')->style('display:table-cell;vertical-align:middle;text-align:center;');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(ActivityClassModel::findOrFail($id));

        $show->field('id', __('Id'));
        //$show->field('sort_id', __('Sort id'));
        $show->field('class_name', '大組名称');
        //$show->field('author', '负责人');
        //$show->class_cove('封面图')->image();
        //$show->field('start_time', '开始时间');
        $show->field('class_number', '参与人数');
        $show->field('order', '大組排序');
        $show->field('created_at', '创建时间');
        $show->field('updated_at','更新时间');


        $show->many_activity_class_book('绑定书本', function ($book) {
            $book->setResource('/admin/activity-class-books');
            $book->book_id('书本名称')->display(function ($book_id)use ($book){
                return BookModel::where('id',$book_id)->value('book_title');
            });
        });

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new ActivityClassModel());

        $form->hidden('id');
        $form->text('class_name', '大組名称');

/*        $form->image('class_cove', '封面图')
            ->move('/upload/images/'.date('Ymd'));*/

        //$form->datetime('start_time', '开始时间')->default(date('Y-m-d H:i:s'));

        $form->number('class_number', '参与人数')->default(0);
        //$form->number('order', '大組排序')->default(0);

        //$form->text('Invitation_code', '邀請碼');

        //$form->multipleSelect('activity_class_book', '綁定書本')->options(BookModel::pluck('book_title', 'id'));
       // $form->listbox('activity_class_book','綁定書本')->options(BookModel::pluck('book_title', 'id'));
        $form->listboxsortable('activity_class_book','綁定書本')->options(BookModel::pluck('book_title', 'id'));

        //保存后回调
        $form->saved(function (Form $form) {

            $activity_class_book = $form->activity_class_book;

            foreach ($activity_class_book as $k=>$v){
                //dd($form->model()->id,$k,$v);
                ActivityClassBook::where('activity_class_id',$form->model()->id)->where('book_id',$v)->update([
                   'sort'=>$k+1
                ]);
            }

        });

        return $form;
    }
}
