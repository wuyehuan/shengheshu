<?php

namespace App\Admin\Controllers;

use App\Models\BannerModel;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class BannerController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '轮播图管理';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new BannerModel());

        $grid->column('id', __('Id'));
        $grid->column('banner_img', '轮播图')->image('',200,200);
        $grid->column('order','排序');
        $grid->column('created_at', '创建时间');
        $grid->column('updated_at', '更新时间');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(BannerModel::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('banner_img', '轮播图')->image();
        $show->field('order','排序');
        $show->field('created_at', '创建时间');
        $show->field('updated_at', '更新时间');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new BannerModel());

        $form->image('banner_img', '轮播图')
            //->image('article_cover','封面图')
            ->move('/upload/images/'.date('Ymd'))
            ->uniqueName();
        $form->number('order','排序');

        return $form;
    }
}
