<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UploadController extends Controller
{
    // 上傳管理

    /**
     * @method post
     * 圖片上傳
     */
    public function image(Request $request){
        try{
            // 获取文件
            $images = $request -> file('upload');

            if(!$images){
                throw new \Exception('请选择需要上传的文件');
            }

            if (!$images -> isValid()) {
                throw new \Exception('文件类型不正确');
            }

            // 获取文件大小
            $size = $images -> getSize();
            if ($size > 5000000) {
                throw new \Exception('图片不能大于5M');
            }

            // 获取文件相关信息 扩展名
            $ext = $images->getClientOriginalExtension();
            if (!in_array($ext, ['png', 'jpg', 'gif', 'jpeg'])) {
                throw new \Exception('文件类型不正确');
            }

            // 临时文件的绝对路径
            $realPath = $images->getRealPath();

            // 上传文件
            $filename = 'editor/' . date('Ymd') . '/' . uniqid() . '.' . $ext;

            // 使用我们新建的uploads本地存储空间（目录）
            $bool = Storage::disk('admin') -> put($filename, file_get_contents($realPath));
            if(!$bool) {
                throw new \Exception('文件上传失败');
            }
            return getDomain().'/upload/' . $filename;
        }catch (\Exception $e){

            return '';
            return response() -> json([
                'error_code' => 0,
                'msg' => '图片上传成功',
                'data' => null
            ]);
        }
    }
}
