<?php

namespace App\Admin\Controllers;

use App\Models\ActivityClassBook;
use App\Models\ActivityClassModel;
use App\Models\BookModel;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Models\WorkModel;
use App\Models\ClassWorkModel;

class ActivityClassWorkController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '作业安排';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ActivityClassModel());

        $grid->column('id', __('Id'));
        //$grid->column('sort_id', __('Sort id'));
        $grid->column('class_name', '活动名称');
        //$grid->column('author', __('Author'));
        $grid->column('class_cove', '封面')->image('',120,120);
        //$grid->column('start_time', __('Start time'));
        //$grid->column('class_number', __('Class number'));
        //$grid->column('order', __('Order'));
        //$grid->column('Invitation_code', __('Invitation code'));
        $grid->column('created_at', '活动创建时间');
        //$grid->column('updated_at', __('Updated at'));
        //$grid->column('company_id', __('Company id'));
        $grid->disableCreateButton();

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(ActivityClassModel::findOrFail($id));

        $show->field('id', __('Id'));
        //$show->field('sort_id', __('Sort id'));
        $show->field('class_name', '活动名称');
        //$show->field('author', __('Author'));
        $show->field('class_cove', '封面');
        //$show->field('start_time', __('Start time'));
        //$show->field('class_number', __('Class number'));
        //$show->field('order', __('Order'));
        //$show->field('Invitation_code', __('Invitation code'));
        $show->field('created_at', '活动创建时间');
        //$show->field('updated_at', __('Updated at'));
        //$show->field('company_id', __('Company id'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new ActivityClassModel());
        $id = '';
        if ($form->isEditing()){
            $id=request()->route()->parameters()['activity_class_work'];
        }

        $form->hasMany('activity_class_work', '安排作业', function (Form\NestedForm $form)use($id) {
            $book_ids = ActivityClassBook::where('activity_class_id',$id)->pluck('book_id');
            $form->select('book_id','书本')->options(
                BookModel::whereIn('id',$book_ids)->pluck('book_title', 'id')
            );

            $form->select('type','作业类型')->options(
                //1心得(模板格式) 2读原文\\录音 3心得（富文本）
                [
                    1=>'多标题作业',
                    2=>'录音',
                    3=>'单节课标题',
                ]
            )->load('work_id','/admin/get_work');

            $form->select('work_id','作业')->options(

            );

            $states = [
                'on'  => ['value' => 1, 'text' => '是', 'color' => 'success'],
                'off' => ['value' => 2, 'text' => '否', 'color' => 'danger'],
            ];

            $form->switch('is_points','是否计算积分')->states($states);
            $form->text('points_value','积分值');

            $form->hidden('key_id','key_id2')->value($id);
            $form->hidden('class_type','class_type')->value(2);
            $form->hidden('activity_class_id','activity_class_id')->value($id);

        });

        return $form;
    }
}
