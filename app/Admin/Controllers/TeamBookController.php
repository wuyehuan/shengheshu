<?php

namespace App\Admin\Controllers;

use App\Models\TeamBook;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class TeamBookController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '小組綁定書本';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new TeamBook());

        $grid->column('id', __('Id'));
        $grid->column('team_id', __('Team id'));
        $grid->column('book_id', __('Book id'));
        $grid->column('start_time', __('Start time'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(TeamBook::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('team_id', __('Team id'));
        $show->field('book_id', __('Book id'));
        $show->field('start_time', __('Start time'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new TeamBook());

        $form->number('team_id', __('Team id'));
        $form->number('book_id', __('Book id'));
        $form->datetime('start_time', __('Start time'))->default(date('Y-m-d H:i:s'));

        return $form;
    }
}
