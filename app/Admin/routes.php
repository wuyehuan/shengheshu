<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
    'as'            => config('admin.route.prefix') . '.',
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('home');
    $router->get('getBook', 'AdminApiController@getBook');
    //$router->get('class_article', 'AdminApiController@class_article');
    $router->get('activity_book', 'AdminApiController@activity_book');

    $router->get('audio_article', 'AdminApiController@audio_article');
    $router->get('get_work', 'AdminApiController@get_work');
    $router->get('book_article', 'AdminApiController@book_article');
    $router->get('company_list', 'AdminApiController@company_list');
    $router->get('company_activity_class', 'AdminApiController@company_activity_class');
    $router->get('personal_article', 'AdminApiController@personal_article');
    $router->get('company_read_list', 'AdminApiController@company_read_list');
    $router->get('company_read_book_article', 'AdminApiController@company_read_book_article');
    $router->get('audio_list', 'AdminApiController@audio_list');
    $router->get('get_team_statistics', 'AdminApiController@get_team_statistics');

    $router->get('personal_book', 'AdminApiController@personal_book');

    $router->post('upload', 'UploadController@image');
    //批量審核 单个小组
    $router->post('team-users/release', 'AdminApiController@release');
    //批量审核 多个小组
    $router->post('team-users/all_team_examine', 'AdminApiController@all_team_examine');

    $router->post('redis_team_book_article', 'AdminApiController@redis_team_book_article');


    //书本模块
    $router->resource('book-models', BookController::class);
    //活动模块
   // $router->resource('activity-models', ActivityController::class);
    //文章模块
    $router->resource('article-models', ArticleController::class);
    //作业模块
    $router->resource('work-models', WorkController::class);
    $router->resource('banner-models', BannerController::class);

    //六项精进
    $router->resource('activity-class-models', ActivityClassController::class);
    //书架
    $router->resource('book-shelves', BookShelfController::class);
    //绑定作业
   // $router->resource('class-work-models', ClassWorkController::class);

    //企业读书活动
    $router->resource('company-read-models', CompanyReadController::class);
//企业读书作业
    //$router->resource('company-read-class-work-models', CompanyReadWorkController::class);
    $router->resource('company-read-work', CompanyReadWorkController::class);

    //公司管理
    $router->resource('company-models', CompanyController::class);

    $router->resource('user-models', CompanyUserController::class);
    //宿生读书
    $router->resource('personal-reads', PersonalReadController::class);

    //小组成员管理 (六项精进)
    $router->resource('activity-team-users', TeamUserController::class);
    //小组成员管理 (企业读书)
    $router->resource('company-team-users',CompanyTeamUserController::class);
    //小组成员管理 （塾生学习）
    $router->resource('personal-team-users',PersonalTeamUserController::class);

    //小组
    $router->resource('activity-team-models', TeamController::class);
    //音频学习
    $router->resource('audio-studies', AudioStudyController::class);
    //音频学习任务
   // $router->resource('audio-work-models', AudioStudyWorkController::class);
    //用户管理
    $router->resource('user-models', UserController::class);
    //心得列表
    $router->resource('study-models', StudyController::class);
    //封面
    $router->resource('activity-coves', ActivityCoveController::class);
    //
   // $router->resource('personal-work-models', PersonalWorkController::class);
    //塾生学习布置作业
    $router->resource('personal-read-works', PersonalReadWorkController::class);
    //封面海报
    $router->resource('posters', PosterController::class);
    //设置学分
    $router->resource('set-sign-models', SetSignController::class);
    //书架指南
    $router->resource('bookshelf-guides', BookShelfGuideController::class);
    //塾生学习报名审核
    $router->resource('personal-read-registers', PersonalReadRegisterController::class);
    //二維碼
    $router->resource('qrcodes', QrcodeController::class);
    //手機號
    $router->resource('phones', PhoneController::class);

    //企业读书小组
    $router->resource('company_activity-team-models', CompanyTeamController::class);
    //塾生学习小组
    $router->resource('personal_activity-team-models', PersonalTeamController::class);
    //企业书本管理
    $router->resource('company_book-models', ComoanyBookController::class);

    //休息日设置
    $router->resource('holidays', HolidaysController::class);
    //背景圖
    $router->resource('article-backgrounds', ArticleBackgroundController::class);

    //小組綁定書本
   // $router->resource('team-books', TeamBookController::class);
    //六項精進綁定書本
    $router->resource('activity-class-books', ActivityClassBookController::class);

    //六项精进 布置作业
    $router->resource('activity-class-work', ActivityClassWorkController::class);
    //塾生学习布置作业
    $router->resource('personal-reads-work', PersonalReadWorkController::class);

    //設計系統圖片
    $router->resource('system-images', SystemImagesController::class);

    //打卡統計 六项精进
    $router->resource('statistics', StatisticsController::class);
    //打卡统计 塾生读书
    $router->resource('personal_read_statistics', PersonalReadStatisticsController::class);
    //打卡统计 企业读书
    $router->resource('company_read_statistics', CompanyrRadStatisticsController::class);
});
