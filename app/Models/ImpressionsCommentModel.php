<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ImpressionsModel;

/**
 * App\Models\ImpressionsCommentModel
 *
 * @property int $id
 * @property int $user_id 用户ID
 * @property int $book_id 书本ID
 * @property int $activity_id 活动ID
 * @property int $article_id 文章ID
 * @property int|null $impressions_id 心得ID
 * @property string|null $content 内容
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\ImpressionsModel|null $impressions
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ImpressionsCommentModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ImpressionsCommentModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ImpressionsCommentModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ImpressionsCommentModel whereActivityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ImpressionsCommentModel whereArticleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ImpressionsCommentModel whereBookId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ImpressionsCommentModel whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ImpressionsCommentModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ImpressionsCommentModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ImpressionsCommentModel whereImpressionsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ImpressionsCommentModel whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ImpressionsCommentModel whereUserId($value)
 * @mixin \Eloquent
 */
class ImpressionsCommentModel extends Model
{
    //心得评论
    protected $table='impressions_comment';

    //关联心得
    public function impressions(){
        return $this->belongsTo(ImpressionsModel::class,'impressions_id','id');
    }

    //添加心得
    public function create_comment($data){
        $this->user_id = $data->user()->id;
        $this->book_id = $data->book_id;
        $this->activity_id = $data->activity_id;
        $this->article_id = $data->article_id;
        $this->impressions_id = $data->impressions_id;
        $this->content = $data->content;
        $res = $this->save();
        return $res;
    }

}
