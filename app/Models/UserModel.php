<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use PhpParser\Node\Expr\FuncCall;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\TeamUserModel;
use App\Models\StudyModel;
use Illuminate\Support\Facades\DB;

/**
 * App\Models\UserModel
 *
 * @property int $id
 * @property string $account 登录账号
 * @property string $password 密码
 * @property string $nickname 昵称
 * @property string $name 真实姓名
 * @property string $wx_openid 微信openid
 * @property string $phone 手机号
 * @property string $mail 邮箱
 * @property float $status 账号状态 1正常 2封停
 * @property float $identity 身份 1游客 2企业验证负责人 3企业员工
 * @property float $identity_status 身份审核 1未审核 2审核中 3审核通过 4审核不通过
 * @property string $last_used_at 最后登录时间
 * @property int $company_id 绑定的企业ID
 * @property string $Invitation_code 企业邀请码
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserModel whereAccount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserModel whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserModel whereIdentity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserModel whereIdentityStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserModel whereInvitationCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserModel whereLastUsedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserModel whereMail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserModel whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserModel whereNickname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserModel wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserModel wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserModel whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserModel whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserModel whereWxOpenid($value)
 * @mixin \Eloquent
 */
class UserModel extends Authenticatable implements JWTSubject
{
    use Notifiable;

    //用户表
    protected $table='user';

    protected $fillable = [
        'nickname',
        'avatar',
        'phone',
        'company_id',
        'Invitation_code',
        'total_integral',
        'user_integral',
        'activity_team_integral',
        'company_read_integral',
        'audio_study_integral',
    ];
    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     *加密
     */
    public function setPasswordAttribute($value){
        $this -> attributes['password'] = bcrypt($value);
    }

    public function getAvatarAttribute($value){
       return oss_domain().'/'.$value;
    }

    //关联用户 用户读书小组
    public function team_user(){
        return $this->hasMany(TeamUserModel::class,'user_id','id');
    }

    //关联学习记录 用户学习记录
    public function study(){
        return $this->hasMany(StudyModel::class,'user_id','id');
    }

    public function company(){
        return $this->belongsTo(CompanyModel::class,'company_id','id');
    }

    //获取某字段的值
    public function get_one($column=['*'],$user_id){
        return UserModel::select($column)->where('id',$user_id)->first();
    }

    //获取某字段的值
    public function get_user($user_id){
        //dd($user_id);
        $user = UserModel::select([
            'id',
            'nickname',
            'avatar',
            'name',
            'wx_openid',
            'phone',
            'mail',
            'identity',
            'identity_status',
            'last_used_at',
            'company_id',
            'Invitation_code',
            'total_integral',
            'user_integral',
            'activity_team_integral',
            'company_read_integral',
            'audio_study_integral',
            //'personal_read_integral',
        ])
            ->where('id',$user_id)
            ->first();

        $num = \DB::table('sign_team')
            ->where('user_id',$user_id)
            ->value('num');
        //获取阅读天数
        $user->continuity_date = empty($num)?0:$num;

        return $user;

    }

    public function others_index($user_id,$my_user_id){
        //dd($user_id);
        $user = UserModel::select([
            'id',
            'nickname',
            'avatar',
            //'name',
            //'wx_openid',
            'phone',
            'mail',
            'identity',
            'identity_status',
            'last_used_at',
            'company_id',
            'Invitation_code',
            'total_integral',
            'user_integral',
            'activity_team_integral',
            'company_read_integral',
            //'personal_read_integral',
        ])
            ->with([
                'study'=>function($q)use($user_id,$my_user_id){
                    $q->select([
                        'id',
                        'type',
                        'user_id',
                        'study_json_content',
                        'study_url',
                        'study_content',
                        'article_id',
                        'is_opend',
                    ])
                    ->where('is_opend',2)
                    ->with([
                        'article'=>function($q){
                            $q->select([
                                'id',
                                'article_leader',
                                'article_title',
                               // 'article_content',
                            ]);
                        },
                        'all_support_avatar'=>function($q){
                            $q->select(['user.id as user_id','user.avatar']);
                        },
                        'my_support'=>function($q)use($my_user_id){
                            $q->where('user_id',$my_user_id)->select(['id','created_at','study_id']);
                        },
                    ]);
                },
                'company'=>function($q){
                    $q->select([
                        'id',
                        'company_name',
                    ]);
                },
            ])
            ->where('id',$user_id)
            ->first();


        //获取阅读天数
        $user->continuity_date = \DB::table('sign_team')
            ->where('user_id',$user_id)
            ->value('num');
        return $user;
    }

    //账号获取
    public function account_user($account){
        return $this->where('account',$account)->first();
    }


    public function bind_company($company,$user_id){
        return $this->where('id',$user_id)
            ->update([
                'Invitation_code'=>$company->Invitation_code,
                'company_id'=>$company->id,
                'identity'=>3,
                'identity_status'=>2,
            ]);
    }

    public function edit_user_info($data,$user_id){
        return $this->where('id',$user_id)
            ->update($data);
    }
}
