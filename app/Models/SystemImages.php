<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SystemImages extends Model
{
    //
    protected $table = 'system_images';

    public function getImgUrlAttribute($value)
    {
        return oss_domain().$value;
    }
}
