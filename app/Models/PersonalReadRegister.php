<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PersonalReadRegister extends Model
{
    protected $fillable=[
        'personal_read_id',
        'user_id',
        'user_id',
        'company_name',
        'stash',
        'phone',
    ];

    //塾生讀書报名
    protected $table='personal_read_register';

    public function create_register($data){
        return $this->create($data);
    }
}
