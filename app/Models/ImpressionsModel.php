<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ImpressionsModel
 *
 * @property int $id
 * @property int $book_id 书本ID
 * @property int $activity_id 活动ID
 * @property int $article_id 文章ID
 * @property int $user_id 用户ID
 * @property string|null $content 心得内容
 * @property float|null $is_open 是否公开 1自己可看 2小组可看 3全部可看
 * @property float|null $is_draft 是否草稿 1是 2否
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ImpressionsCommentModel[] $comment
 * @property-read int|null $comment_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ImpressionsModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ImpressionsModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ImpressionsModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ImpressionsModel whereActivityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ImpressionsModel whereArticleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ImpressionsModel whereBookId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ImpressionsModel whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ImpressionsModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ImpressionsModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ImpressionsModel whereIsDraft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ImpressionsModel whereIsOpen($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ImpressionsModel whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ImpressionsModel whereUserId($value)
 * @mixin \Eloquent
 */
class ImpressionsModel extends Model
{
    //心得
    protected $table='impressions';

    //关联评论
    public function comment(){
        return $this->hasMany(ImpressionsCommentModel::class,'impressions_id','id');
    }

    //我的心得
    public function my_impressions($user_id,$is_open=[1,2],$is_draft=[1,2]){
        return $this->where('user_id',$user_id)
            ->whereIn('is_open',$is_open)
            ->whereIn('is_draft',$is_draft)
            ->paginate(15);
    }

    //添加心得
    public function create_impressions($data){
        $this->book_id = $data->book_id;
        $this->activity_id = $data->activity_id;
        $this->article_id = $data->article_id;
        $this->user_id = $data->user()->id;
        $this->content = $data->content;
        $res = $this->save();
        return $res;
    }

    //查看心得
    public function impressions_info($id){
        return $this->where('id',$id)
            ->with(['comment'=>function($q){
                $q->select(['content','user_id','impressions_id']);
            }])
            ->first();
    }
    
}
