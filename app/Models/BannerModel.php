<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\BannerModel
 *
 * @property int $id
 * @property string $banner_img 轮播图URl
 * @property int $order 排序
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BannerModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BannerModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BannerModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BannerModel whereBannerImg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BannerModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BannerModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BannerModel whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BannerModel whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class BannerModel extends Model
{
    //轮播
    protected $table='banner';

    public function getBannerImgAttribute($value)
    {
        return oss_domain().$value;
    }
}
