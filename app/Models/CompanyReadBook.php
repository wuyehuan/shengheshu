<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyReadBook extends Model
{
    //企业读书绑定书本
    protected $table = 'company_read_book';

    public function article(){
        return $this->hasMany(ArticleModel::class,'book_id','book_id');
    }

    public function work(){
        return $this->hasMany(WorkModel::class,'key_id','id');
    }

}
