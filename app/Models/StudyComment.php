<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Tests\Models\User;

class StudyComment extends Model
{
    //作业评论表
    protected $table='study_comment';

    protected $fillable=[
        'user_id',
        'study_id',
        'comment',
    ];

    public function user(){
        return $this->belongsTo(UserModel::class,'user_id','id');
    }

    public function create_study_comment($data){
        return $this->create($data);
    }

    public function del_study_comment($study_comment_id,$user_id){
        return $this->where('id',$study_comment_id)
            ->where('user_id',$user_id)
            ->delete();
    }
}
