<?php

namespace App\Models;

use App\Models\ArticleModel;
use Illuminate\Database\Eloquent\Model;
use App\Models\SortModel;
use phpDocumentor\Reflection\Types\False_;
use App\Models\BookShelfRegister;
use App\Services\Api\BaseService;

/**
 * App\Models\BookModel
 *
 * @property int $id
 * @property string $book_title 书籍名称
 * @property string $book_logo 书籍Logo
 * @property string|null $author 作者
 * @property int $article_number 章节数
 * @property int $sort_id 分类ID
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ArticleModel[] $article
 * @property-read int|null $article_count
 * @property-read \App\Models\SortModel $sort
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookModel whereArticleNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookModel whereAuthor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookModel whereBookLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookModel whereBookTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookModel whereSortId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookModel whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class BookModel extends Model
{
    //书本表
    protected $table='book';

    //关联书本 与 公司 权限
    public function book_permission(){
       return $this->belongsToMany(CompanyModel::class, 'book_permission','book_id', 'company_id');
    }

    //关联分类
    public function sort(){
        return $this->belongsTo(SortModel::class,'id','sort_id');
    }

    //关联文章
    public function article(){
        return $this->hasMany(ArticleModel::class,'book_id','id');
    }

    public function getBookLogoAttribute($value){
        return oss_domain().'/'.$value;
    }

    //article_number
    public function getArticleNumberAttribute($value){
        //dd($this->id);
        return ArticleModel::where('book_id',$this->id)->where('book_id','!=',0)->count();
    }

    //获取书本详情
    public function book_info($id,$user_id){
        $data=[
          'book_id'=>$id,
          'user_id'=>$user_id,
        ];
        $BookRecord = new BookRecord;
        //非登记用户 只能读3本书
        $user_register = BookShelfRegister::where('user_id',$user_id)->first();
        $is_register = 1;
        if(empty($user_register)){
            $is_register = 2;
            $number = $BookRecord->where('user_id',$user_id)->where('book_id',$id)->count();

            if($number >= 3){
                $BaseService = new BaseService();
                return $BaseService->fail(1100002);
            }
        }

        $BookRecord->create_book_record($data);
        $data = $this
            ->where('id',$id)
            ->with(['article'=>function($query){
                $query->select(['id','article_title','article_cover','reader_link','book_id'])->orderBy('order','asc');
            }])
            ->first();

        $data->type = BookShelf::where('book_id',$id)->value('type');

        $data->is_register = $is_register;
        return $data;
    }

    public function book_base_info($id){
        return $this
            ->where('id',$id)
            ->first();
    }

}
