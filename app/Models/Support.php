<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Support extends Model
{
    //点赞文章
    protected $table = 'support';

    protected $fillable = [
        'user_id',
        'article_id',
        'study_id',
        'created_at',
    ];

    public function create_support($data){
        return $this->create($data);
    }

    public function check_support($study_id,$user_id){
        return $this->where('study_id',$study_id)
            ->where('user_id',$user_id)
            ->first();
    }
}
