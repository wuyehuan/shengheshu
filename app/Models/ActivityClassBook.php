<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActivityClassBook extends Model
{
    //六項精進綁定書本
    protected $table = 'activity_class_book';

    public function article(){
        return $this->hasMany(ArticleModel::class,'book_id','book_id');
    }

    public function work(){
        return $this->hasMany(WorkModel::class,'key_id','id');
    }
}
