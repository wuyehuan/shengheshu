<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActivityCove extends Model
{
    //
    protected $table='activity_cove';

    public function getCoveImgAttribute($value)
    {
        return oss_domain().$value;
    }
}
