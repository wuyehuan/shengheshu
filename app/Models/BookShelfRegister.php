<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookShelfRegister extends Model
{
    //书架登记记录
    protected $table = 'bookshelf_register';
}
