<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookshelfGuide extends Model
{
    protected $table='bookshelf_guide';
}
