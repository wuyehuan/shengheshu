<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SignTeamModel extends Model
{
    //小组签到
    protected $table='sign_team';

    public function team_personal_sign($user_id,$teamUserObj,$class_work,$is_calculation = true){
        DB::beginTransaction(); //开启事务
        try {
            //重新获取用户信息
            $user = UserModel::select([
                'id',
                'company_id',
                'user_integral',
                'total_integral',
                'activity_team_integral',
                'company_read_integral',
                'audio_study_integral',
                ])
                ->where('id',$user_id)
                ->first();

            $integral_day = 0;

            //查询是否获得积分
            if($class_work->is_points == 1 && $is_calculation){
                $integral_day = $class_work->points_value;
            }
            $user->total_integral = $user->total_integral + $integral_day;

            $user->save();

            //更新小组分数
            $team_obj = ActivityTeamModel::where('id',$teamUserObj->team_id)->first();
            $team_obj->team_integral = $team_obj->team_integral + $integral_day;
            $team_obj->save();

            $num = $this->where('user_id',$user_id)
                ->orderBy('id','desc')
                ->where('team_id',$teamUserObj->team_id)
                //->where('sign_type',1)
                ->value('num');
            $sign_num = $num;
            $this->fresh();

            $sign_num = !empty($sign_num)?$sign_num:0;
            $this->user_id = $user_id;
            $this->team_id = $teamUserObj->team_id;

            $this->key_id = $team_obj->key_id;
            $this->team_type = $team_obj->team_type;

            $this->num = $sign_num + 1;
            $this->integral = $integral_day;

            $res = $this->save();

            $data = [
                'user_id'=>$user_id,
                'integral'=>$integral_day,
                'team_id'=>$teamUserObj->team_id,
                'integral_type'=>$team_obj->team_type,
            ];
            $IntegralChangeModel = new IntegralChange();
            $IntegralChangeModel->create_integral_change($data);
            DB::commit();
            return $res;
        }catch (\Exception $e) {
            DB::rollBack();
            Log::info('用户团队打卡签到失败'.$e -> getMessage());
            return false;
        }
    }

    public function sign_team_order($user_id){
        //获取全部小组ID
        $team_ids = TeamUserModel::where('user_id',$user_id)->pluck('team_id')->toArray();
        //dd($team_ids->toArray());die;
        return $sign_data = $this->whereIn('team_id',$team_ids)
            //->where('user_id',$user_id)
            // ->where('sign_type',2)
            ->select([
                'user_id',
                'team_id',
                'activity_id',
                'integral',
                \DB::raw("sum(integral) as total_integral"),
            ])
            // ->groupBy('team_id','user_id')
            //->orderBy('total_integral')
            ->get()
            ->toArray();

    }

}
