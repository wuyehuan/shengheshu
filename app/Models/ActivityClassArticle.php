<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActivityClassArticle extends Model
{
    //活动文章绑定
    protected $table='activity_class_article';

    //关联作业
   public function work(){
        return $this->hasMany(ClassWorkModel::class,'activity_class_id','class_id');
   }
}
