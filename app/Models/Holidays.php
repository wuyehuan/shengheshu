<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Holidays extends Model
{
    //休息日表
    protected $table='holidays';

    //wee_holiday
    public function setWeeHolidayAttribute($value){
        return $this->attributes['wee_holiday'] =  implode(',',$value);
    }

    public function getWeeHolidayAttribute($value){
        return explode(',',$value);
    }

}
