<?php

namespace App\Models;

use App\Services\Api\WorkService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use App\Admin\Controllers\AdminApiController;

class CompanyReadModel extends Model
{
    //企业读书
    protected $table='company_read';

    public function book(){
        return $this->belongsToMany(BookModel::class, 'company_read_book',
            'comapny_read_id', 'book_id');

    }

    public function getCoveUrlAttribute($value){
        if(empty($value)){
            return '';
        }
        return oss_domain().'/'.$value;
    }


    //作业
    public function company_read_work(){
        return $this->hasMany(ClassWorkModel::class, 'company_read_id','id');
    }

    public function many_book(){
        return $this->hasMany(CompanyReadBook::class,'comapny_read_id','id');
    }

    public function article(){
        return $this->hasMany(ArticleModel::class,'book_id','book_id');
    }

    public function class_team(){
        return $this->belongsTo(ActivityTeamUser::class,'id','key_id');
    }

    public function company_read_list($company_id,$user_id){
        if(empty($company_id)){
            $query = $this->where('company_id',$company_id);
        }else{
            $query = $this->select(['*']);
        }
        $data = $query
            ->with([
                'class_team'=>function($q)use($user_id){
                    $q->where('user_id',$user_id)->select(['team_id','key_id','team_user_stash','created_at'])
                        ->where('team_type',1);
                }
            ])
            ->orderBy('id','desc')
            ->paginate(3)
            ->toArray();
        foreach ($data['data'] as $k=>$v){
            if(!empty($v['class_team'])){
                $data['data'][$k]['order'] = 1;
                //获取缓存
                $redis_key = env('REDIS_KEY_TEAM_WORK').$v['class_team']['team_id'];
                $work_data = Cache::get($redis_key);
                if(!empty($work_data)){
                    $work_data = json_decode($work_data,true);
                }else{
                    $api = new AdminApiController();
                    $res = $api->redis_team_book_article($v['class_team']['key_id'],1,$v['class_team']['team_id']);
                    $work_data = Cache::get($redis_key);
                    $work_data = json_decode($work_data,true);
                }
                $today = date("Y-m-d");
                if(!empty($work_data[$today])){
                    $data['data'][$k]['class_team']['read_book'] = $work_data[$today];
                }else{
                    $data['data'][$k]['class_team']['read_book'] = [];
                }
            }else{
                $data['data'][$k]['order'] = 0;
            }
        }
        $data['data'] = arraySort($data['data'], 'order');
        return $data;
    }

    public function company_read_info($user_id,$company_read_id){
/*        $teamObj = ActivityTeamUser::where('user_id',$user_id)
            ->where('key_id',$company_read_id)
            ->where('team_type',1)
            ->with(['team'])
            ->first();
        if(empty($teamObj)){
            return [];
        }

        if(!empty($teamObj->wee_holiday)){
            $w = date("w");
            //$wee_holiday = explode(',',$teamObj->wee_holiday);
            $wee_holiday = $teamObj->wee_holiday;

            if(in_array($w,$wee_holiday) && $w!=0){

                return [];
            }elseif ($w==0 && in_array('7',$wee_holiday)){

                return [];
            }

            $sta = strtotime($teamObj->day_holiday_start);
            $end = strtotime($teamObj->day_holiday_end);
            if($sta >0 && $end>0){
                //dd($teamObj->id,$sta,$end);
                $now_time = time();
                if($now_time>=$sta && $now_time<=$end){
                    return [];
                }
            }

        }

        $total_days = app(WorkService::class)->get_total_work_date($teamObj->team->start_time,$teamObj->team->wee_holiday,$teamObj->team->day_holiday_start,$teamObj->team->day_holiday_end);

        $book = CompanyReadBook::where('comapny_read_id',$company_read_id)
            ->with([
                'article'
            ])
            ->orderBy('sort','asc')
            ->get()
            ->toArray();
        $article_number = 0;
        foreach ($book as $k=>$v){
            $article_number += count($v['article']);
            //排到那一本书
            if($article_number >= $total_days){
                $book_id = $v['book_id'];
                //計算该本书 排到第几章 不是第一本
                if($v['sort'] != 1){
                    $total_days = $total_days - count($v['article']);
                }
                break;
            }
        }

        $data = $this->where('id',$company_read_id)
            ->first();
        if(!empty($data)){
            $data->book = BookModel::where('id',$book_id)
                ->select([
                    'id',
                    'book_title',
                    'book_logo',
                    'author',
                    'synopsis',
                    'article_number',
                    'created_at',
                ])->first();
            $data->article = ArticleModel::where('book_id',$book_id)
                ->select([
                    'id',
                    'book_id',
                    'article_title',
                    'article_cover',
                    'reader_link',
                    'article_leader',
                ])
                ->take($total_days)
                ->get();
        }
        return $data;*/
    }

    public function company_read_array($company_id){
        $data =  $this
                ->where('company_id',$company_id)
                ->select([
                    'id',
                    'book_id as name',
                ])
                ->orderBy('created_at','desc')
                ->paginate(15)
                ->toArray();
        if(count($data['data'])>0){
            $bok_ids = array_column($data['data'], 'name');

            $data2 = BookModel::whereIn('id',$bok_ids)->pluck('book_title','id')->toArray();

            foreach ($data['data'] as $k=>$v){
                if(!empty($data2[$v['name']])){
                    $data['data'][$k]['name'] = $data2[$v['name']];
                }

            }
        }
        return $data;
    }
}
