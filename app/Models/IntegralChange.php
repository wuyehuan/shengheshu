<?php

namespace App\Models;

use App\Rules\CheckCreateTeam;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class IntegralChange extends Model
{
    //积分变动记录
    protected $table='integral_change';

    protected $fillable=[
        'id',
        'user_id',
        'integral',
        'team_id',
        'integral_type',
        'created_at',
    ];

    public function user(){
        return $this->belongsTo(UserModel::class,'user_id','id');
    }

    public function create_integral_change($data){
        return $this->create($data);
    }

    public function integral_record($company_id){
        return $this->where('company_id',$company_id)
            ->groupBy('team_id')
            ->orderby('created_at','desc')
            ->get();
    }
}
