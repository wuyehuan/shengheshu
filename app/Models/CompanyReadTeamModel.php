<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyReadTeamModel extends Model
{
    //参与企业读书的小组
    protected $table='company_read_team';

}
