<?php

namespace App\Models;

use App\Models\ClassWorkModel;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ArticleModel
 *
 * @property int $id
 * @property string $article_title 文章标题
 * @property string $article_content 文章内容
 * @property int $book_id 书本ID
 * @property string $article_cover 封面图
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ClassWorkModel[] $class_work
 * @property-read int|null $class_work_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleModel whereArticleContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleModel whereArticleCover($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleModel whereArticleTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleModel whereBookId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleModel whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ArticleModel extends Model
{
   // protected $guarded = ['aa'];

    //章节表
    protected $table='article';

    protected $fillable= [
        'id',
        'order',
    ];

    public function getArticleCoverAttribute($value){
        $Model = new ArticleBackground();
        $cover = $Model->value('url');
        return oss_domain().$cover;
    }

    public function getArticleLeaderAttribute($value){
        if(empty($value)){
            return '';
        }
        return oss_domain().'/'.$value;
    }

    public function book(){
        return $this->belongsTo(BookModel::class,'book_id','id');
    }

    public function collection(){
        return $this->hasOne(CollectionArticle::class,'article_id','id');
    }

    public function class_work(){
        return $this->hasMany(ClassWorkModel::class,'book_id','book_id');
    }



    public function article_info($article,$user_id){
        $data =  $this->where('id',$article)
            ->with([
                'collection'=>function($q)use($user_id){
                    $q->where('user_id',$user_id)->select([
                        'id',
                        'article_id',
                        'created_at',
                    ]);
                },
                'book'=>function($q){
                    $q->select([
                        'id',
                        'book_title',
                        'book_logo',
                        'author',
                    ]);
                }
            ])
            ->first();
        $text =preg_replace('#</p[^>]*>#i','\n ',$data->article_content);
        $text =preg_replace('#<p[^>]*>#i','&emsp;&emsp;',$text);
        $data->text = strip_tags($text);
        return $data;
    }

}
