<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Tests\Models\User;

class Statistics extends Model
{
    //打卡統計用
    protected $table = 'activity_team_user';

   // protected $appends = ['study_number'];

    public function user(){
        return $this->belongsTo(UserModel::class,'user_id','id');
    }

    public function team(){
        return $this->belongsTo(ActivityTeamModel::class,'team_id','id');
    }

    public function activity_class(){
        return $this->belongsTo(ActivityClassModel::class,'key_id','id');
    }

    public function company_read(){
        return $this->belongsTo(CompanyReadModel::class,'key_id','id');
    }

    public function personal_read(){
        return $this->belongsTo(PersonalRead::class,'key_id','id');
    }

    public function getStudyNumberArrayableAttribute()
    {

    }

}
