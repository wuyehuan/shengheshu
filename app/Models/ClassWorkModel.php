<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ClassWorkModel
 *
 * @property int $id
 * @property int $article_id 文章ID
 * @property int $activity_id 活动ID
 * @property int $activity_class_id 活动ID
 * @property int $work_id 任务ID
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\WorkModel|null $work
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClassWorkModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClassWorkModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClassWorkModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClassWorkModel whereActivityClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClassWorkModel whereActivityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClassWorkModel whereArticleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClassWorkModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClassWorkModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClassWorkModel whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClassWorkModel whereWorkId($value)
 * @mixin \Eloquent
 */
class ClassWorkModel extends Model
{
    //活动任务
    protected $table='class_work';

    protected $appends=[
        'work_title',
        //'type',
    ];

    protected $fillable=[
      'article_id',
      'key_id',
      'work_id',
      'class_type',
      'type',
      'activity_class_id',
      'company_read_id',
      'personal_read_id',
      'audio_study_id',
    ];

    public function getWorkTitleAttribute()
    {
        return WorkModel::where('id',$this->work_id)->value('title');
    }

    public function work(){
        return $this->belongsTo(WorkModel::class,'work_id','id');
    }

    public function study_work(){
        return $this->hasOne(StudyModel::class,'class_work_id','id');
    }


    /*public function article(){
        return $this->belongsTo(ArticleModel::class,'article_id','id');
    }*/

    public function class_work(){
        return $this->belongsToMany(WorkModel::class, 中间表表名,
            当前模型在中间模型中的外键名称, 关联模型在中间模型的外键名称);
    }

    public function article_calss_work($book_id,$article_id,$key_id,$user_id,$class_type,$company_id){
       //除了塾生学习 其他活动都改为 2020/08/21
        if($class_type != 3){
            $q = $this
                ->where('book_id',$book_id);
                //->where('company_id',$company_id);
        }else{
            $q = $this
                ->where('book_id',$book_id);
        }
        return
        $q
            ->where('key_id',$key_id)
            ->where('class_type',$class_type)
            ->with([
                'study_work'=>function($q)use($user_id,$class_type,$article_id){
                    $q->where('user_id',$user_id)
                        ->where('article_id',$article_id)
                        ->select([
                            'id',
                            //'class_id',
                            'user_id',
                            'work_id',
                            'study_json_content',
                            'study_url',
                            'study_content',
                            'class_work_id',
                            //'type',
                            'created_at',
                        ])
                        ->where('class_type',$class_type);
                },
                    'work'=>function($q){
                        $q->select(['id','title','describe','type','content_json']);
                    },
                ])
            ->paginate(15);
    }



}
