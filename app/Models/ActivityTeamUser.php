<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Tests\Models\User;
use Illuminate\Support\Facades\DB;

class ActivityTeamUser extends Model
{
    //六项精进活动 与 小组 用户 绑定关系
    protected $table='activity_team_user';
    protected $fillable = [
        'id',
        'team_id',
        'key_id',
        'user_id',
        'team_user_stash',
        'team_type',
        'name',
        'phone',
        'company_name',
        'is_admin',
        'is_Invisible',
        'created_at',
        'start_time'
    ];

    protected $appends = ['is_holiday'];

    public function getIsHolidayAttribute()
    {
        if(empty($this->attributes['team_id'])){
            return 0;
        }
        $teamObj = ActivityTeamModel::where('id',$this->attributes['team_id'])->select([
            'wee_holiday',
            'day_holiday_start',
            'day_holiday_end',
        ])->first();
        $res = 1;
        if(!empty($teamObj->wee_holiday)){

            $w = date("w");

            $wee_holiday = $teamObj->wee_holiday;

            if(in_array($w,$wee_holiday) && $w!=0){
                $res = 2;
            }elseif ($w==0 && in_array('7',$wee_holiday)){
                $res = 2;
            }

            $sta = strtotime($teamObj->day_holiday_start);
            $end = strtotime($teamObj->day_holiday_end);
            if($sta >0 && $end>0){
                $now_time = time();
                if($now_time>=$sta && $now_time<=$end){
                    $res = 2;
                }
            }

        }

        return $res;
    }

    public function class(){
        return $this->belongsTo(ActivityClassModel::class,'class_id','id');
    }

    public function team(){
        return $this->belongsTo(ActivityTeamModel::class,'team_id','id');
    }

    public function sign_team(){
        return $this->hasMany(SignTeamModel::class,'user_id','user_id');
    }

    public function user(){
        return $this->belongsTo(UserModel::class,'user_id','id');
    }

    public function team_user_class_work(){
        return $this->hasMany(ClassWorkModel::class,'key_id','key_id');
    }

    public function add_team_user($data){
        return $this->create($data);
    }

    public function my_team_user($user_id,$team_type=[1,2,3,4],$stash){
        $team = [];
        $team_ids = $this->where('user_id',$user_id)
            ->whereIn('team_user_stash',$stash)
            ->whereIn('team_type',$team_type)
            ->pluck('team_id')
            ->toArray();
        if(count($team_ids)>0){
            $team = ActivityTeamModel::whereIn('id',$team_ids)
                ->select([
                    'id',
                    'team_name',
                    'team_logo',
                    'key_id',
                    'team_number',
                    'team_type',
                    'team_integral',
                    'created_at'
                ])
                ->orderBy('team_integral','desc')
                ->orderBy('created_at','desc')
                ->paginate(15);
        }

        return $team;
    }

    public function examine_team($user_id,$team_id,$team_user_stash){
        //开启事务
        DB::beginTransaction();
        try{
            //中间逻辑代码
            $this->where('user_id',$user_id)
            ->where('team_id',$team_id)
            ->update(['team_user_stash'=>$team_user_stash]);
            //審核通過人數+1
            if($team_user_stash == 1){
                ActivityTeamModel::where('id',$team_id)->increment('team_number', 1);
            }
            DB::commit();
            return true;
        }catch (\Exception $e) {
            //接收异常处理并回滚
            DB::rollBack();
            return false;
        }
    }

    public function out_team_user($user_id,$team_id){
        return $this->where('user_id',$user_id)
            ->where('team_id',$team_id)
            ->delete();
    }

}
