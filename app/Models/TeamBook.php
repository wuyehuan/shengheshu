<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TeamBook extends Model
{
    //小組綁定書本
    protected $table = 'team_book';
}
