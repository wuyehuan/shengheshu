<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AudioStudyArticle extends Model
{
    //
    protected $table='audio_study_article';
}
