<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookRecord extends Model
{
    //最近阅读
    protected $table='book_record';

    protected $fillable=[
        'book_id',
        'user_id',
        'article_id',
    ];

    public function book(){
        return $this->belongsTo(BookModel::class,'book_id','id');
    }

    public function create_book_record($data){
        return $this->create($data);
    }

    public function my_book_record($user_id){
        return $this->where('user_id',$user_id)
            ->with([
                'book'=>function($q){
                    $q->select([
                        'id',
                        'book_title',
                        'book_logo',
                        'author',
                    ]);
                },
            ])
            ->groupBy('book_id')
            ->orderBy('created_at','desc')
            ->paginate(4);
    }
}
