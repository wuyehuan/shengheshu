<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyReadWorkModel extends Model
{
    //企业读书 与 作业绑定
    protected $table='company_read_work';

}
