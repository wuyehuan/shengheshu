<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\BookModel;

/**
 * App\Models\SortModel
 *
 * @property int $id
 * @property string $sort_name 分类名称
 * @property int $pid 父ID
 * @property int $order 排序 越大排位越前
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SortModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SortModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SortModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SortModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SortModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SortModel whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SortModel wherePid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SortModel whereSortName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SortModel whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class SortModel extends Model
{
    //分类表
    protected $table='sort';

    public function book(){
        $this->hasMany(BookModel::class,'sort_id','id');
    }
}
