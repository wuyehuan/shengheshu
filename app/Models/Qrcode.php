<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Qrcode extends Model
{
    //二維碼
    protected $table='qrcode';

    public function getUrlAttribute($value)
    {
        return oss_domain().'/'.$value;

    }
}
