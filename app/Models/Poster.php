<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Poster extends Model
{
    //海报
    protected $table='poster';

    public function getCoveUrlAttribute($value){
        return oss_domain().'/'.$value;
    }
}
