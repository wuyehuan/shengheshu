<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\WorkModel
 *
 * @property int $id
 * @property string $title 标题
 * @property string|null $describe 任务描述
 * @property float|null $type 提交作业类型  1文字 2音频 3视频
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\StudyModel[] $study_work
 * @property-read int|null $study_work_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkModel whereDescribe($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkModel whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkModel whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkModel whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class WorkModel extends Model
{
    //作业
    protected $table='work';

    protected $casts = [
        'content_json' => 'json',
    ];

    //关联学生作业
    public function study_work(){
        return $this->hasMany(StudyModel::class,'work_id','id');
    }

}
