<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TeamBookActivity extends Model
{
    //小組對應活动書本
    protected $table = 'team_book_activity';

}
