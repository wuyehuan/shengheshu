<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActivityRegister extends Model
{
    //六項精進報名
    protected $table='activity_register';

    protected $fillable=[
        'user_id',
        'activity_id',
    ];

    public function create_activity_register($data){
        return $this->create($data);
    }
}
