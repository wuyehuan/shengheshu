<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SetSignModel extends Model
{
    //每日签到积分
    protected $table='set_sign';

    public function get_set_sign($company_id){
        return $this
            ->where('company_id',$company_id)
            ->first();
    }

}
