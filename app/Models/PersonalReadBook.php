<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PersonalReadBook extends Model
{
    //塾生学习绑定书本
    protected $table = 'personal_read_book';

    public function article(){
        return $this->hasMany(ArticleModel::class,'book_id','book_id');
    }

    public function work(){
        return $this->hasMany(WorkModel::class,'key_id','id');
    }
}
