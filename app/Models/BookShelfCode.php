<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookShelfCode extends Model
{
    //书架验证码 没有验证只可以读3本书
    protected $table = 'bookshelf_code';
}
