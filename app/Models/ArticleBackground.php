<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ArticleBackground extends Model
{
    //文章背景圖
    protected $table = 'article_background';
}
