<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AudioStudy extends Model
{
    //音频学习
    protected $table='audio_study';

    //关联书本
    public function book(){
        return $this->belongsTo(BookModel::class,'book_id','id');
    }

    public function audio_class_work(){
        return $this->hasMany(ClassWorkModel::class,'audio_study_id','id');
    }

    public function my_audio_team(){
        return $this->hasOne(ActivityTeamUser::class,'key_id','id');
    }

    public function many_audio_article(){
        return $this->hasMany(AudioStudyArticle::class,'audio_study_id','id');
    }

    public function getCoveAttribute($value){
        return oss_domain().'/'.$value;
    }

    //废弃了 逻辑改来改去 看不懂就重构吧
    public function audio_article(){
        return $this->belongsToMany(
            ArticleModel::class,
            'audio_study_article',
            'audio_study_id',
            'article_id');
    }

    public function new_audio_article(){
        $this->belongsToMany(ArticleModel::class, 'book',
            'book_id', 'book_id');
    }

    public function audio_list($user_id,$company_id){
        return $this
        ->paginate(15);
    }

    public function audio_info($audio_id,$user_id){
        return $this
            ->where('id',$audio_id)
            ->with([
            'new_audio_article'=>function($q){
                $q->select([
                    //'book.article_id',
                    'article.id',
                    'article.book_id',
                    'article.article_title',
                    //'article.article_cover',
                    'article.article_leader',
                    'article.reader_link',
                    'article.playtime',
                ]);
            },
        ])
            ->first();
    }

    public function audio_list_array($company_id){
        return $this
            ->where('company_id',$company_id)
            ->select([
                'id',
                'title as name',
            ])
            ->paginate(15);
    }
}
