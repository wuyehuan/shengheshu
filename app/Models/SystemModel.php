<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SystemModel
 *
 * @property int $id
 * @property float $type 类型1设置默认小组最大人数
 * @property string|null $key 名称
 * @property float|null $value 数值
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SystemModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SystemModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SystemModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SystemModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SystemModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SystemModel whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SystemModel whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SystemModel whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SystemModel whereValue($value)
 * @mixin \Eloquent
 */
class SystemModel extends Model
{
    //系统设置表
    protected $table='system';
}
