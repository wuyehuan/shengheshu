<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CollectionArticle extends Model
{
    //收藏文章
    protected $table='collection_article';

    protected $fillable=[
        'id',
        'user_id',
        'article_id',
        'created_at',
    ];

    public function activity(){
        return $this->belongsTo(ArticleModel::class,'article_id','id');
    }

    /**
     * 添加收藏
     */
    public function create_collection($data){
        return $this->create($data);
    }

    public function my_collection($user_id){
        return $this->where('user_id',$user_id)
            ->with([
                'activity'=>function($q){
                    $q->select([
                        'id',
                        //'article_leader',
                        'article_title',
                        //'article_content',
                        'article_cover',
                        'created_at',
                    ]);
                },
            ])
            ->orderBy('created_at','desc')
            ->paginate(15);
    }

    public function del_collection($id){
        return $this->where('id',$id)->delete();
    }
}
