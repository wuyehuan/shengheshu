<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\SystemModel;
use Illuminate\Support\Facades\Log;

/**
 * App\Models\CompanyModel
 *
 * @property int $id
 * @property int $user_id 申请的用户ID
 * @property string $company_name 公司名称
 * @property string $Invitation_code 企业邀请码
 * @property int $max_number 当前小组允许最大人数值
 * @property int|null $employees_number 员工人数
 * @property float|null $industry_type 行业类型 1制造业 2服务业 3其他
 * @property string|null $apply_sketch 申请描述
 * @property string $province 省
 * @property string $city 市
 * @property string $district 区/街道
 * @property string $company_address 详细地址
 * @property string $last_used_at 最后登录时间
 * @property string|null $Industry_description 行业说明
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyModel whereApplySketch($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyModel whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyModel whereCompanyAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyModel whereCompanyName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyModel whereDistrict($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyModel whereEmployeesNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyModel whereIndustryDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyModel whereIndustryType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyModel whereInvitationCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyModel whereLastUsedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyModel whereMaxNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyModel whereProvince($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyModel whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyModel whereUserId($value)
 * @mixin \Eloquent
 */
class CompanyModel extends Model
{
    //公司表
    protected $table='company';

    public function user(){
        return $this->belongsTo(UserModel::class,'user_id','id');
    }

    //申请验证公司
    public function create_company($data){
        $max_number = SystemModel::where('type',1)->value('value');
        DB::beginTransaction();
        try{
            $this->user_id = $data->user()->id;
            $this->company_name = $data->company_name;
            $this->employees_number = $data->employees_number;
            //设置的最大小组人数
            $this->max_number = !empty($max_number)?$max_number:100;
            $this->industry_type = $data->industry_type;
            $this->apply_sketch = $data->apply_sketch;
            $this->province = $data->province;
            $this->city = $data->city;
            $this->district = $data->district;
            $this->company_address = $data->company_address;
            $this->Industry_description = $data->Industry_description;
            $this->save();
            //更新用户绑定企业ID
            $data->user()->company_id = $this->id;
            $data->user()->identity_status = 2;
            $data->user()->identity = 2;
            $data->user()->save();
            DB::commit();
            return $this->id;
        }catch (\Exception $e) {
            DB::rollBack();
            Log::info('提交公司验证失败'.$e -> getMessage());
            return false;
        }

    }

    //公司详情
    public function company_info($company_id){
        return $this->where('id',$company_id)
            ->first();
    }

}
