<?php

namespace App\Exceptions;

use Exception;

class PolicyException extends Exception
{
    /**
     * 转换异常为 HTTP 响应
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {
       // dd($this);die;
        return response()->json([
            'status'  => false,
            'code'    => $this->code,
            'message' => config('errorcode.code')[(int) $this->code],
           //'data'    => '',
        ]);
        //return response($this->getMessage() ?: '发生异常啦');
    }

}
