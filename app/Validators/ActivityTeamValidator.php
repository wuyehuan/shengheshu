<?php

namespace App\Validators;

use Illuminate\Validation\Validator as Validator;

class CustomValidator extends Validator
{
    protected function validateCheckCreateTeam($attribute, $value, $parameters)
    {
        return ($this->validateImage($attribute, $value, $parameters)
            or $this->validateActiveUrl($attribute, $value, $parameters));
    }
}