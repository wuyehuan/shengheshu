<?php

namespace App\Services\Api;

use App\Services\Api\BaseService;

class BookService extends BaseService
{
    /**
     * @param $request \App\Http\Requests\UserApi\BookRequest
     * @param $bookModel \App\Models\BookModel
     * @return \Illuminate\Http\JsonResponse
     */
    public function book_info($request,$bookModel){
        //dd($request);
        $data = $bookModel->book_info($request->book_id,$request->user()->id);
        return $this->success($data);
    }

    /**
     * @param $book_id
     * @param $bookModel \App\Models\BookModel
     */
    public function book_base_info($book_id,$bookModel){
        $data = $bookModel->book_base_info($book_id);
        return $this->success($data);
    }

    /**
     * @param $request \App\Http\Requests\UserApi\BookRequest
     * @param $bookShelfModel \App\Models\BookShelf
     */
    public function my_bookshelf($request,$bookShelfModel){
        $data = $bookShelfModel->my_bookshelf($request->user()->id,$request->type,$request->key_work,$request->sort);
        return $this->success($data);
    }
}