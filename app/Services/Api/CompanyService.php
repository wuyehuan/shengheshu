<?php

namespace App\Services\Api;
use App\Services\Api\BaseService;

class CompanyService extends BaseService
{
    /**
     * @param $request
     * @param $companyModel \App\Models\CompanyModel
     * @return \Illuminate\Http\JsonResponse
     */
    public function create_company($request,$companyModel){
        if($request->user()->identity == 2 && $request->user()->identity_status == 3){
            return $this->fail(100001);
        }

        if($request->user()->identity == 3 && $request->user()->identity_status == 3){
            return $this->fail(100008);
        }

        $res = $companyModel->create_company($request);
        if($res){
            return $this->success();
        }
        return $this->fail(405);
    }

    /**
     * @param $company_id
     * @param $companyModel \App\Models\CompanyModel
     * @return \Illuminate\Http\JsonResponse
     */
    public function company_info($company_id,$companyModel){
        $data = $companyModel->company_info($company_id);
        return $this->success($data);
    }

    /**
     * @param $request
     * @param $companyModel \App\Models\CompanyModel
     * @param $userModel \App\Models\UserModel;
     * @return \Illuminate\Http\JsonResponse
     */
    public function bind_company($request,$companyModel,$userModel){
        if($request->user()->identity==2){
            return $this->fail(405);
        }
        $company  = $companyModel->where('Invitation_code',$request->Invitation_code)->first();
        $res = $userModel->bind_company($company,$request->user()->id);
        if($res){
            return $this->success('绑定成功,请等待审核');
        }
        return $this->fail(450);
    }

}