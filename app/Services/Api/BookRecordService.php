<?php

namespace App\Services\Api;
use App\Services\Api\BaseService;
class BookRecordService extends BaseService
{

    /**
     * @param $bookRequest \App\Http\Requests\UserApi\BookRecordRequest
     * @param $bookRecord \App\Models\BookRecord
     */
    public function my_book_record($bookRequest,$bookRecord){
        $data = $bookRecord->my_book_record($bookRequest->user()->id);
        return $this->success($data);
    }

}