<?php

namespace App\Services\Api;

class PersonalReadService extends BaseService
{
    /**
     * @param $personalReadModel \App\Models\PersonalRead
     */
    public function personal_read_list($personalReadModel,$personalReadRequest){
        $data = $personalReadModel->personal_read_list($personalReadRequest->user()->id);
        return $this->success($data);
    }

    /**
     * @param $personal_read_id \App\Http\Requests\UserApi\PersonalReadRequest
     * @param $personalReadModel \App\Models\PersonalRead
     */
    public function personal_read_info($personalReadRequest,$personalReadModel,$personalReadRegister){

        $data = $personalReadModel->personal_read_info($personalReadRequest->personal_read_id,$personalReadRequest->user()->id);
        return $this->success($data);
    }

    /**
     * @param $personalReadRequest
     * @param $personalReadRegister \App\Models\PersonalReadRegister
     */
    public function personal_read_register($personalReadRequest,$personalReadRegister){
       // dd($personalReadRequest->post());
        $data = $personalReadRequest->post();
        $data['user_id'] = $personalReadRequest->user()->id;
        $res = $personalReadRegister->create_register($data);
        if($res){
            return $this->success();
        }
    }

    public function phone($phone){
        $data = $phone->value('phone');
        return $this->success($data);
    }

}