<?php

namespace App\Services\Api;

class CollectionService extends BaseService
{
    /**
     * @param $request \App\Http\Requests\UserApi\CollectionRequest
     * @param $collectionArticle \App\Models\CollectionArticle
     */
    public function create_collection($request,$collectionArticle){
        $data = [];
        $data = $request->post();
        $data['user_id'] = $request->user()->id;
        $res = $collectionArticle->create_collection($data);
        return $this->success(200);
    }

    /**
     * @param $request \App\Http\Requests\UserApi\CollectionRequest
     * @param $collectionArticle \App\Models\CollectionArticle
     */
    public function my_collection($request,$collectionArticle){
        $data = $collectionArticle->my_collection($request->user()->id);
        return $this->success($data);
    }

    /**
     * @param $request \App\Http\Requests\UserApi\CollectionRequest
     * @param $collectionArticle \App\Models\CollectionArticle
     */
    public function del_collection($request,$collectionArticle){
        $res = $collectionArticle->del_collection($request->collection_id);
        if($res){
            return $this->success();
        }
        return $this->fail(405);
    }
}