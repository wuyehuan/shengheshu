<?php

namespace App\Services\Api;

class StudyCommentService extends BaseService
{

    /**
     * @param $studyCommentRequest \App\Http\Requests\UserApi\StudyCommentRequest
     * @param $studyComment \App\Models\StudyComment
     */
    public function create_study_comment($studyCommentRequest,$studyComment){
        $data = [];
        $data = $studyCommentRequest->post();
        $data['user_id'] = $studyCommentRequest->user()->id;
        $res = $studyComment->create_study_comment($data);
        if(!$res){
            return $this->fail(405);
        }
        return $this->success();
    }

    /**
     * @param $studyCommentRequest \App\Http\Requests\UserApi\StudyCommentRequest
     * @param $studyComment \App\Models\StudyComment
     */
    public function del_study_commnet($studyCommentRequest,$studyComment){
        $res = $studyComment->del_study_comment($studyCommentRequest->study_comment_id,$studyCommentRequest->user()->id);
        if($res){
            return $this->success();
        }
        return $this->fail(450);
    }

}