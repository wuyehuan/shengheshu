<?php

namespace App\Services\Api;

class SignService extends BaseService
{
    /**
     * @param $signRequest \App\Http\Requests\UserApi\SignRequest
     * @param $setSignModel \App\Models\SetSignModel
     */
    public function set_sign($signRequest,$setSignModel){
         $data = $setSignModel->get_set_sign($signRequest->company_id);
         return $this->success($data);
    }

    /**
     * @param $signRequest \App\Http\Requests\UserApi\SignRequest
     * @param $signModel \App\Models\SignModel
     */
    public function personal_sign($signRequest,$signModel){
        //dd($signRequest->user());die;
        $check_sign = $signModel
            ->where('user_id',$signRequest->user()->id)
            ->whereDate('created_at',date('Y-m-d'))
            ->count();
        if($check_sign>0){
            return $this->fail(200001);
        }
        $res = $signModel->personal_sign($signRequest);
        if(!$res){
            return $this->fail(405);
        }
        return $this->success();
    }

    /**
     * @param $signRequest \App\Http\Requests\UserApi\SignRequest
     * @param $signTeamModel \App\Models\SignTeamModel
     */
    public function team_personal_sign($signRequest,$signTeamModel){
        $check_sign = $signTeamModel
            ->where('user_id',$signRequest->user()->id)
            ->whereDate('created_at',date('Y-m-d'))
            ->where('team_id',$signRequest->team_id)
            ->count();
        if($check_sign>0){
            return $this->fail(200001);
        }
        $res = $signTeamModel->team_personal_sign($signRequest);
        if(!$res){
            return $this->fail(405);
        }
        return $this->success();
    }

    /**
     * @param $signRequest \App\Http\Requests\UserApi\SignRequest
     * @param $signTeamModel \App\Models\SignTeamModel
     */
    public function sign_team_order($signRequest,$signTeamModel){
        $data = $signTeamModel->sign_team_order($signRequest->user()->id);
        return $this->success($data);
    }

    /**
     * @param $signRequest \App\Http\Requests\UserApi\SignRequest
     * @param $signModel \App\Models\SignModel
     */
    public function sign_user_order($signRequest,$signModel){
        $data = $signModel->sign_user_order($signRequest->user()->company_id);
        return $this->success($data);
    }

    /**
     * @param $signRequest \App\Http\Requests\UserApi\SignRequest
     * @param $IntegralModel \App\Models\IntegralChange
     */
    public function integral_record($signRequest,$IntegralModel){
        $data = $IntegralModel->integral_record($signRequest->user()->company_id);
        return $this->success($data);
    }

}
