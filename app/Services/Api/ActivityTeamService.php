<?php

namespace App\Services\Api;
use App\Models\ActivityTeamModel;
use App\Models\SystemModel;
use App\Services\Api\BaseService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ActivityTeamService extends BaseService
{
    /**
     * @param $request \App\Http\Requests\UserApi\ActivityTeamRequest
     * @param $teamModel \App\Models\ActivityTeamModel
     * @return \Illuminate\Http\JsonResponse
     */
    public function create_activity_team($request,$teamModel){

        $data = [];
        $data = $request->post();
        $data['company_id'] = $request->user()->company_id;
        $data['user_id'] = $request->user()->id;
        $data['max_number'] = SystemModel::where('type',1)->value('value');
        $res = $teamModel->create_activity_team($data);
        if($res){
            return $this->success(200);
        }
        return $this->fail(405);
    }

    /**
     * @param $request \App\Http\Requests\UserApi\ActivityTeamRequest
     * @param $teamUser \App\Models\ActivityTeamUser
     */
    public function add_activity_team($request,$teamUser){
        //这里应该封装出去

        $team = \DB::table('activity_team')->where('id',$request->team_id)->first();

        if ($team->team_type == 2 || $team->team_type == 3){
            //is_Invisible
            if(!empty($request->is_Invisible) && $request->is_Invisible==1){

                $data = $teamUser->where('key_id',$team->key_id)
                    ->where('team_id',$team->id)
                    ->where('user_id',$request->user()->id)->where('is_Invisible',1)->first();
                //dd($data);
                if(!empty($data)){
                    return $this->fail(1000010);
                }
            }

        }elseif($team->team_type == 1){

            $data = $teamUser->where('team_id',$request->team_id)->where('user_id',$request->user()->id)->first();

            if(!empty($data)){
                return $this->fail(100005);
            }

        }

        if($team->team_type == 3 || $team->team_type ==2){
            if($team->Invitation_code != $request->Invitation_code){
                return $this->fail('1100003');
            }
        }

        $data = [];
        $data = $request->post();
        $data['user_id'] = $request->user()->id;
        $data['team_type'] = $team->team_type;
        $data['key_id'] = $team->key_id;
        $data['start_time'] = $team->start_time;
        $data['team_id'] = $team->id;

        //if($team->team_type == 1 || $team->team_type ==2){
            $data['team_user_stash'] = 1;
        //}

        //if(!empty($request->is_Invisible)){
            $data['is_Invisible'] = $request->is_Invisible;
            $teamUser->refresh();
            $team = $teamUser->where('user_id',$request->user()->id)
                ->where('team_id',$request->team_id)
                ->where('team_type',$team->team_type)
                //->where('is_Invisible',$request->is_Invisible)
                ->first();

            if(!empty($team)){
                return $this->fail(100005);
            }
        //}

        $res = $teamUser->add_team_user($data);
        if($res){
            return $this->success(200);
        }
        return $this->fail(405);
    }

    /**
     * @param $uer_id
     * @param $teamUser \App\Models\ActivityTeamUser
     */
    public function my_activity_team($uer_id,$team_type,$team_user_stash,$teamUser){

        if(empty($team_type)){
            $type = [1,2,3,4];
        }else{
            $type[] = $team_type;
        }

        if(empty($team_user_stash)){

            $stash = [0,1,2];
        }else{
            $stash[] = $team_user_stash;
        }

        $data = $teamUser->my_team_user($uer_id,$type,$stash);
        return $this->success($data);
    }

    /**
     * @param $class_id
     * @param $teamModel \App\Models\ActivityTeamModel
     */
    public function team_list($teamRequest,$teamModel){
        if(!empty($teamRequest->type)){
            $type[] = $teamRequest->type;
        }else{
            $type = [1,2,3,4];
        }

        $key_work = '';
        if(!empty($teamRequest->key_work)){
            $key_work = $teamRequest->key_work;
        }

        $data = $teamModel->team_list($teamRequest->class_id,$type,$teamRequest->user()->company_id,$key_work);
        return $this->success($data);
    }

    /**
     * @param $class_id
     * @param $teamModel \App\Models\ActivityTeamModel
     */
    public function other_team_list($teamRequest,$teamModel){
        if(!empty($teamRequest->type)){
            $type[] = $teamRequest->type;
        }else{
            $type = [1,2,3,4];
        }
        //dd($type);
        $data = $teamModel->other_team_list($teamRequest->class_id,$type,$teamRequest->user()->id);
        return $this->success($data);
    }

    /**
     * @param $class_id
     * @param $teamModel \App\Models\ActivityTeamModel
     */
    public function team_info($teamRequest,$teamModel){
        if(!empty($teamRequest->type)){
            $type[] = $teamRequest->type;
        }else{
            $type = [1,2,3,4];
        }
        $data = $teamModel->team_info($teamRequest->team_id,$type,$teamRequest->user()->id);
        return $this->success($data);
    }

    /**
     * @param $teamRequest \App\Http\Requests\UserApi\ActivityTeamRequest
     * @param $teamUser \App\Models\ActivityTeamUser
     */
    public function examine_team($teamRequest,$teamUser){
        $res = $teamUser->examine_team($teamRequest->user_id,$teamRequest->team_id,$teamRequest->team_user_stash);
        if(!$res){
            return $this->fail(405);
        }
        return $this->success(200);
    }

    /**
     * @param $teamRequest \App\Http\Requests\UserApi\ActivityTeamRequest
     * @param $teamModel \App\Models\ActivityTeamModel
     * @return \Illuminate\Http\JsonResponse
     */
    public function my_create_team($teamRequest,$teamModel){
        $data = $teamModel->my_create_team($teamRequest->user()->id,$teamRequest->team_type);
        return $this->success($data);
    }

    /**
     * @param $teamRequest \App\Http\Requests\UserApi\ActivityTeamRequest
     * @param $teamUser \App\Models\ActivityTeamUser
     */
    public function out_team_user($teamRequest,$teamUser){
        $team = \DB::table('activity_team')->where('id',$teamRequest->team_id)
            ->where('user_id',$teamRequest->user()->id)
            ->first();
        if(!empty($team)){
            return $this->fail(100009);
        }
        $res = $teamUser->out_team_user($teamRequest->user()->id,$teamRequest->team_id);
        if($res){
            return $this->success('退出成功');
        }
        return $this->fail(407);
    }

    /**
     * @param $teamRequest \App\Http\Requests\UserApi\ActivityTeamRequest
     * @param $teamModel \App\Models\ActivityTeamModel
     */
    public function integral_rank($teamRequest,$teamModel){
        if(empty($teamRequest->type)){
            $type = [1,2,3,4];
        }else{
            $type[] =  $teamRequest->type;
        }
        $data = $teamModel->integral_rank($teamRequest->user()->id,$type);
        return $this->success($data);
    }

    /**
     * @param $teamRequest \App\Http\Requests\UserApi\ActivityTeamRequest
     * @param $teamModel \App\Models\ActivityTeamModel
     */
    public function team_integral_rank($teamRequest,$teamModel){
        if(empty($teamRequest->type)){
            $type = [1,2,3,4];
        }else{
            $type[] =  $teamRequest->type;
        }
        $data = $teamModel->team_integral_rank($teamRequest->user_id,$type);
        return $this->success($data);
    }

    /**
     * @param $teamRequest \App\Http\Requests\UserApi\ActivityTeamRequest
     * @param $teamUser \App\Models\ActivityTeamUser
     */
    public function edit_my_team_is_invisible($teamRequest,$teamUser){
        DB::beginTransaction();
        try{
            //中间逻辑代码
            $teamRequest->type = !empty($teamRequest->type)?$teamRequest->type:2;
            $team = $teamUser->where('user_id',$teamRequest->user()->id)
                ->where('team_type',$teamRequest->type)
                ->where('key_id',$teamRequest->key_id)
                ->where('is_Invisible',1)
                ->first();

            $team->is_Invisible = 2;
            $team->save();

            $teamUser->refresh();
            $res = $teamUser
                ->where('team_id',$teamRequest->team_id)
                ->where('user_id',$teamRequest->user()->id)
                ->update([
                    'is_Invisible'=>1,
                ]);
            DB::commit();
            if($res){
                return $this->success('更新成功');
            }
        }catch (\Exception $e) {
            //接收异常处理并回滚
            DB::rollBack();
            return $this->fail(407);
        }

    }

    /**
     * @param $teamRequest \App\Http\Requests\UserApi\ActivityTeamRequest
     * @param $teamModel \App\Models\ActivityTeamModel
     */
    public function all_team_list($teamRequest,$teamModel){

        if(!empty($teamRequest->type)){
            $type[] =  $teamRequest->type;
        }else{
            $type = [1,2,3];
        }
        $user_id = $teamRequest->user()->id;
        $q = $teamModel->whereIn('team_type',$type)->with([
                'team_user'=>function($q)use($user_id){
                    $q->select([
                        'user_id',
                        'team_id',
                        'company_name',
                        'team_user_stash',
                        'phone',
                        'created_at',
                    ])
                    ->where('user_id',$user_id)
                    ->whereIn('team_user_stash',[0,1]);
                }
            ]);

            if(!empty($teamRequest->key_word)){
                $q->where('team_name','like','%'.$teamRequest->key_word.'%');
            }
        $data = $q ->paginate(15);

        $data = $data->toArray();
        $new_data = [];

            $new_data['my_team'] = [];
            $new_data['team_list'] = [];
            foreach ($data['data'] as $k=>$v){
                if(count($v['team_user'])>0){
                    $new_data['my_team'][] = $v;
                }else{
                    $new_data['team_list'][] = $v;
                }

            }

        $data['data'] = $new_data;
        return $this->success($data);
    }

}