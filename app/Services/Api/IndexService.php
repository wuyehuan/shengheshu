<?php

namespace App\Services\Api;
use App\Services\Api\BaseService;

class IndexService extends BaseService
{

    /**
     * @param $bannerModel \App\Models\BannerModel
     * @return \Illuminate\Http\JsonResponse
     */
    public function banner($bannerModel){
        $data =  $bannerModel->orderBy('id','desc')->limit('3')->get();
        return $this->success($data);
    }

    /**
     * @param $poster \App\Models\Poster
     */
    public function poster($poster){
        $data =  $poster->value('cove_url');
        return $this->success($data);
    }

    /**
     * @param $activityCove \App\Models\ActivityCove
     */
    public function activity_coves($type,$activityCove){
        $class_type = [];
        if(!empty($type)){
            $class_type[] = $type;
        }else{
            $class_type = [1,2,3,4];
        }
        //dd($type);
        $data =  $activityCove->where('class_type',$class_type)->value('cove_img');
        return $this->success($data);
    }

    public function bookshelf_guide($bookshelfGuide){
        $data =  $bookshelfGuide->first();
        return $this->success($data);
    }

    public function system_images($images,$type=1){
        $data['image_url'] = $images->where('type',$type)->value('img_url');
        return $this->success($data);
    }

}