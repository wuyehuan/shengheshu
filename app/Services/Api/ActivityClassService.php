<?php

namespace App\Services\Api;

use App\Models\ActivityTeamUser;
use App\Models\PersonalRead;
use App\Models\PersonalReadRegister;

class ActivityClassService extends BaseService
{

    /**
     * @param $class_id
     * @param $classModel \App\Models\ActivityClassModel
     * @return \Illuminate\Http\JsonResponse
     */
    public function class_all_info($class_id,$classModel,$user_id){
        $data = $classModel->class_info($class_id,$user_id);
        return $this->success($data);
    }

    /**
     * @param $class_id
     * @param $classModel \App\Models\ActivityClassModel
     * @return \Illuminate\Http\JsonResponse
     */
    public function calss_list($classRequest,$classModel){
        $data = $classModel->class_list($classRequest->user()->id,$classRequest->user()->company_id);
        return $this->success($data);
    }

    /**
     * @param $classRequest
     * @param $activityClassModel \App\Models\ActivityClassModel
     * @param $companyReadModel \App\Models\CompanyReadModel
     * @param $personalRead \App\Models\PersonalRead
     * @param $audioStudy \App\Models\AudioStudy
     * @return \Illuminate\Http\JsonResponse
     */
    public function activity_list($classRequest,$activityClassModel,$companyReadModel,$personalRead,$audioStudy){
        if($classRequest->type == 2){
            //var_dump($classRequest->type);die;
            $data = $activityClassModel->class_list_array();
        }elseif ($classRequest->type == 1){
            $data = $companyReadModel->company_read_array($classRequest->user()->company_id);
        }elseif ($classRequest->type == 3){
            $data = $personalRead->personal_read_array($classRequest);
        }elseif ($classRequest->type == 4){
            $data = $audioStudy->audio_list_array($classRequest->user()->company_id);
        }
        return $this->success($data);
    }

    /**
     * @param $classRequest \App\Http\Requests\UserApi\ActivityClassRequest
     * @param $activityRegister \App\Models\ActivityRegister
     * @param $activityClassModel \App\Models\ActivityClassModel
     * @param $activityTeamModel \App\Models\ActivityTeamModel
     */
    public function add_activity_register($classRequest,$activityRegister,$activityClassModel,$activityTeamModel){
        $data = $classRequest->post();
        $data['user_id'] = $classRequest->user()->id;
        //$data['activity_id'] = $classRequest->class_id;

        $activity = $activityTeamModel->where('Invitation_code',$classRequest['Invitation_code'])
            ->first();
        if(empty($activity)){
            return $this->fail(405);
        }
        $res = $activityRegister->create_activity_register($data);
        if($res){
            return $this->success(200);
        }
    }

    /**
     * @param $classRequest
     * @param $activityClassModel
     * @param $audioStudy
     * @param $personalRead
     * @param $companyReadModel
     * 獲取我的全部活动
     * @param team_type 1企業讀書 2六項精進 3塾生讀書 4音頻學習
     */
    public function my_activity_list($classRequest,$activityClassModel,$audioStudy,$personalRead,$companyReadModel){
        //team_type 1 2 4 是需要參加小組的 3企業讀書 只需報名
        //  查出4個分類再合併 沒用UNION
        $teamModel = new ActivityTeamUser();
        $key_ids = $teamModel->where('user_id',$classRequest->user()->id)->select(['key_id','team_type'])
            ->get()
            ->groupBy('team_type')
            ->toArray();

        $activityClassData = [];
        $audioStudyData = [];
        $personDate = [];
        $company_data = [];

        //if(!empty($key_ids)){
            //獲取我參加的六項精進活动
            if(!empty($key_ids[2])){
                $activityClass_ids = array_column($key_ids[2], 'key_id');
                $activityClassData = $activityClassModel
                    ->whereIn('id',$activityClass_ids)
                    ->select([
                        'id',
                        'class_name as title',
                        //'author',
                        'class_cove as cove',
                        'created_at',
                        \DB::raw("2 as activity_type"),
                    ])
                    ->paginate(5)
                    ->toArray();
            }else{
                $activityClassData['data'] = [];
            }

            if(!empty($key_ids[4])){

            }else{
                $audioStudyData['data'] = [];
            }

            if(!empty($key_ids[1])){
                //獲取我參加的企業附屬活动
                $company_read_ids = array_column($key_ids[1], 'key_id');
                $company_data = $companyReadModel->whereIn('id',$company_read_ids)
                    ->select([
                        'id',
                        'cove_url as cove',
                        'title',
                        \DB::raw("1 as activity_type"),
                        'created_at',
                    ])
                    ->paginate(5)
                    ->toArray();

            }else{
                $company_data['data'] = [];
            }

        //獲取我參加的塾生學習活动
        $personal_read_register = new PersonalReadRegister();
        $personal_ids = $personal_read_register
            ->where('user_id',$classRequest->user()->id)
            ->where('stash',2)
            ->pluck('personal_read_id')
            ->toArray();
        $personDate = $personalRead->whereIn('id',$personal_ids)
            ->select([
                'id',
                'personal_title as title',
                'cove_url as cove',
                \DB::raw("3 as activity_type"),
                'created_at',
            ])
            ->with([
                'book'
            ])
            ->paginate(5)
            ->toArray();

        $data['data'] = array_merge($activityClassData['data'],$audioStudyData['data'],$personDate['data'],$company_data['data']);

        $data['total'] = 0;
        if(!empty($key_ids[1])){
            $data['total'] +=  count($key_ids[1]);
        }
        if(!empty($key_ids[2])){
            $data['total'] +=  count($key_ids[2]);
        }
        if(!empty($key_ids[4])){
            $data['total'] +=  count($key_ids[4]);
        }
        if(!empty($personal_ids)){
            $data['total'] +=  count($personal_ids);
        }
        //count($personal_ids);
        return $this->success($data);
    }
}