<?php

namespace App\Services\Api;
use App\Models\ActivityTeamModel;
use App\Models\ActivityTeamUser;
use App\Models\ArticleModel;
use App\Models\PersonalReadBook;
use App\Models\TeamBook;
use Illuminate\Support\Facades\Log;
use App\Models\SignTeamModel;
use App\Models\SignModel;
use App\Models\Holidays;
use App\Models\CompanyReadBook;
use App\Models\ActivityClassBook;
use App\Admin\Controllers\AdminApiController;
use Illuminate\Support\Facades\Cache;


class WorkService extends BaseService
{
    /**
     * @param $request \App\Http\Requests\UserApi\WorkRequest
     * @param $classWorkModel \App\Models\ClassWorkModel
     */
    public function article_calss_work($request,$classWorkModel,$class_type){
        $data = $classWorkModel->article_calss_work($request->book_id,$request->article_id,$request->key_id,$request->user()->id,$class_type,$request->user()->company_id);
        return $this->success($data);
    }

    /**
     * @param $request
     * @param $studyModel \App\Models\StudyModel
     * @return \Illuminate\Http\JsonResponse
     */
    public function add_work($request,$studyModel){

        //后期要封装 $request建議 改為参数
        $class_work = \DB::table('class_work')
            ->where('id',$request->class_work_id)
            ->first();
        //dd($class_work);
        //检查该活动是否加入小组 並且有設置默認打卡小組
        $team = \DB::table('activity_team_user')
            ->where('key_id',$class_work->key_id)
            ->where('team_type',$class_work->class_type)
            ->where('user_id',$request->user()->id)
            //->where('type',$request->user()->id)
            ->where('team_user_stash',1)
            //->where('is_Invisible',1)
            ->first();
       // dd($class_work->key_id,$class_work->class_type);

        if(empty($team)){
            return $this->fail(100006);
        }

        $teamObj = ActivityTeamModel::where('id',$team->team_id)->first();
        //获取缓存
        $redis_key = env('REDIS_KEY_TEAM_WORK').$team->team_id;
        $work_data = Cache::get($redis_key);
        if(!empty($work_data)){
            $work_data = json_decode($work_data,true);
        }else{
            $api = new AdminApiController();
            $res = $api->redis_team_book_article($class_work->key_id,$class_work->class_type,$teamObj->id);
            $work_data = Cache::get($redis_key);
            $work_data = json_decode($work_data,true);
        }

        //檢查是否在休息日 休息日不能提交
        $today = date("Y-m-d");
        if(empty($work_data[$today])){
            return $this->fail(300002);
        }

        $check_study = \DB::table('study')
            ->where('key_id',$class_work->key_id)
            ->where('user_id',$request->user()->id)
            ->where('class_work_id',$request->class_work_id)
            ->where('class_type',$class_work->class_type)
            ->where('type',$class_work->type)
            ->where('article_id',$request->article_id)
            ->where('team_id',$teamObj->id)
            ->first();
        if(!empty($check_study)){
            return $this->fail(300001);
        }

        $data = [];
        $data  = $request->post();
        $data['key_id'] = $class_work->key_id;
        $data['user_id'] = $request->user()->id;
        $data['team_id'] = $teamObj->id;
        $data['article_id'] = $request->article_id;
        $data['work_id'] = $class_work->work_id;
        $data['class_type'] = $class_work->class_type;
        $data['team_id'] = $team->team_id;

        if($class_work->type == 1){
            $data['study_json_content'] = $request->study_json_content;
           //Log::info($request->study_json_content);
        }elseif ($class_work->type == 2){
            $data['study_url'] = $request->study_url;
            $data['playtime'] = $request->playtime;
        }elseif ($class_work->type == 3){
            $data['study_content'] = $request->study_content;
        }

        $data['type'] = $class_work->type;

        $res = $studyModel->add_work($data);

       if($res){
           //打卡小组&&不是监督 获得积分
           if($team->is_Invisible == 1 && $team->is_supervisor==2){
               $is_calculation = true;
           }else{
               $is_calculation = false;
           }

           $integral_day = 0;
           //查询是否获得积分
           if($class_work->is_points == 1 && $is_calculation){
               $integral_day = $class_work->points_value;
           }

           if(!empty($team)){
               $signTeamModel = new SignTeamModel();
               $signTeamModel->team_personal_sign($request->user()->id,$team,$class_work,$is_calculation);
           }

           if($class_work->class_type == 3){
               //塾生讀書 有小組積分（如果加入） 和個人積分
                $SignModel = new SignModel();
                $SignModel->personal_sign($request,$class_work);
           }
           //
           return $this->success(['study_id'=>$res->id,'integral_day'=>$integral_day]);
       }
        return $this->fail(500);
    }

    /**
     * @param $request
     * @param $studyModel \App\Models\StudyModel
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit_work($request,$studyModel){
        //后期封装出去
        $study_type = \DB::table('study')
            ->where('id',$request->study_id)
            ->value('type');
        //dd($class_work);die;
        $data = [];

        if($study_type == 1){
            $data['study_json_content'] = $request->study_json_content;
        }elseif ($study_type == 2){
            $data['study_url'] = $request->study_url;
            $data['playtime'] = $request->playtime;
        }elseif ($study_type == 3){
            $data['study_content'] = $request->study_content;
        }

        if($request->is_draft){
            $data['is_draft'] = $request->is_draft;
        }

        if($request->is_opend){
            $data['is_opend'] = $request->is_opend;
        }

        $res = $studyModel->edit_work($request->study_id,$data);
       // dd($res);die;
        if($res){
            return $this->success(200);
        }
        return $this->fail(500);
    }

    /**
     * @param $request   \App\Http\Requests\UserApi\WorkRequest
     * @param $studyModel \App\Models\StudyModel
     * 我的六项精进作业
     */
    public function my_work($request,$studyModel){
        if(empty($request->type)){
            $type = [1,2,3,4];
        }else{
            $type[] = $request->type;
        }
        $data = $studyModel->my_work($request->user()->id,$type);
        return $this->success($data);
    }

    /**
     * @param $request   \App\Http\Requests\UserApi\WorkRequest
     * @param $studyModel \App\Models\StudyModel
     * 成员作业
     */
    public function team_user_work($request,$studyModel){

        $type = \DB::table('activity_team')->where('id',$request->team_id)->value('team_type');

        $data = $studyModel->team_user_work($request->team_id,$type,$request->start_time,$request->end_time);
        return $this->success($data);
    }

    /**
     * @param $request \App\Http\Requests\UserApi\WorkRequest
     * @param $studyModel \App\Models\StudyModel
     */
    public function my_finish_work($request,$studyModel){
        $data = $studyModel->my_finish_work($request->user()->id,$request->class_type,$request->work_type);
        return $this->success($data);
    }

    /**
     * @param $request \App\Http\Requests\UserApi\WorkRequest
     * @param $studyModel \App\Models\StudyModel
     */
    public function my_study_info($request,$studyModel){
        $data = $studyModel->my_study_info($request->study_id,$request->user()->id,$request->is_draft);
        return $this->success($data);
    }

    /**
     * @param $request \App\Http\Requests\UserApi\WorkRequest
     * @param $studyModel \App\Models\StudyModel
     */
    public function study_list($request,$studyModel){
        if(empty($request->type)){
            $type=[1,2,3,4];
        }else{
            $type[] = $request->type;
        }
        //dd($type);
        $data = $studyModel->study_list($type,$request->user()->id);
        return $this->success($data);
    }

    /**
     * @param $request \App\Http\Requests\UserApi\WorkRequest
     * @param $studyModel \App\Models\StudyModel
     */
    public function study_del($request,$studyModel){
        $res = $studyModel->study_del($request->study_id,$request->user()->id);
        if(!$res){
            return $this->fail(405);
        }
        return $this->success();
    }

    /**
     * @param $request \App\Http\Requests\UserApi\WorkRequest
     * @param $studyModel \App\Models\StudyModel
     * @return \Illuminate\Http\JsonResponse
     */
    public function study_sort($request,$studyModel){
        $data = $studyModel->study_sort($request->user()->id,$request->type,$request->start_time,$request->end_time,$request->user()->company_id);
        return $this->success($data);
    }

    /**
     * @param $request \App\Http\Requests\UserApi\WorkRequest
     * @param $studyModel \App\Models\StudyModel
     * @return \Illuminate\Http\JsonResponse
     */
    public function team_study_sort($request,$studyModel){
        $data = $studyModel->team_study_sort($request->team_id,$request->start_time,$request->end_time);
        return $this->success($data);
    }

    /**
     * @param $request \App\Http\Requests\UserApi\WorkRequest
     * @param $studyModel \App\Models\StudyModel
     * @return \Illuminate\Http\JsonResponse
     */
    public function company_work_record($request,$studyModel){
        $data = $studyModel->company_work_record($request->user()->company_id,$request->start_time,$request->end_time);
        return $this->success($data);
    }

    /**
     * @param $request \App\Http\Requests\UserApi\WorkRequest
     * @param $studyModel \App\Models\StudyModel
     * @return \Illuminate\Http\JsonResponse
     */
    public function company_read_work_record($request,$studyModel){
        $data = $studyModel->company_read_work_record($request->user()->company_id,$request->start_time,$request->end_time);
        return $this->success($data);
    }

    /**
     * @param $request \App\Http\Requests\UserApi\WorkRequest
     * @param $studyModel \App\Models\StudyModel
     * @return \Illuminate\Http\JsonResponse
     */
    public function personal_read_work_record($request,$studyModel){
        $data = $studyModel->personal_read_work_record($request->user()->company_id,$request->start_time,$request->end_time);
        return $this->success($data);
    }

    /**
     * @param $request \App\Http\Requests\UserApi\WorkRequest
     * @param $studyModel \App\Models\StudyModel
     * @return \Illuminate\Http\JsonResponse
     */
    public function my_personal_work($request,$studyModel){
        $data = $studyModel->my_personal_work($request->user()->user_id);
        return $this->success($data);
    }


    /**
     *  獲取小組開始读书时间 到 今天 要读章节的数量 （减去休息日）
     * @param $start_time //开始时间
     * @param $weeks||array //周几休息
     * @param $day_holiday_start //固定休息开始时间
     * @param $day_holiday_end //固定休息结束时间
     * @return float|int
     */
    public function get_total_work_date($start_time,$weeks,$day_holiday_start,$day_holiday_end){
        //dd($start_time,$weeks,$day_holiday_start,$day_holiday_end);
        //计算周几休息天数
        $today = date("Y-m-d 23:59:59");
        //$weeks = $teamObj->team->wee_holiday;
        if(empty($start_time)){
            return 0;
        }
        $total_days = round(strtotime($today) - strtotime(date('Y-m-d 00:00:00',strtotime($start_time))));

        $days_number = 0;
        $days_number2 = 0;
        //$days = 0;
        $days = ceil($total_days / 86400);

            if(count($weeks) > 0){
                //dd($today,$teamObj->team->start_time);

                //dd($today,$weeks,$days);
                $holiday = getWeekDays($start_time,$weeks,$days,$today);
                $days_number = count($holiday);
            }



        //$holiday2=[];
        //计算固定休息时间天数
        if(!empty($day_holiday_end) && !empty($day_holiday_start) && $day_holiday_start < $today){
            $days2 = strtotime($day_holiday_end) - strtotime($day_holiday_start);
            $days_number2 = intval($days2 / 86400);
            //$holiday2 = getWeekDays($day_holiday_start,[1,2,3,4,5,6,0],$days_number2,$today);
        }

        //减去总休息天数
        $total_days = $days - ($days_number + $days_number2);
        return $total_days;
    }
}