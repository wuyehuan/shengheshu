<?php

namespace App\Services\Api;

class PersonalService extends BaseService
{
    /**
     * @param $user_id
     * @param $userModel  \App\Models\UserModel
     */
    public function personal_info($user_id,$userModel){
        $data = $userModel->get_user($user_id);
        return $this->success($data);
    }

    /**
     * @param $user_id
     * @param $userModel  \App\Models\UserModel
     */
    public function others_index($user_id,$userModel,$my_user_id){
        $data = $userModel->others_index($user_id,$my_user_id);
        return $this->success($data);
    }

    /**
     * @param $user_id
     * @param $userModel  \App\Models\UserModel
     */
    public function edit_user_info($request,$userModel){
        $data = $request->post();
        unset($data['token']);
        $res = $userModel->edit_user_info($data,$request->user()->id);
        if(!$res){
            return $this->fail(405);
        }
        return $this->success();
    }

    public function qrcode($qrcode){
        //dd(date("Y-m-d H:i:s"));
        $code = $qrcode
            ->whereDate('start_time','<=',date("Y-m-d H:i:s"))
            ->whereDate('end_time','>=',date("Y-m-d H:i:s"))
            ->value('url');
        return $this->success($code);
    }

}