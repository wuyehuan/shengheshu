<?php

namespace App\Services\Api;

class CompanyReadService extends BaseService
{
    /**
     * @param $companyReadRequest \App\Http\Requests\UserApi\CompanyReadRequest
     * @param $companyReadModel \App\Models\CompanyReadModel
     */
    public function company_read_list($companyReadRequest,$companyReadModel){
        $data = $companyReadModel->company_read_list($companyReadRequest->user()->company_id,$companyReadRequest->user()->id);
        return $this->success($data);
    }

    /**
     * @param $request \App\Http\Requests\UserApi\CompanyReadRequest
     * @param $companyReadModel \App\Models\CompanyReadModel
     */
    public function company_read_info($request,$companyReadModel){
        $data = $companyReadModel->company_read_info($request->user()->id,$request->company_read_id);
        return $this->success($data);
    }

}