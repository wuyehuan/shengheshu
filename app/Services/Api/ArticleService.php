<?php

namespace App\Services\Api;

class ArticleService extends BaseService
{

    /**
     * @param $request
     * @param $articleModel \App\Models\ArticleModel
     * @return \Illuminate\Http\JsonResponse
     */
    public function article_info($request,$articleModel){
        $data = $articleModel->article_info($request->article_id,$request->user()->id);
        return $this->success($data);
    }

}