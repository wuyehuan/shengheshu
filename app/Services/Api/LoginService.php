<?php

namespace App\Services\Api;
use App\Models\UserModel;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\EasyWeChat;
use Illuminate\Support\Facades\Log;
use App\Services\Oss;

class LoginService extends BaseService
{

    function __construct(UserModel $userModel) {
        $this->userModel = $userModel;
    }

    //账号密码登录
    public function account_login($request){
        $account_user = $this->userModel->account_user($request['account']);
        // 验证密码是否正确
        if(!Hash::check($request['password'],$account_user->password)){
            return $this->fail(400);
        }
        //判断是否禁用
        if($account_user->status!=1){
            return $this->fail(403);
        }

        $token = JWTAuth::fromUser($account_user);
        $account_user->token = $token;

        //获取阅读天数
        $account_user->continuity_date = \DB::table('sign_team')
            ->where('user_id',$account_user->id)
            ->value('num');

        return $this->success($account_user);
    }

    /**
     * @param $request
     * @param $userModel \App\Models\UserModel
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function wx_login($request,$userModel){
        try{
            $app = EasyWeChat::app();
            $session = $app->auth->session($request['wx_code']);

            // 说明没有获取到openid
            if(empty($session['openid'])){
                throw new \Exception($session['errmsg']);
            }

            // 查找會員是否存在
            $userObj = $userModel
                -> where('wx_openid','=',$session['openid'])
                -> first();


            if(empty($userObj)){
                $decryptedData = $app->encryptor->decryptData($session['session_key'],$request['wx_iv'],$request['encryptedData']);
                if(!key_exists('wx_iv',$request) && !key_exists('encryptedData',$request)){
                    throw new \Exception('wx_iv和encryptedData必传');
                }
                if(empty($decryptedData['avatarUrl'])){
                    throw new \Exception('解密失败 请检查参数');
                }
                Log::info(json_encode($decryptedData));
                $userModel->refresh;
                //头像上传oss
                //$avatar = getWxHead($decryptedData['avatarUrl']);
                $oss = new Oss();
                $img = getcurl($decryptedData['avatarUrl']);
                $pathName = pathName();
                $oss->uploadContent($pathName, $img);
                //$avatar = OSS::getPublicObjectURL(config('alioss.aliyun_oss_bucket'), $pathName);
                $userModel->avatar = $pathName;

                $userModel->account = $decryptedData['nickName'];
                $userModel->password = 123456;
                $userModel->nickname = $decryptedData['nickName'];
                $userModel->wx_openid = $decryptedData['openId'];
                $userModel->identity = 1;
                $userModel->identity_status = 3;
                $userModel->company_id = 0;
                $userModel->save();
                //dd($userModel);die;
                $userObj = $userModel;
            }

            // 自動登錄
            $token = JWTAuth::fromUser($userObj);

            //获取阅读天数
            $userObj->continuity_date = \DB::table('sign_team')
                ->where('user_id',$userObj->id)
                ->value('num');

            $userObj->token = $token;
            return $this->success($userObj);

        }catch (\Exception $e){
            return $this->fail(406,$e -> getMessage());
        }

    }


}

