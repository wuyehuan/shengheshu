<?php

namespace App\Services;

use App\Http\Controllers\EasyWeChat;
use App\Models\BookModel;
use App\Services\Api\BaseService;
use App\Models\ArticleModel;
use App\Models\ActivityTeamUser;

class AudioStudyService extends BaseService
{
    /**
     * @param $request \App\Http\Requests\UserApi\AudioStudyRequest
     * @param $audioStudy \App\Models\AudioStudy
     */
    public function audio_study_list($request,$audioStudy){
        $data = $audioStudy->audio_list($request->user()->id,$request->user()->company_id);
        return $this->success($data);
    }

    /**
     * @param $request \App\Http\Requests\UserApi\AudioStudyRequest
     * @param $audioStudy \App\Models\AudioStudy
     */
    public function audio_info($request,$audioStudy){
        $data = $audioStudy->audio_info($request->audio_id,$request->user()->id);
        return $this->success($data);
    }
}