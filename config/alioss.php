<?php
/**
 * Created by PhpStorm.
 * User: zhan
 * Date: 2020/7/5  16:18
 */
return [
    'aliyun_oss_endpoint' => env('ALIYUN_OSS_ENDPOINT', null), //
    'aliyun_access_id' => env('ALIYUN_ACCESS_ID', null),// key
    'aliyun_access_key' => env('ALIYUN_ACCESS_KEY', null),// secret
    'aliyun_oss_bucket' => env('ALIYUN_OSS_BUCKET', null), // bucket
    'aliyun_oss_visit' => env('ALIYUN_OSS_VISIT', null), // bucket
];