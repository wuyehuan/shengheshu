<?php
/**
 * Created by PhpStorm.
 * User: zhan
 * Date: 2020/7/13  14:48
 */
return [
    'wx_appid'=>env('WX_APPID',''),
    'wx_appsecret'=>env('WX_APPSECRET',''),
];