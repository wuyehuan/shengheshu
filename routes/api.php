<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//会员端
Route::group(['namespace'=>'UserApi','prefix' => 'User'],function (){

    //会员登录
    Route::post('login','LoginController@login');
    Route::post('wx_login','LoginController@wx_login');

    //小组模块 六项精进
    //我的小队
    Route::post('create_activity_team','ActivityTeamController@create_activity_team');
    //加入小组
    Route::post('add_activity_team','ActivityTeamController@add_activity_team');
    //我加入\报名的活动
    Route::post('my_activity_team','ActivityTeamController@my_activity_team');
    //小组列表
    Route::post('team_list','ActivityTeamController@team_list');
    //小组审核
    Route::post('examine_team','ActivityTeamController@examine_team');
    //我創建的小組
    Route::post('my_create_team','ActivityTeamController@my_create_team');
    //退出小组
    Route::post('out_team_user','ActivityTeamController@out_team_user');
    //
    Route::post('integral_rank','ActivityTeamController@integral_rank');
    //小组详情
    Route::post('team_info','ActivityTeamController@team_info');
    Route::post('team_integral_rank','ActivityTeamController@team_integral_rank');
    //其他人的小组
    Route::post('other_team_list','ActivityTeamController@other_team_list');
    //修改我的打卡小组
    Route::post('edit_my_team_is_invisible','ActivityTeamController@edit_my_team_is_Invisible');
    //所有小組 區分我加入的 跟其他
    Route::post('all_team_list','ActivityTeamController@all_team_list');

    //公司模块
    //创建公司
    Route::post('create_company','CompanyController@create_company');
    //公司详情
    Route::post('company_info','CompanyController@company_info');
    //绑定公司
    Route::post('bind_company','CompanyController@bind_company');

    //书籍模块
    Route::post('book_info','BookController@book_info');
    //书本基本信息
    Route::post('book_base_info','BookController@book_base_info');
    //加入书架
    Route::post('create_bookshelf','BookController@create_bookshelf');
    //我的书架
    Route::post('my_bookshelf','BookController@my_bookshelf');
    //验证登记书架
    Route::post('bookshelf_register','BookController@bookshelf_register');

    //文章详情
    Route::post('article_info','ArticleController@article_info');

    //首页
    Route::get('banner','IndexController@banner');
    //海报
    Route::get('poster','IndexController@poster');
    //封面
    Route::post('activity_coves','IndexController@activity_coves');
    //书架智能
    Route::get('bookshelf_guide','IndexController@bookshelf_guide');
    Route::post('system_images','IndexController@system_images');

    Route::post('test','IndexController@test');


    //活动列表
    Route::post('class_list','ActivityClassController@class_list');
    //活动模块
    Route::post('class_info','ActivityClassController@class_info');
    //活动详细信息
    Route::post('class_all_info','ActivityClassController@class_all_info');
    //添加 六項精進活动 報名記錄
    Route::post('add_activity_register','ActivityClassController@add_activity_register');

    Route::post('activity_list','ActivityClassController@activity_list');

    //我參加的所有讀書活动
    Route::post('my_activity_list','ActivityClassController@my_activity_list');

    //文章作业
    Route::post('article_calss_work','WorkController@article_calss_work');
    //企业读书
    Route::post('company_read_work','WorkController@company_read_work');
    //塾生学习
    Route::post('personal_read_work','WorkController@personal_read_work');
    //我的音频学习
    Route::post('audio_study_work','WorkController@audio_study_work');

    //发布作业
    Route::post('add_work','WorkController@add_work');
    //修改作业
    Route::post('edit_work','WorkController@edit_work');

    Route::post('my_work','WorkController@my_work');
    //
    Route::post('team_user_work','WorkController@team_user_work');
    //我完成的作业
    Route::post('my_finish_work','WorkController@my_finish_work');
    //我的作业详情
    Route::post('my_study_info','WorkController@my_study_info');
    //所有心得列表
    Route::post('study_list','WorkController@study_list');
    //删除学习
    Route::post('study_del','WorkController@study_del');
    //心得统计
    Route::post('study_sort','WorkController@study_sort');

    //六項精進 打卡统计
    Route::post('company_work_record','WorkController@company_work_record');

    //企業讀書 打卡統計
    Route::post('company_read_work_record','WorkController@company_read_work_record');
    //塾生讀書 打卡統計
    Route::post('personal_read_work_record','WorkController@personal_read_work_record');

    //塾生学习作业统计
    Route::post('my_personal_work','WorkController@my_personal_work');

    //某小組打卡統計
    Route::post('team_study_sort','WorkController@team_study_sort');

    //个人签到
    Route::post('personal_sign','SignController@personal_sign');
    //签到设置
    Route::post('set_sign','SignController@set_sign');
    //小组签到
    Route::post('team_personal_sign','SignController@team_personal_sign');
    //小组签到排名
    Route::post('sign_team_order','SignController@sign_team_order');

    Route::post('sign_user_order','SignController@sign_user_order');
    //积分统计（企业）
    Route::post('integral_record','SignController@integral_record');

    //企业读书
    //列表
    Route::post('company_read_list','CompanyReadController@company_read_list');
    //详情
    Route::post('company_read_info','CompanyReadController@company_read_info');

    //塾生学习
    Route::post('personal_read_list','PersonalReadController@personal_read_list');
    Route::post('personal_read_info','PersonalReadController@personal_read_info');
    Route::post('personal_read_register','PersonalReadController@personal_read_register');
    //手机号码
    Route::post('phone','PersonalReadController@phone');


    //
    Route::post('aliyun_oss_config','AliYunOsssController@aliyun_oss_config');

    //
    Route::post('create_collection','CollectionController@create_collection');
    Route::post('my_collection','CollectionController@my_collection');
    //删除
    Route::post('del_collection','CollectionController@del_collection');

    //个人信息
    Route::post('personal_info','PersonalController@personal_info');
    //其他人主页
    Route::post('others_index','PersonalController@others_index');
    Route::post('edit_user_info','PersonalController@edit_user_info');
    //获取打卡海报二维码
    Route::post('qrcode','PersonalController@qrcode');
    //获取微信手机号
    Route::post('get_wx_phone','PersonalController@get_wx_phone');

    //我的书架记录
    Route::post('my_book_record','BookRecordController@my_book_record');

    //点赞
    Route::post('create_support','SupportController@create_support');
    //删除点赞
    Route::post('del_support','SupportController@del_support');

    //发布评论
    Route::post('create_study_comment','StudyCommentController@create_study_comment');
    //删除评论
    Route::post('del_study_comment','StudyCommentController@del_study_comment');

    //音频学习列表
    Route::post('audio_study_list','AudioStudyController@audio_study_list');
    //音频学习详情
    Route::post('audio_info','AudioStudyController@audio_info');


});

Route::post('upload','TestController@upload');