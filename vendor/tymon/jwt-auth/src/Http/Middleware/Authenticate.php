<?php

/*
 * This file is part of jwt-auth.
 *
 * (c) Sean Tymon <tymon148@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tymon\JWTAuth\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Authenticate extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     *
     * @throws \Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //dd(Auth::guard('api') -> check());
        try{
            if (!$this -> auth -> parser() -> setRequest($request) -> hasToken()) {
                throw new \Exception('token不能为空');
            }

            if (!Auth::guard('jwt_auth') -> check()) {
                throw new \Exception('token无效或已过期');
            }

            return $next($request);

        }catch (\Exception $e){
            return response() -> json([
                'error_code' => 400,
                'msg' => $e -> getMessage()
            ]);
        }
    }
}
